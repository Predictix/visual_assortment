let
  userFile = import ./users.nix;
  userList = userFile.userList;
in
{
  require = [ <lbdevops/nixops/modeler-app/network.nix> ];

  machine =
    { imports = [      
        <lbdevops/nixos/logicblox/installer.nix>
        <lbdevops/nixos/base.nix>
      ];

      users.lbAdminUsers = userList;

      logicblox.application.sslCertificate = <global_creds/logicblox.crt>;
      logicblox.application.sslCertificateKey = <global_creds/logicblox.key>;      
    };
}
