#! /usr/bin/env python

import os
import lb_simple
import unittest
import csv
import argparse

class TestCase(unittest.TestCase):

    def assert_popcount(self, predname, count):
        actual = self.session.popcount(predname)
        self.assertEqual(actual, count, "Expected %s found %s for %s" % (count, actual, predname))

    def assert_popcount_atleast(self, predname, count):
        actual = self.session.popcount(predname)
        self.assertGreaterEqual(actual, count, 'Popcount for %s was %s and not >= %s' % (predname, actual, count))

    def assert_popcount_nonzero(self, predname):
        self.assertGreater(self.session.popcount(predname), 0, 'Popcount for %s was zero' % predname)

    def tearDown(self):
        self.session.close()

# class TestLinePlanWorkbook(TestCase):

#     def setUp(self):
#         wb = os.environ.get('WORKBOOK_ID')
#         self.session = lb_simple.Session('target-ap@' + wb)

#     def test_hierarchy_cluster(self):
#         self.assert_popcount_atleast("location:cluster", 5)
#     def test_measures_storesellingweeks(self):
#         self.assert_popcount_nonzero("StoreSellingWeeks_Cnt_LY_CLSOCY_LCL")
#     def test_measures_slsnonclear(self):
#         self.assert_popcount_nonzero("SlsNonClear_U_LY_CLSOCY_LCL")

#     @unittest.expectedFailure
#     def test_measures_storevogrpup(self):
#         self.assert_popcount_nonzero("StoreVOGroup_TY_DPSO_PLP")
#     def test_measures_cluster(self):
#         self.assert_popcount_nonzero("Cluster_WP_CLSOCY_PLP")
#     def test_measures_elig_slsnonclear_cu(self):
#         self.assert_popcount_nonzero("EligibleSlsNonClear_U_WP_CLCUCY_PLP")
#     def test_measures_elig_storesellingweeks(self):
#         self.assert_popcount_nonzero("EligibleStoreSellingWeeks_Cnt_WP_CLCHCY_PLP")
#     def test_measures_elig_slsnonclear_ch(self):
#         self.assert_popcount_nonzero("EligibleSlsNonClear_U_WP_CLCHCY_PLP")

# class TestLinePlanWorkbookDev(TestLinePlanWorkbook):
#     def test_hierarchy_store(self):
#         self.assert_popcount_atleast("location:store", 1888)    


class TestPopcount(TestCase):

    def setUp(self):
        master_ws = os.environ.get('MASTER_WS')
        print("master_ws %s" % master_ws)
        self.session = lb_simple.Session(master_ws)

    def convert_to_num(self,numstr):
        return int(numstr.strip())
    
    def popcounts(self, popcount_file):
        with open(popcount_file, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                expected_value=row["ExpectedValue"].strip()
                predicate_name=row["Predicate"]

                if expected_value.startswith("+",0):
                    actual_value = self.convert_to_num(expected_value[1:])
                    self.assert_popcount_atleast(predicate_name, actual_value)
                elif expected_value.startswith("!",0):
                    actual_value = self.convert_to_num(expected_value[1:])
                    self.assert_popcount_nonzero(predicate_name, actual_value)
                else:
                    self.assert_popcount(predicate_name, self.convert_to_num(expected_value))
    
class TestMasterDev(TestPopcount) :
    def setUp(self):
        master_ws = os.environ.get('MASTER_WS')        
        self.session = lb_simple.Session(master_ws)

    def test_popcounts(self):
        self.popcounts('tests/smoke/expected_popcounts/master-dev-popcount.txt')

#     def test_hierarchy_week(self):
#         self.assert_popcount_atleast("time:week", 204)
#     def test_hierarchy_woy(self):
#         self.assert_popcount_atleast("time:woy", 52)
#     def test_hierarchy_design_cycle(self):
#         self.assert_popcount_atleast("time:design_cycle", 17)
#     def test_hierarchy_class(self):
#         self.assert_popcount_atleast("product:class", 13)
#     def test_hierarchy_stylecolor(self):
#         self.assert_popcount_atleast("product:stylecolor", 893)
#     def test_hierarchy_style(self):
#         self.assert_popcount_atleast("product:style", 329)
#     def test_hierarchy_store(self):
#         self.assert_popcount_atleast("location:store", 1888)
#     def test_hierarchy_cluster(self):
#         self.assert_popcount_atleast("location:cluster", 5)
#     def test_metrics(self):
#         self.assert_popcount_nonzero("GlobalAccess_WP_B_USMO_UPER")
#         self.assert_popcount_nonzero("Permissions_WP_USDPMO_UPER")
#         for popcount in self.popcounts:
#             n, predname = popcount.split(': ')
#             predname = predname.strip()
#             self.assert_popcount_atleast(predname, int(n))


if __name__ == '__main__':
    unittest.main()
