#! /usr/bin/env bash

set -e
set -x
set -o pipefail

MASTER_WS=$1
shift;

function smoketest_master_dev()
{
  export PYTHONPATH="tests/smoke:$PYTHONPATH"
  export MASTER_WS=$MASTER_WS
  python -m unittest test_popcount.TestMasterDev
}

# function smoketest_workbooks()
# {
#   export PYTHONPATH="tests/smoke:$PYTHONPATH"

#   export WORKBOOK_ID=$(lb workbook list -w target-ap --csv id --name "Line Plan - 042-00 WRANGLER/LG SHORTS - 2016 C1 Spring")
#   python -m unittest test_popcount.TestLinePlanWorkbookDev

#   export WORKBOOK_ID=$(lb workbook list -w target-ap --csv id --name "Line Plan - 073 ONLINE UNIQUE MENS SPORTSWEAR - 2016 C1 Spring")
#   python -m unittest test_popcount.TestLinePlanDeptWorkbookDev
# }

echo "$@"
$@
