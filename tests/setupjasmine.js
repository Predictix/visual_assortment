/* jshint jasmine: true */ 
"use strict";

var reporter = require('jasmine-spec-reporter');

jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000 * 5;

jasmine.getEnv().addReporter(new reporter({displaySpecDuration: true}));
