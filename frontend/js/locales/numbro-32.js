(window["webpackJsonp_name_"] = window["webpackJsonp_name_"] || []).push([[155],{

/***/ 1485:
/***/ (function(module, exports) {

/*!
 * numbro.js language configuration
 * language : Indonesian
 * author : Tim McIntosh (StayinFront NZ)
 */

module.exports = {
    languageTag: "id",
    delimiters: {
        thousands: ",",
        decimal: "."
    },
    abbreviations: {
        thousand: "r",
        million: "j",
        billion: "m",
        trillion: "t"
    },
    ordinal: function() {
        return ".";
    },
    currency: {
        symbol: "Rp",
        code: "IDR"
    }
};


/***/ })

}]);