(window["webpackJsonp_name_"] = window["webpackJsonp_name_"] || []).push([[162],{

/***/ 1492:
/***/ (function(module, exports) {

/*!
 * numbro.js language configuration
 * language : Norwegian Bokmål (nb)
 * author : Tim McIntosh (StayinFront NZ)
 */

module.exports = {
    languageTag: "nb",
    delimiters: {
        thousands: " ",
        decimal: ","
    },
    abbreviations: {
        thousand: "t",
        million: "mil",
        billion: "mia",
        trillion: "b"
    },
    ordinal: function() {
        return ".";
    },
    currency: {
        symbol: "kr",
        code: "NOK"
    }
};


/***/ })

}]);