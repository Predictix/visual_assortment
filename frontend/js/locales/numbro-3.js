(window["webpackJsonp_name_"] = window["webpackJsonp_name_"] || []).push([[152],{

/***/ 1456:
/***/ (function(module, exports) {

/*!
 * numbro.js language configuration
 * language : German
 * locale: Austria
 * author : Tim McIntosh (StayinFront NZ)
 */

module.exports = {
    languageTag: "de-AT",
    delimiters: {
        thousands: " ",
        decimal: ","
    },
    abbreviations: {
        thousand: "k",
        million: "m",
        billion: "b",
        trillion: "t"
    },
    ordinal: function() {
        return ".";
    },
    currency: {
        symbol: "€",
        code: "EUR"
    }
};


/***/ })

}]);