(window["webpackJsonp_name_"] = window["webpackJsonp_name_"] || []).push([[171,170],{

/***/ 1325:
/***/ (function(module, exports) {

/*!
 * numeral.js language configuration
 * language : Romanian
 * author : Andrei Alecu https://github.com/andreialecu
 */

module.exports = {
    languageTag: "ro-RO",
    delimiters: {
        thousands: ".",
        decimal: ","
    },
    abbreviations: {
        thousand: "mii",
        million: "mil",
        billion: "mld",
        trillion: "bln"
    },
    ordinal: function() {
        return ".";
    },
    currency: {
        symbol: " lei",
        position: "postfix",
        code: "RON"
    },
    currencyFormat: {
        thousandSeparated: true,
        totalLength: 4,
        spaceSeparated: true,
        average: true
    },
    formats: {
        fourDigits: {
            totalLength: 4,
            spaceSeparated: true,
            average: true
        },
        fullWithTwoDecimals: {
            output: "currency",
            mantissa: 2,
            spaceSeparated: true,
            thousandSeparated: true
        },
        fullWithTwoDecimalsNoCurrency: {
            mantissa: 2,
            thousandSeparated: true
        },
        fullWithNoDecimals: {
            output: "currency",
            spaceSeparated: true,
            thousandSeparated: true,
            mantissa: 0
        }
    }
};


/***/ }),

/***/ 1499:
/***/ (function(module, exports, __webpack_require__) {

/*!
 * numbro.js language configuration
 * language : Romanian (ro)
 * author : Tim McIntosh (StayinFront NZ)
 */

module.exports = __webpack_require__(1325);


/***/ })

}]);