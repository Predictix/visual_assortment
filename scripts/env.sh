#!/usr/bin/env bash
# env.sh (use "source" to run)

# LB_COMPONENTS
source "upstream/logicblox/etc/profile.d/logicblox.sh"
source "upstream/logicblox/etc/bash_completion.d/logicblox.sh"

# Setup Node
export NODE_HOME="${PWD}/upstream/node"
export NODE_PATH="${PWD}/node_modules:${NODE_HOME}/lib/node_modules:${NODE_PATH}"
export PATH="${NODE_HOME}/bin:${PATH}"

# Set up Network Manager
# export NETWORK_MANAGER_HOME=upstream/network_manager/out

# Required for workspace creation in make deploy
export  LB_CONNECTBLOX_ENABLE_ADMIN=1

# Measure query gen HOME and Measure views
# export MEASURE_QUERY_GEN_HOME=upstream/measure-query-gen
# export MEASURE_QUERY_GEN_VIEWS=${THD_TESTS}/relax-config.js

# Set up testing framework
# export PATH=upstream/lamias-servicetester/bin:${PATH}
# export PATH=./node_modules/.bin:$PATH
