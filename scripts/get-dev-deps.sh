#!/usr/bin/env bash 

set -e

#-------------------------------------------
# The script gets dependencies based on the value of :
#    - LOGICBLOX_RELEASE
# Check the configuration section below to set them accordingly.
#-------------------------------------------

#-------------------------------------------
# Configuration
#-------------------------------------------

# Color of output
brown='\e[0;33m'
NC='\e[0m'

# Builder URL
BUILDER_URL="https://bob.logicblox.com"
# Determine OS/architecture
arch=$(uname | tr '[:upper:]' '[:lower:]')
echo "Current architecture: ${arch}"
# Set the Node JS version we want to install
NODE_VERSION=6.11.5
NODE_URL=http://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-$arch-x64.tar.gz
if [[ $arch == 'linux' ]]; then
	LOGICBLOX_RELEASE=$BUILDER_URL/job/logicblox-40/release-4.4.12/linux.binary_tarball/latest/download-by-type/file/binary-dist
else
	LOGICBLOX_RELEASE=$BUILDER_URL/job/logicblox-40/release-4.4.12/osx.binary_tarball/latest/download-by-type/file/binary-dist
fi	
echo $LOGICBLOX_RELEASE

#-------------------------------------------
# Functions:
#      1- clean
#      2- create_upstream
#      3- logicblox
#      4- mqg
#      5- lamias
#      6- node
#      7- packages
#      8- get_data
#-------------------------------------------

function clean()
{
  if [ -d upstream ]
  then
    mkdir -p /tmp/upstream
    BACKDIR="/tmp/upstream/tgt_upstream_`date +%Y%m%d_%M%S`"
    echo -e "-------------------------"
    echo -e "Backing up downloads to $BACKDIR ..."
    echo -e "-------------------------"
    chmod -R +w upstream
    mv upstream $BACKDIR
  fi
}

function create_upstream()
{
  if [ ! -d upstream ]
  then
    mkdir -p upstream/downloads
  fi
}

function logicblox()
{
  create_upstream
  pushd upstream 1>/dev/null
  echo -e "--------------------------------"
  echo -e " Downloading logicblox release..."
  echo -e "--------------------------------"
  curl -L $LOGICBLOX_RELEASE > downloads/LogicBlox.tar.gz
  tar xfz downloads/LogicBlox.tar.gz
  rm -rf logicblox
  if [ -d LogicBlox* ]
  then
    mv LogicBlox* logicblox
  else
    mv logicblox* logicblox
  fi
  popd 1>/dev/null
}

function mqg()
{
  create_upstream
  pushd upstream 1>/dev/null
  echo -e "--------------------------------"
  echo -e " Downloading measureQueryGen..."
  echo -e "--------------------------------"
  rm -rf measure-query-gen
  hg clone -b default ssh://hg@bitbucket.org/logicblox/measure-query-gen 
  popd 1>/dev/null
}

function lamias()
{
  create_upstream
  pushd upstream 1>/dev/null
  echo -e "--------------------------------"
  echo -e " Downloading Lamias Service Tester..."
  echo -e "--------------------------------"
  rm -rf lamias-servicetester
  hg clone -b default ssh://hg@bitbucket.org/Predictix/lamias-servicetester
  popd 1>/dev/null
  pushd upstream/lamias-servicetester 1>/dev/null
  npm install
  popd 1>/dev/null
}

function network_manager()
{
  create_upstream
  pushd upstream 1>/dev/null
  echo -e "--------------------------------"
  echo -e " Downloading Network Manager..."
  echo -e "--------------------------------"
  rm -rf network_manager
  hg clone -b default ssh://hg@bitbucket.org/Predictix/network_manager
  popd 1>/dev/null
  pushd upstream/network_manager 1>/dev/null
  lb services restart && lb config && make && make install
  popd 1>/dev/null
  ln -s upstream/network_manager/out network_manager
}

function node()
{
  create_upstream
  pushd upstream 1>/dev/null
  echo -e "--------------------------------"
  echo -e " Downloading NodeJS..."
  echo -e "--------------------------------"
  echo $NODE_URL
  curl -L $NODE_URL > downloads/node.tar.gz
  rm -rf node*
  tar xvfvz downloads/node.tar.gz
  mv node* node
  popd 1>/dev/null
}

function packages()
{
  rm -rf node_modules 
  echo
  echo -e "----------------------------------"
  echo -e " Getting NPM dependencies..."
  echo -e "----------------------------------"
  npm install
  echo -e "------------------------------------"
  echo -e " Installing pystache through pip... "
  echo -e "------------------------------------"
  pip install pystache
 
}

#-------------------------------------------
# Main
#-------------------------------------------
if [ "$#" = "0" ]
then
  clean
  create_upstream
  logicblox
  #network_manager
  #mqg
  #lamias
  node
  packages
else
  for f in $* ; do
    $f
  done
fi

source scripts/env.sh

exit 0
