#! /usr/bin/env bash

set -e
set -x

deploymentid=""
jobset=""
instancetype="ec2"
dataroot=""
constantroot=""
runworkflow="yes"

for i in "$@"
do
case $i in
    --deployment-id=*)
    deploymentid="${i#*=}"
    shift
    ;;
    --instance-type=*)
    instancetype="${i#*=}"
    shift
    ;;
    --jobset=*)
    jobset="${i#*=}"
    shift
    ;;
    --data-root=*)
    dataroot="${i#*=}"
    shift
    ;;
    --constant-root=*)
    constantroot="${i#*=}"
    shift   
    ;;
    --no-workflow)
    runworkflow="no"
    shift
    ;;
    *)
    # unknown option
    ;;
esac
done

echo "Deploying jobset $jobset to $deploymentid, instance type $instancetype"
echo "Using data-root: $dataroot"
echo "Using constant-root: $constantroot"

if [ "${runworkflow}" == "yes" ]; then
    nixops set-args -d $deploymentid --argstr installArgs " --initialize-data --data-root=$dataroot --constant-root=$constantroot"
fi

./deploy-scripts/generic-deploy.sh $deploymentid $instancetype wholefoods $jobset