#!/usr/bin/env bash

set -e
set -x

if [ `uname` == "Darwin" ]; then
    script_dir=$(cd "$(dirname "$0")"; pwd)
else
    script_dir=$(dirname $(readlink -f $0))
fi
base_dir=$script_dir/..

WS=$1

# unload services first to avoid errors
lb web-server unload-services -w $WS
lb web-server unload-services -w $WS@__workbook_base_branch

# load services
lb web-server load-services -w $WS

# load meta model configuration
echo "Load meta model configuration"
pushd $base_dir
lb web-client batch -i scripts/batch/config.batch
lb execblock $WS modeler_config:metamodel:translate

echo "Load address model configuration"
lb web-client batch -i scripts/batch/addressmodel.batch
lb execblock $WS modeler_config:metamodel:address_mapping

echo "Install target locks"
#lb execblock $WS util:lock_pivot

# refresh service after loading meta model
echo "Refresh services"
lb web-server unload-services -w $WS
lb web-server load-services -w $WS

