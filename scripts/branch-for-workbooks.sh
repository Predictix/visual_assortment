#!/usr/bin/env bash

set -e
set -x

APP=$1
WORKSPACE=$2
BRANCH_NAME=$3

if [ -z $WORKSPACE ]; then
    echo "Must provide WORKSPACE name."
    echo "USAGE: sh branch_for_workbooks.sh <WORKSPACE> [BRANCH_NAME]"
    exit 1
fi

if [ -z $BRANCH_NAME ]; then
    BRANCH_NAME="__workbook_base_branch"
fi

# create the base branch for workbooks
lb branch $WORKSPACE $BRANCH_NAME --overwrite

# unload services on main and load services on the branch
lb web-server unload-services -w $WORKSPACE
lb web-server load-services -w $WORKSPACE@$BRANCH_NAME

# generate view warmup queries
make rule-queries
make view-queries

# submit view warmup queries
make warmup-rules
make warmup-views

# unload services on branch and load services on main.
lb web-server unload-services -w $WORKSPACE@$BRANCH_NAME
lb web-server load-services -w $WORKSPACE
