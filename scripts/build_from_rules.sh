#!/usr/bin/env bash

set -e
set -x

APP=$1
WS=$2
WORKBOOK_BASE_BRANCH=$3

if [ `uname` = "Darwin" ]; then
    script_dir=$(cd "$(dirname "$0")"; pwd)
else
    script_dir=$(dirname $(readlink -f $0))
fi
base_dir=$script_dir/..

echo "Load CubiQL rules"
lb branch $WS before-load-cubiql-rules --overwrite
bash $script_dir/load-cubiql-rules.sh $APP $base_dir

# create branch before before workbook branch
lb branch $WS before-rule-warmup --overwrite

# create data-empty branch used for creating workbooks.
echo "Create branch with no data, for workbooks, and warmup rules"
bash $script_dir/branch-for-workbooks.sh $APP $WS $WORKBOOK_BASE_BRANCH

echo "Creating workbook templates"
lb workflow run -q -f $base_dir/src/workflows/workbooks.wf --main workbooks.create_templates \
  --param app_prefix=$APP --param src_root=$base_dir

lb branch $WS before-data-load --overwrite
