#!/usr/bin/env bash

set -e
set -x

ROOT=$1

shopt -s nullglob
for wf in "$ROOT/workflows/main"/*.wf
do
  filename=$(basename "$wf")
  wfname="${filename%.*}"
  echo "Installing workflow file ${wf} with name \"${wfname}\""
  lb workflow install -f $wf --name $wfname
done
