#!/usr/bin/env bash

set -x
set -e

WS=$1

## this should build everything needed to run the tests, i.e., workbooks etc

lb replace-default-branch $1 before-data-load
make load-test-data
make build-all-workbooks
