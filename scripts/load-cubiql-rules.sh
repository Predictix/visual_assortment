#! /usr/bin/env bash

set -e
set -x

APP_NAME=$1
ROOT=$2

# CubiQL

for f in $(find $ROOT/src/rules -path "*.logic")
do
  lb exec /$APP_NAME -f $f
done