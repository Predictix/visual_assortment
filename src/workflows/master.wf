import "workbooks.wf"

workflow master.TdxImport(input*, app_prefix, transport, timeout="86400000s",
  allow_partial_import=false, file_type="AUTO", full=false, if_missing="FAIL")

  lb.tdx.Import(
    input=$input,
    timeout=$timeout,
    txn_service="/$(app_prefix)/delim-file/txn",
    allow_partial_import=$allow_partial_import,
    transport=$transport,
    error_file_only_on_error=true,
    full=$full,
    unmatched_import_reaction=$if_missing)

workflow master.TdxExport(export*, transport)
  lb.tdx.Export(
    transport=$transport,
    input=$export
  )

workflow master.deploy_initial(app_prefix,constant_location,location,timeout) [] {
  master.deploy_initial_data($app_prefix, $constant_location, $location, $timeout)
  ;
  workbooks.build_all_workbooks($app_prefix)
}

workflow master.deploy_initial_data(app_prefix,constant_location,location,timeout) [] {
  master.import_hierarchies(app_prefix=$app_prefix,location=$location,timeout=$timeout) 
  ||
  master.import_users(app_prefix=$app_prefix,location=$location,timeout=$timeout)
  ;
  master.import_measures(app_prefix=$app_prefix,location=$location,timeout=$timeout) 
  
}

workflow master.import_hierarchies(app_prefix,location,timeout,transport="http://localhost:55183",full=false)[] {
  master.TdxImport(
    app_prefix=$app_prefix,
    timeout=$timeout,
    input={
      "{
         service: '/$(app_prefix)/delim-file/hierarchy/location'
         file:    '$(location)/hierarchies/location.csv'
         error:   '$(location)/error/hierarchies/location.csv'
      }","{
         service: '/$(app_prefix)/delim-file/hierarchy/product'
         file:    '$(location)/hierarchies/product.csv'
         error:   '$(location)/error/hierarchies/product.csv'
      }","{
         service: '/$(app_prefix)/delim-file/hierarchy/time'
         file:    '$(location)/hierarchies/time.csv'
         error:   '$(location)/error/hierarchies/time.csv'
      }","{
         service: '/$(app_prefix)/delim-file/hierarchy/concept'
         file:    '$(location)/hierarchies/concept.csv'
         error:   '$(location)/error/hierarchies/concept.csv'
      }"
    },
    transport=$transport,
    full=$full)
}

workflow master.import_measures(app_prefix,location,timeout,transport="http://localhost:55183",full=false)[] {
  master.TdxImport(
    app_prefix=$app_prefix,
    timeout=$timeout,
    input={
       "{
         service: '/$(app_prefix)/delim-file/measures/dashboard'
         file:    '$(location)/measures/dashboard.csv'
         error:   '$(location)/error/dashboard.csv'
       }",
     "{
        service: '/$(app_prefix)/delim-file/measures/assortment_visualization'
        file:    '$(location)/measures/assortment_visualization.csv'
        error:   '$(location)/error/measures/assortment_visualization.csv'
      }",
     "{
        service: '/$(app_prefix)/delim-file/measures/store_count'
        file:    '$(location)/measures/store_count.csv'
        error:   '$(location)/error/measures/store_count.csv'
      }",
     "{
        service: '/$(app_prefix)/delim-file/measures/concept_measures'
        file:    '$(location)/measures/concept_measures.csv'
        error:   '$(location)/error/measures/concept_measures.csv'
      }"          
    },
    transport=$transport,
    full=$full)
}

workflow master.import_users(app_prefix,location,timeout,transport="http://localhost:55183",full=false)[] {
  master.TdxImport(
    app_prefix=$app_prefix,
    timeout=$timeout,
    input={
      "{
         service: '/$(app_prefix)/admin/credentials/users'
         file:    '$(location)/users/users.csv'
         error:   '$(location)/error/users.csv'
       }"
    },
    transport=$transport,
    full=$full)
  ;
  master.TdxImport(
    app_prefix=$app_prefix,
    timeout=$timeout,
    input={
      "{
         service: '/$(app_prefix)/admin/credentials/authorization/user_roles'
         file:    '$(location)/users/user-roles.csv'
         error:   '$(location)/error/user-roles.csv'
       }"
    },
    transport=$transport,
    full=$full)
}
