/**
 * Scalars that are used to configure workflow execution dynamically.
 */
block(`config) {

  export(`{
    app_prefix[] = s -> string(s).

    // backup-recovery related configuration options
    backup_root[] = s -> string(s).
    recovery_root[] = s -> string(s).
    timestamp[] = s -> string(s).
    timeout[] = s -> string(s).
    br_timeout[] = s -> string(s). // specific timeout for backup recovery

    /**
     * The prefix for the URLs where data should be stored 
     *
     * This can be an S3 URL (e.g. s3://modeler-now/data) or a file path/URL (e.g. file:///tmp/data).
     */
    data_root[] = s -> string(s).

    /**
     * Prefix for URLs where constant data is stored.
     */
    constant_root[] = s -> string(s).

    mk_timestamp() -> .

  }),

  clauses(`{

    lang:pulse(`mk_timestamp).

    ^timestamp[] = s
      <-
      datetime:now[] = now,
      datetime:format[now, "%Y_%m_%d_%H_%M_%S_%q"] = s,
      +mk_timestamp().

  })

} <--.
