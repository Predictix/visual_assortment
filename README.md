_Modeler Now!_
===============

### Quick Start

#### Building with workbooks

Use the following commands to build Modeler Now! with workbooks:

    $ npm install
    $ lb config
    $ make
    $ make load-test-data
    $ make build-all-workbooks
    $ make start-nginx

You can access the application at http://localhost:8086/

Three workbooks will be built: 

* Two workbooks based on the Class template, each containing data of one product class
* One workbook for the Class/Region template, only containing data for class Z and the Midwest region.

### Making changes to the application

Create or Modify Dimensions, Hierarchies, and Levels
* src/config/Dimensions.csv
* src/config/Hierarchies.csv
* src/config/Levels.csv

Create or Modify Measures
* src/config/Intersection.csv
* src/config/Measures.csv

Create or Modify CubiQL Rules
* src/rules/*.logic

Create or Modify Views and Navigation Panel for a certain Workbook Template
* src/config/views/\<template\>/navigation.json
* src/config/views/\<template\>/canvas/*.json
* src/config/views/\<template\>/sheets/*.json

### src directory

- src/config/ : 
  configuration specification for levels, dimensions, hierarchies, and measures.  These files are used to
    * generate LogiQL declarations of predicates that contain level members, their mappings, as well as measure base predicates
    * generate LogiQL that populates the measure meta model accordingly
- src/logiql/ : 
  contains LogiQL 
    * modeler\_now.project: main project 
    * modeler\_now-wf.project: workflow project     
- src/logiql/services: 
  service configurations for common services such as workbook, connectblox, measure service, as well as TDX services for data import/export 
- src/rules/ : 
  CubiQL business rules 
- src/workflows/ : 
  Workflow definitions for loading data and creating templates and workbook instantiations

- scripts: utility scripts, including one that generates LogiQL from metamodel specification, as well as helper scripts to inspect the model for debugging
