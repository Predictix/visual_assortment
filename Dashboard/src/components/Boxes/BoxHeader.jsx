import React from "react";
import { ReactComponent as TimesIcon } from "../../icons/times.svg";

function BoxHeader({ title = "Default Title", layout, dispatch, children }) {
  const closeWidget = () => {
    dispatch({ type: "PUT_ITEM", item: layout });
  };

  return (
    <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
      <div className="-ml-4 -mt-2 flex items-center justify-between flex-wrap sm:flex-no-wrap">
        <div className="ml-4 mt-2">
          <h3 className="text-lg leading-6 font-medium text-gray-900">
            {title}
          </h3>
        </div>
        <div className="ml-4 mt-2 flex-shrink-0">
          <span className="inline-flex">
            {children}
            <button
              className="flex items-center text-gray-400 hover:text-gray-600 focus:outline-none focus:text-gray-600"
              onClick={closeWidget}
            >
              <TimesIcon className="h-6 w-6" title="Close" />
            </button>
          </span>
        </div>
      </div>
    </div>
  );
}

export default BoxHeader;
