import React, { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { PieChart, Pie, Cell, ResponsiveContainer, Tooltip } from "recharts";
import { getMeasureValue } from "../../../api/MeasuresAPI";
import ContributionAnalysisDropdown from "./ContributionAnalysisDropdown";
import { CloseWidgetIcon, UpdateWidgetIcon } from "../../Icons";

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text
      className="text-sm"
      x={x}
      y={y}
      fill="white"
      textAnchor={x > cx ? "start" : "end"}
      dominantBaseline="central"
    >
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

function ContributionAnalysisWidget({
  widget,
  boxId,
  selectOptions,
  chartColors,
  filtersMapping,
  dispatch,
}) {
  const [measureValue, setMeasureValue] = useState([]);
  const [params] = useSearchParams();
  useEffect(() => {
    let ignore = false;

    async function fetchData() {
      const measureValue = await getMeasureValue(
        widget,
        params,
        filtersMapping
      );
      console.log(measureValue);
      if (!ignore) setMeasureValue(measureValue);
    }

    fetchData();
    return () => {
      ignore = true;
    };
  }, [widget, params]);

  const updateWidget = ({ attribute, measure }) => {
    dispatch({
      type: "UPDATE_WIDGET",
      boxId,
      widgetId: widget.id,
      data: { id: widget.id, attribute, measure },
    });
  };

  const removeWidget = () => {
    dispatch({ type: "REMOVE_WIDGET", boxId, id: widget.id });
  };

  return (
    <div className="border p-2 text-center w-1/3 mr-2">
      <div className="flex text-gray-900">
        <ContributionAnalysisDropdown
          className="absolute"
          widget={widget}
          options={selectOptions}
          actionText="Update"
          icon={<UpdateWidgetIcon className="h-5 w-5" title="Update Widget" />}
          onAction={updateWidget}
        />
        <button onClick={removeWidget} className="gray-icon ml-auto">
          <CloseWidgetIcon className="h-5 w-5" title="Close Widget" />
        </button>
      </div>
      <div style={{ width: "100%", height: "79%" }}>
        <ResponsiveContainer>
          <PieChart>
            <Pie
              data={measureValue}
              labelLine={false}
              label={renderCustomizedLabel}
              // outerRadius={80}
              dataKey="value"
              animationDuration={1000}
              animationBegin={0}
              // isAnimationActive={false}
            >
              {measureValue.map((entry, index) => (
                <Cell
                  key={`cell-${index}`}
                  fill={chartColors[index % chartColors.length]}
                />
              ))}
            </Pie>
            <Tooltip />
          </PieChart>
        </ResponsiveContainer>
      </div>
      <span className="italic text-center text-gray-900 block text-sm">
        {`${getOptionText(
          widget.measure,
          selectOptions.measures
        )} - ${getOptionText(widget.attribute, selectOptions.attributes)}`}
      </span>
    </div>
  );
}

function getOptionText(value, options = []) {
  return options.find((option) => option.value === value)?.label;
}

export default ContributionAnalysisWidget;
