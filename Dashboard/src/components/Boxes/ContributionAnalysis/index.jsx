import React, { useState, useEffect } from "react";
import ContributionAnalysisWidget from "./ContributionAnalysisWidget";
import { NumberUtils } from "../../../utils";
import { getAttributes } from "../../../api/AttributesAPI";
import { getMeasures } from "../../../api/MeasuresAPI";
import BoxHeader from "../BoxHeader";
import ContributionAnalysisDropdown from "./ContributionAnalysisDropdown";

function ContributionAnalysis({
  boxConfig,
  dimensions,
  layout,
  dispatch,
  filtersMapping,
}) {
  const [selectOptions, setSelectOptions] = useState({
    attributes: [],
    measures: [],
  });
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    let ignore = false;
    setIsLoading(true);

    async function fetchData() {
      const [attributes, measures] = await Promise.all([
        getAttributes(),
        getMeasures(),
      ]);
      if (!ignore) {
        setSelectOptions({ attributes, measures });
        setIsLoading(false);
      }
    }

    fetchData();
    return () => {
      ignore = true;
    };
  }, []);

  const createWidget = ({ attribute, measure }) => {
    dispatch({
      type: "ADD_WIDGET",
      boxId: boxConfig.id,
      data: {
        id: NumberUtils.generateId(),
        attribute,
        measure,
      },
    });
  };

  return (
    <div className="box">
      <BoxHeader title={boxConfig.label} layout={layout} dispatch={dispatch}>
        <ContributionAnalysisDropdown
          options={selectOptions}
          actionText="Add Widget"
          onAction={createWidget}
        />
      </BoxHeader>
      <div
        className="flex flex-wrap p-2 overflow-x-auto bg-white"
        style={{ height: dimensions.height - 71 }}
      >
        {boxConfig.widgets.map((widget) => (
          <ContributionAnalysisWidget
            key={widget.id}
            boxId={boxConfig.id}
            widget={widget}
            selectOptions={selectOptions}
            filtersMapping={filtersMapping}
            dispatch={dispatch}
            chartColors={boxConfig.chartColors}
          />
        ))}
      </div>
    </div>
  );
}

export default ContributionAnalysis;
