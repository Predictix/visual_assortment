import React, { useState } from "react";
import Select from "../../Select";
import Dropdown from "../../Dropdown";

function ContributionAnalysisDropdown({
  widget,
  options,
  actionText,
  icon,
  onAction,
  className,
}) {
  const [isOpen, setIsOpen] = useState(false);
  let [attribute, setAttribute] = useState(widget?.attribute);
  let [measure, setMeasure] = useState(widget?.measure);

  const toggleDropdown = () => setIsOpen(!isOpen);

  const handleAction = () => {
    if (!attribute) {
      attribute = options.attributes[0].value;
    }
    if (!measure) {
      measure = options.measures[0].value;
    }
    onAction({ attribute, measure });
    toggleDropdown();
  };

  return (
    <Dropdown
      className={className}
      onToggle={(open) => setIsOpen(open)}
      open={isOpen}
      icon={icon}
    >
      <Select
        label="Attribute"
        options={options.attributes}
        value={attribute}
        onChange={(attribute) => setAttribute(attribute)}
      />
      <Select
        label="Measure"
        options={options.measures}
        value={measure}
        onChange={(measure) => setMeasure(measure)}
      />
      <button className="elevated-btn mt-2 mr-2" onClick={handleAction}>
        {actionText}
      </button>
      <button className="elevated-btn mt-2" onClick={toggleDropdown}>
        Cancel
      </button>
    </Dropdown>
  );
}

export default ContributionAnalysisDropdown;
