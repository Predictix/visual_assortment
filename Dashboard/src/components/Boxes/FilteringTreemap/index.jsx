import React, { useState, useEffect } from "react";
import { Treemap, ResponsiveContainer, Tooltip } from "recharts";
import { useSearchParams } from "react-router-dom";
import TreemapRectangle from "./TreemapRectangle";
import TreemapTooltip from "./TreemapTooltip";
import { getTreemapData } from "../../../api/MeasuresAPI";
import { LoadingIcon } from "../../Icons";
import BoxHeader from "../BoxHeader";
import Dropdown from "../../Dropdown";
import Select from "../../Select";

function FilteringTreemap({
  dimensions,
  layout,
  dispatch,
  filtersMapping,
  boxConfig,
}) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  let [searchParams, setSearchParams] = useSearchParams({
    splitBy: boxConfig.defaultFilter,
  });

  useEffect(() => {
    setIsLoading(true);
    let ignore = false;

    async function fetchTreemapData() {
      const data = await getTreemapData(
        boxConfig.sizeMetric,
        boxConfig.colorsMetric,
        searchParams,
        filtersMapping
      );

      if (!ignore) {
        setData(data);
        setIsLoading(false);
      }
    }

    fetchTreemapData();
    return () => {
      ignore = true;
    };
  }, [searchParams]);

  const handleSplitBy = (value) => {
    searchParams.set("splitBy", value);
    setSearchParams(searchParams);
  };

  return (
    <div className="box">
      <BoxHeader title={boxConfig.label} layout={layout} dispatch={dispatch}>
        <Dropdown>
          <Select
            id="splitBy"
            label="Split By"
            className="mr-4"
            value={searchParams.get("splitBy") || ""}
            onChange={handleSplitBy}
            options={boxConfig.filtersList.map((filter) => ({
              value: filter,
              label: filtersMapping[filter].level,
            }))}
          />
        </Dropdown>
      </BoxHeader>
      <div className="w-full" style={{ height: dimensions.height - 71 }}>
        {isLoading ? (
          <div className="h-full flex items-center justify-center">
            <LoadingIcon />
          </div>
        ) : !data || !data.length ? (
          <div className="h-full flex items-center justify-center">
            No data based on the current filters.
          </div>
        ) : (
          <ResponsiveContainer>
            <Treemap
              data={data}
              dataKey="value"
              stroke="#fff"
              aspectRatio={4 / 3}
              isAnimationActive={false}
              animationEasing={"ease-out"}
              animationDuration={500}
              content={
                <TreemapRectangle
                  colorsOptions={boxConfig.colorsOptions}
                  filtersList={boxConfig.filtersList}
                />
              }
            >
              <Tooltip content={<TreemapTooltip />} />
            </Treemap>
          </ResponsiveContainer>
        )}
      </div>
    </div>
  );
}

export default FilteringTreemap;
