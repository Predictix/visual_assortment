import React from "react";

function TreemapTooltip({ active, payload }) {
  if (active) {
    const { sizeMetric, colorsMetric, label } = payload[0].payload;
    return (
      <div className="border rounded p-3 bg-gray-100">
        <div className="text-center text-gray-800 font-bold">{label}</div>
        <div className="text-sm">
          <div>
            <span className="font-medium">{sizeMetric.label}: </span>
            {sizeMetric.value}
          </div>
          <div>
            <span className="font-medium">{colorsMetric.label}: </span>
            {colorsMetric.value}
          </div>
        </div>
      </div>
    );
  }

  return null;
}

export default TreemapTooltip;
