import React from "react";
import { useSearchParams } from "react-router-dom";

export default function TreemapRectangle({
  x,
  y,
  width,
  height,
  label,
  id,
  filterKey,
  drilldownTo,
  filtersList,
  colorsOptions,
  colorsMetric,
}) {
  let [searchParams, setSearchParams] = useSearchParams();
  const drillDown = () => {
    // Don't drill down further when you are splitting by the last level
    const lastLevel = filtersList[filtersList.length - 1];
    if (filterKey === lastLevel) {
      return;
    }

    searchParams.set(filterKey, id);
    searchParams.set("splitBy", drilldownTo);
    setSearchParams(searchParams);
  };

  return (
    <g className="cursor-pointer" onClick={drillDown}>
      <rect
        x={x}
        y={y}
        width={width}
        height={height}
        style={{
          fill: getColorCode(colorsMetric?.value, colorsOptions),
          stroke: "#fff",
        }}
      />
      <text
        x={x + width / 2}
        y={y + height / 2 + 7}
        textAnchor="middle"
        fill="#fff"
        fontSize={14}
      >
        {label}
      </text>
    </g>
  );
}

function getColorCode(value, colorsOptions) {
  let color = colorsOptions.defaultCode;

  colorsOptions.values.forEach(({ code, min, max }) => {
    if (value >= min && value <= max) {
      color = code;
      return;
    }
  });

  return color;
}
