import React, { useState, useEffect } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import { getPlans } from "../../../api/PlansAPI";
import BoxHeader from "../BoxHeader";
import { useSearchParams } from "react-router-dom";

export default function BuyPlanReview({
  dimensions,
  boxConfig,
  layout,
  filtersMapping,
  dispatch,
}) {
  console.log(boxConfig);
  const [plans, setPlans] = useState([]);
  const [params] = useSearchParams();

  useEffect(() => {
    let ignore = false;

    async function fetchData() {
      const plans = await Promise.all(
        boxConfig.components.map((component) =>
          getPlans(component, filtersMapping, params)
        )
      );
      console.log(plans);
      if (!ignore) setPlans(plans);
    }

    fetchData();
    return () => {
      ignore = true;
    };
  }, [params]);

  return (
    <div className="box">
      <BoxHeader title={boxConfig.label} layout={layout} dispatch={dispatch} />
      <div className="p-2 bg-white" style={{ height: dimensions.height - 71 }}>
        <ResponsiveContainer>
          <BarChart data={plans}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            {Object.entries(boxConfig.barColors).map(([key, color]) => {
              return <Bar dataKey={key} fill={color} />;
            })}
          </BarChart>
        </ResponsiveContainer>
      </div>
    </div>
  );
}
