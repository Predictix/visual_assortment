import React, { useState } from "react";
import { Dialog } from "@reach/dialog";
import VisuallyHidden from "@reach/visually-hidden";

function GalleryItem({ item }) {
  const [showDialog, setShowDialog] = useState(false);
  const open = () => setShowDialog(true);
  const close = () => setShowDialog(false);

  return (
    <div className="w-full">
      <div className="gallery-item h-full cursor-pointer shadow-md">
        <img
          className="h-full w-full"
          src={item.image}
          alt={item.id}
          onClick={open}
        />
        <h3>{item.label}</h3>
      </div>
      <Dialog
        aria-label="Item Details"
        className="relative rounded-lg z-20"
        isOpen={showDialog}
        onDismiss={close}
      >
        <button className="close-button" onClick={close}>
          <VisuallyHidden>Close</VisuallyHidden>
          <span aria-hidden>×</span>
        </button>
        <div className="flex">
          <div>
            <img
              className="w-full h-auto block rounded-b"
              src={item.image}
              alt={item.id}
            />
          </div>
          <div>
            <h1 className="text-3xl">{item.label}</h1>
            <p>{item.description}</p>
          </div>
        </div>
      </Dialog>
    </div>
  );
}

export default GalleryItem;
