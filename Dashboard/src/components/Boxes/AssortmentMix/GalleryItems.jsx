import React from "react";
import GalleryItem from "./GalleryItem";

function GalleryItems({ dimensions, items }) {
  return (
    <div
      style={{
        height: dimensions.height - 134,
        display: "grid",
        gridTemplateColumns: "repeat(auto-fit, minmax(4rem, 1fr))",
        gridTemplateRows: "repeat(auto-fit, minmax(2.5rem, 1fr))",
      }}
    >
      {items.map((item, index) => (
        <GalleryItem key={index} item={item} />
      ))}
    </div>
  );
}
export default GalleryItems;
