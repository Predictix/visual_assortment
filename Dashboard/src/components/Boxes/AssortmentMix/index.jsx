import React, { useState, useEffect } from "react";
import GalleryItems from "./GalleryItems";
import Pagination from "./Pagination";
import BoxHeader from "../BoxHeader";
import Dropdown from "../../Dropdown";
import { getStyleColors } from "../../../api/ProductsAPI";
import "./AssortmentMix.css";
import { useSearchParams } from "react-router-dom";

function AssortmentMix({
  layout,
  dimensions,
  dispatch,
  filtersMapping,
  boxConfig,
}) {
  const [items, setItems] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = boxConfig.itemsPerPage;
  const [params] = useSearchParams();

  useEffect(() => {
    async function fetchGalleryItems() {
      const items = await getStyleColors(params, filtersMapping);
      setItems(items);
    }

    fetchGalleryItems();
  }, [params]);

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = items.slice(indexOfFirstItem, indexOfLastItem);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const paginateToNextPage = () => {
    const numberOfPages = Math.ceil(items.length / itemsPerPage);
    if (currentPage === numberOfPages) {
      return;
    }
    setCurrentPage(currentPage + 1);
  };

  const paginateToPrevPage = () => {
    if (currentPage === 1) {
      return;
    }
    setCurrentPage(currentPage - 1);
  };

  const onChange = (event) => {
    const itemsPerPage = parseInt(event.target.value);
    if (itemsPerPage > 0) {
      dispatch({
        type: "SET_ITEMS_PER_PAGE",
        boxId: boxConfig.id,
        itemsPerPage,
      });
    }
  };

  return (
    <div className="box">
      <BoxHeader title={boxConfig.label} layout={layout} dispatch={dispatch}>
        <Dropdown>
          <label className="block">
            <span className="text-gray-700">Items Per Page</span>
            <input
              type="number"
              className="form-input mt-1 block w-full"
              min={1}
              max={items.length}
              onChange={onChange}
              placeholder="Number"
              defaultValue={itemsPerPage}
            />
          </label>
        </Dropdown>
      </BoxHeader>
      <div>
        <GalleryItems dimensions={dimensions} items={currentItems} />
        <Pagination
          itemsPerPage={itemsPerPage}
          indexOfFirstItem={indexOfFirstItem}
          indexOfLastItem={indexOfLastItem}
          totalItems={items.length}
          paginate={paginate}
          paginateToNextPage={paginateToNextPage}
          paginateToPrevPage={paginateToPrevPage}
          dimensions={dimensions}
        />
      </div>
    </div>
  );
}
export default AssortmentMix;
