import React from "react";
import { LeftChevronIcon, RightChevronIcon } from "../../Icons";

function Pagination({
  totalItems,
  indexOfFirstItem,
  indexOfLastItem,
  itemsPerPage,
  paginate,
  paginateToNextPage,
  paginateToPrevPage,
  dimensions,
}) {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  const smWidth = dimensions.width <= 640;

  return (
    <div className="bg-white w-full px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
      <div
        className={`flex-1 flex justify-between ${
          smWidth ? "block" : "hidden"
        }`}
      >
        <button
          className="relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm leading-5 font-medium rounded-md text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
          onClick={paginateToPrevPage}
        >
          Previous
        </button>
        <button
          className="ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm leading-5 font-medium rounded-md text-gray-700 bg-white hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
          onClick={paginateToNextPage}
        >
          Next
        </button>
      </div>
      <div
        className={`${
          smWidth ? "hidden" : "flex flex-1 items-center justify-between"
        }`}
      >
        <div>
          <p className="text-sm leading-5 text-gray-700">
            Showing <span className="font-medium">{indexOfFirstItem + 1}</span>{" "}
            to <span className="font-medium">{indexOfLastItem}</span> of{" "}
            <span className="font-medium">{totalItems}</span> results
          </p>
        </div>
        <div>
          <nav className="relative z-0 inline-flex shadow-sm">
            <button
              className="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150"
              aria-label="Previous"
              onClick={paginateToPrevPage}
            >
              <LeftChevronIcon className="h-5 w-5" title="Previous" />
            </button>
            {pageNumbers.map((number, index) => (
              <button
                key={index}
                className="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                onClick={() => paginate(number)}
              >
                {number}
              </button>
            ))}
            <button
              className="-ml-px relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm leading-5 font-medium text-gray-500 hover:text-gray-400 focus:z-10 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150"
              aria-label="Next"
              onClick={paginateToNextPage}
            >
              <RightChevronIcon className="h-5 w-5" title="Next" />
            </button>
          </nav>
        </div>
      </div>
    </div>
  );
}

export default Pagination;
