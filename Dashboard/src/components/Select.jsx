import React from "react";

function Select({
  value = "",
  options,
  id,
  label,
  className,
  placeholder = "Select an option",
  onClearFilter,
  isClearable = false,
  onChange,
}) {
  const handleChange = (event) => {
    onChange(event.target.value);
  };

  const showClearButton = isClearable && value !== "";
  const clearFilter = () => onClearFilter(id);

  return (
    <div className={className}>
      <span className="text-sm leading-5 font-medium text-gray-700">
        {label}
      </span>
      {/* Used for clearing filters */}
      {showClearButton && (
        <button
          className="text-xs leading-7 font-medium text-gray-700 float-right"
          onClick={clearFilter}
        >
          Clear
        </button>
      )}
      <select
        className="form-select mt-1 block w-full"
        value={value}
        onChange={handleChange}
      >
        <option value="" disabled>
          {placeholder}
        </option>
        {options.map(({ label: text, value }) => (
          <option key={value} value={value}>
            {text}
          </option>
        ))}
      </select>
    </div>
  );
}

export default Select;
