import React from "react";
import { Responsive, WidthProvider } from "react-grid-layout";
import Wrapper from "./Wrapper";
import Header from "./Header";
import { COMPONENTS } from "../config";
const ResponsiveReactGridLayout = WidthProvider(Responsive);

function MainContent({
  layouts,
  currentBreakpoint,
  boxes,
  filtersMapping,
  globalFilters,
  dispatch,
}) {
  const generateBoxes = () => {
    return layouts[currentBreakpoint].map((layout) => {
      const boxId = layout.i;
      const boxConfig = boxes[boxId];
      const Box = COMPONENTS[boxId].boxComponent;
      return (
        // This wrapper is used to pass dynamically
        // the height and width (in pixels) of each box
        // look at this issue for more informations
        // https://github.com/STRML/react-grid-layout/issues/14
        <Wrapper key={boxId}>
          <Box
            layout={layout}
            boxConfig={boxConfig}
            filtersMapping={filtersMapping}
            globalFilters={globalFilters}
            dispatch={dispatch}
          />
        </Wrapper>
      );
    });
  };

  const handLayoutChange = (_, layouts) => {
    dispatch({ type: "UPDATE_LAYOUT", layouts });
  };

  const handleBreakpointChange = (breakpoint) => {
    dispatch({ type: "CHANGE_BREAKPOINT", breakpoint });
  };

  return (
    <div className="w-11/12 bg-gray-100 overflow-y-scroll">
      <Header filtersMapping={filtersMapping} globalFilters={globalFilters} />
      <ResponsiveReactGridLayout
        rowHeight={30}
        containerPadding={[16, 16]}
        layouts={layouts}
        onBreakpointChange={handleBreakpointChange}
        onLayoutChange={handLayoutChange}
      >
        {generateBoxes()}
      </ResponsiveReactGridLayout>
    </div>
  );
}

export default MainContent;
