import React from "react";
import Sidebar from "./Sidebar";
import MainContent from "./MainContent";
import { useStickyState } from "../hooks/use-sticky-state";
import dashboardReducer, { initialState } from "./dashboardReducer";
import "./Dashboard.css";

function Dashboard() {
  const [
    {
      currentBreakpoint,
      toolbox,
      layouts,
      boxes,
      filtersMapping,
      globalFilters,
    },
    dispatch,
  ] = useStickyState(dashboardReducer, initialState, "dashboard");

  return (
    <div className="flex h-screen">
      <Sidebar
        items={toolbox[currentBreakpoint]}
        boxes={boxes}
        dispatch={dispatch}
      />
      <MainContent
        layouts={layouts}
        currentBreakpoint={currentBreakpoint}
        boxes={boxes}
        filtersMapping={filtersMapping}
        globalFilters={globalFilters}
        dispatch={dispatch}
      />
    </div>
  );
}

export default Dashboard;
