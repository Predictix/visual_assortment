import React, { useState, useEffect, useRef } from "react";
import Transition from "./Transition";
import useClickOutside from "../hooks/use-click-outside";
import { SettingsIcon } from "./Icons";

export default function Dropdown({
  open = false,
  icon,
  onToggle = () => {},
  children,
  className = "relative",
}) {
  const [isOpen, setIsOpen] = useState(open);
  const elRef = useRef(null);

  const toggle = () => {
    onToggle(!isOpen);
    setIsOpen(!isOpen);
  };
  const close = () => setIsOpen(false);

  useEffect(() => {
    setIsOpen(open);
  }, [open]);

  useClickOutside(elRef, close);

  return (
    <div ref={elRef} className={`inline-block text-left ${className}`}>
      <button
        className="flex items-center text-gray-400 hover:text-gray-600 focus:outline-none focus:text-gray-600"
        onClick={toggle}
      >
        {icon || <SettingsIcon className="h-6 w-6" title="Settings" />}
      </button>
      <Transition
        show={isOpen}
        enter="transition ease-out duration-100 transform"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="transition ease-in duration-75 transform"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <div className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg z-10">
          <div className="rounded-md bg-white shadow-xs">
            <div className="p-2">{children}</div>
          </div>
        </div>
      </Transition>
    </div>
  );
}
