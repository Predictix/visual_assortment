import React from "react";

function Wrapper(props) {
  return (
    <div {...props}>
      {React.Children.map(props.children, (child) => {
        return React.cloneElement(child, {
          dimensions: {
            width: parseInt(props.style.width, 10),
            height: parseInt(props.style.height, 10),
          },
        });
      })}
    </div>
  );
}

export default Wrapper;
