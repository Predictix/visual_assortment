import React from "react";

function Button({ onClick, className, children }) {
  return (
    <button
      className={`${className} bg-white ml-auto hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 inline-flex items-center border border-gray-400 rounded shadow`}
      onClick={onClick}
    >
      {children}
    </button>
  );
}

export default Button;
