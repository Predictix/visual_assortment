import React, { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import Button from "./Button";
import Select from "./Select";
import { FiltersIcon, ClearFiltersIcon } from "./Icons";
import { getFilterData } from "../api/FiltersAPI";

function Header({ filtersMapping, globalFilters }) {
  const [filtersList, setFiltersList] = useState([]);
  const [showFilters, setShowFilters] = useState(false);
  let [searchParams, setSearchParams] = useSearchParams();

  useEffect(() => {
    let ignore = false;

    async function fetchFiltersOptions() {
      const filtersList = await Promise.all(
        globalFilters.map((filter) => getFilterData(filter, filtersMapping))
      );

      if (!ignore) {
        setFiltersList(filtersList);
      }
    }

    fetchFiltersOptions();
    return () => {
      ignore = true;
    };
  }, []);

  const handleClearFilter = (key) => {
    searchParams.delete(key);
    setSearchParams(searchParams);
  };

  const handleClearAllFilters = () => {
    setSearchParams();
  };

  const changeFilter = (key, value) => {
    searchParams.set(key, value);
    const { drilldownTo } = filtersList.find((filter) => filter.id === key);
    if (drilldownTo) {
      searchParams.set("splitBy", drilldownTo);
    }
    setSearchParams(searchParams);
  };

  return (
    <div className="p-2">
      <Button onClick={() => setShowFilters(!showFilters)}>
        <FiltersIcon className="h-5 w-5 mr-1" title="Filter" />
        <span>Filters</span>
      </Button>
      {showFilters && (
        <div className="p-1">
          <div className="flex">
            {filtersList.map(({ id, level, options }) => (
              <Select
                key={id}
                id={id}
                className="mr-4"
                label={level}
                value={searchParams.get(id) || ""}
                isClearable={true}
                options={options}
                onClearFilter={handleClearFilter}
                onChange={(value) => changeFilter(id, value)}
              />
            ))}
          </div>
          <br />
          <Button onClick={handleClearAllFilters}>
            <ClearFiltersIcon className="mr-2" />
            <span>Clear Filters</span>
          </Button>
        </div>
      )}
    </div>
  );
}

export default Header;
