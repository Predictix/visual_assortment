import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { ReactComponent as XCircleIcon } from "../icons/x-circle.svg";
import { ReactComponent as FilterIcon } from "../icons/filter.svg";
export { ReactComponent as AssortmentMixIcon } from "../icons/photograph.svg";
export { ReactComponent as BuyPlanReviewIcon } from "../icons/chart-bar.svg";
export { ReactComponent as ContributionAnalysisIcon } from "../icons/chart-pie.svg";
export { ReactComponent as FilteringTreemapIcon } from "../icons/treemap.svg";
export { ReactComponent as StatsIcon } from "../icons/document-report.svg";
export { ReactComponent as FiltersIcon } from "../icons/adjustments.svg";
export { ReactComponent as SettingsIcon } from "../icons/cog.svg";
export { ReactComponent as CompanyLogo } from "../icons/Infor_logo.svg";
export { ReactComponent as RightChevronIcon } from "../icons/chevron-right.svg";
export { ReactComponent as LeftChevronIcon } from "../icons/chevron-left.svg";
export { ReactComponent as CloseWidgetIcon } from "../icons/times.svg";
export { ReactComponent as UpdateWidgetIcon } from "../icons/pencil-alt.svg";

export function ClearFiltersIcon() {
  return (
    <span className="relative mr-2">
      <FilterIcon className="h-6 w-6" />
      <XCircleIcon className="h-4 w-4 absolute" style={{ top: 13, left: 11 }} />
    </span>
  );
}

export function LoadingIcon() {
  return <FontAwesomeIcon className="block" icon={faSpinner} spin size="lg" />;
}
