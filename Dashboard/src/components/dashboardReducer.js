import initialState from "../config/layout.json";

function dashboardReducer(state, action) {
  switch (action.type) {
    // Add a Box to the layout and remove it from the Sidebar
    case "TAKE_ITEM":
      return {
        ...state,
        toolbox: {
          ...state.toolbox,
          [state.currentBreakpoint]: state.toolbox[
            state.currentBreakpoint
          ].filter(({ i }) => i !== action.item.i),
        },
        layouts: {
          ...state.layouts,
          [state.currentBreakpoint]: [
            ...state.layouts[state.currentBreakpoint],
            action.item,
          ],
        },
      };
    // Remove a Box from the layout and add it to the Sidebar
    case "PUT_ITEM":
      return {
        ...state,
        toolbox: {
          ...state.toolbox,
          [state.currentBreakpoint]: [
            ...(state.toolbox[state.currentBreakpoint] || []),
            action.item,
          ],
        },
        layouts: {
          ...state.layouts,
          [state.currentBreakpoint]: state.layouts[
            state.currentBreakpoint
          ].filter(({ i }) => i !== action.item.i),
        },
      };
    // Change the breakpoint when the viewport is changed
    case "CHANGE_BREAKPOINT":
      return {
        ...state,
        currentBreakpoint: action.breakpoint,
        toolbox: {
          ...state.toolbox,
          [action.breakpoint]:
            state.toolbox[action.breakpoint] ||
            state.toolbox[state.currentBreakpoint] ||
            [],
        },
      };
    // Update the layout when you resize the boxes
    case "UPDATE_LAYOUT":
      return {
        ...state,
        layouts: action.layouts,
      };
    // Add a widget to the contribution analysis box
    case "ADD_WIDGET":
      return {
        ...state,
        boxes: {
          ...state.boxes,
          [action.boxId]: {
            ...state.boxes[action.boxId],
            widgets: [...state.boxes[action.boxId].widgets, action.data],
          },
        },
      };
    // Update a widget from the contribution analysis box
    case "UPDATE_WIDGET":
      return {
        ...state,
        boxes: {
          ...state.boxes,
          [action.boxId]: {
            ...state.boxes[action.boxId],
            widgets: state.boxes[action.boxId].widgets.map((widget) =>
              widget.id === action.data.id ? action.data : widget
            ),
          },
        },
      };
    // Remove a widget from the contribution analysis box
    case "REMOVE_WIDGET":
      return {
        ...state,
        boxes: {
          ...state.boxes,
          [action.boxId]: {
            ...state.boxes[action.boxId],
            widgets: state.boxes[action.boxId].widgets.filter(
              (widget) => widget.id !== action.id
            ),
          },
        },
      };
    // Set the number of items displayed in the Assortment Mix box
    case "SET_ITEMS_PER_PAGE":
      console.log(action);
      return {
        ...state,
        boxes: {
          ...state.boxes,
          [action.boxId]: {
            ...state.boxes[action.boxId],
            itemsPerPage: action.itemsPerPage,
          },
        },
      };
    default:
      throw new Error();
  }
}

export { initialState };
export default dashboardReducer;
