import React from "react";
import { CompanyLogo } from "./Icons";
import { COMPONENTS } from "../config";

export default function Sidebar({ items, boxes, dispatch }) {
  return (
    <div className="w-1/12 flex flex-col items-center justify-center relative bg-gray-900">
      <CompanyLogo
        className="h-12 w-12 absolute"
        style={{ top: 13 }}
        title="Company Logo"
      />
      <ul className="flex flex-col">
        {items.map((item) => {
          const Icon = COMPONENTS[item.i].iconComponent;
          const title = boxes[item.i].label;
          return (
            <li
              key={item.i}
              className="cursor-pointer p-1"
              onClick={() => dispatch({ type: "TAKE_ITEM", item })}
            >
              <Icon title={title} className="h-10 w-10 text-white" />
            </li>
          );
        })}
      </ul>
    </div>
  );
}
