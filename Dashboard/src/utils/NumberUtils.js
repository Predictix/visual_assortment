function generateId() {
  return Math.random().toString(36).substring(7);
}

function generateRandomNumber(min, max) {
  return Math.floor(Math.random() * max) + min;
}

export default {
  generateId,
  generateRandomNumber,
};
