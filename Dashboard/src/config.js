import {
  AssortmentMix,
  BuyPlanReview,
  ContributionAnalysis,
  FilteringTreemap,
  Stats,
} from "./components/Boxes";
import {
  AssortmentMixIcon,
  BuyPlanReviewIcon,
  FilteringTreemapIcon,
  StatsIcon,
  ContributionAnalysisIcon,
} from "./components/Icons";

export const COMPONENTS = {
  "assortment-mix": {
    boxComponent: AssortmentMix,
    iconComponent: AssortmentMixIcon,
  },
  "contribution-analysis": {
    boxComponent: ContributionAnalysis,
    iconComponent: ContributionAnalysisIcon,
  },
  "buy-plan-review": {
    boxComponent: BuyPlanReview,
    iconComponent: BuyPlanReviewIcon,
  },
  "filtering-treemap": {
    boxComponent: FilteringTreemap,
    iconComponent: FilteringTreemapIcon,
  },
  stats: {
    boxComponent: Stats,
    iconComponent: StatsIcon,
  },
};
