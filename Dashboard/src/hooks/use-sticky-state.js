// https://joshwcomeau.com/react/persisting-react-state-in-localstorage/
import { useReducer, useEffect } from "react";

export function useStickyState(reducer, defaultValue, key) {
  const [state, dispatch] = useReducer(
    reducer,
    defaultValue,
    (defaultValue) => {
      const stickyValue = window.localStorage.getItem(key);
      return stickyValue !== null ? JSON.parse(stickyValue) : defaultValue;
    }
  );

  useEffect(() => {
    window.localStorage.setItem(key, JSON.stringify(state));
  }, [key, state]);

  return [state, dispatch];
}
