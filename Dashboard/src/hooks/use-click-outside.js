// https://youtu.be/J-g9ZJha8FE?t=696
import { useEffect, useRef } from "react";

function useClickOutside(elRef, callback) {
  const callbackRef = useRef();
  callbackRef.current = callback;

  useEffect(() => {
    function handleClickOutside(event) {
      if (!elRef?.current.contains(event.target)) {
        callbackRef.current(event);
      }
    }

    // Bind the event listener
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, [elRef, callback]);
}

export default useClickOutside;
