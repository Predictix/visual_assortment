import {
  getMQG,
  getFiltersList,
  getMeasureLabel,
  getMeasuresList,
} from "../api/utils";

export async function getMeasures() {
  const MQG = await getMQG();
  return getMeasuresList(MQG);
}

async function getAttributeLabel(MQG, id, namedType = "Concept.Silhouette") {
  const data = await MQG.QueryView()
    .measure("value", `${namedType}.label`)
    .groupBy(namedType)
    .diceBy(`${namedType}.id=${id}`)
    .get();

  return data[0].value;
}

export async function getMeasureValue(
  { attribute, measure },
  params,
  filtersMapping
) {
  const MQG = await getMQG();

  // Get the named type of a measure
  const namedType = MQG.builder
    .Measure(attribute)
    .getType()
    .named.replace(/:/g, ".");

  const filters = getFiltersList(params, filtersMapping);

  const data = await MQG.QueryView()
    .measure("attribute", attribute)
    .measure("value", measure)
    .groupBy("Product.StyleColor")
    .diceBy(filters)
    .get();

  // Get the label of each attribute
  let updatedData = [];
  for (let element of data) {
    const label = await getAttributeLabel(MQG, element.attribute, namedType);
    updatedData = [...updatedData, { ...element, label }];
  }

  // Aggregate by Attribute
  return Object.values(
    updatedData.reduce((accumulator, { attribute, label, value }) => {
      value = parseInt(value);
      return {
        ...accumulator,
        [attribute]: {
          attribute,
          name: label,
          value: (accumulator[attribute]?.value || 0) + value,
        },
      };
    }, {})
  );
}

export async function getTreemapData(
  sizeMetric,
  colorsMetric,
  params,
  filtersMapping
) {
  const MQG = await getMQG();
  const splitBy = params.get("splitBy");

  const filters = getFiltersList(params, filtersMapping);
  const { level, dimension, drilldownTo } = filtersMapping[splitBy];

  let data = await MQG.QueryView()
    .attribute("id", `${dimension}.${level}.id`)
    .attribute("label", `${dimension}.${level}.label`)
    .measure("colorsMetricValue", colorsMetric)
    .measure("sizeMetricValue", sizeMetric)
    .groupBy(`${dimension}.${level}`)
    .diceBy([...filters, `${sizeMetric}>0`])
    .get();

  return data.map(({ id, label, sizeMetricValue, colorsMetricValue }) => ({
    id,
    label,
    filterKey: splitBy,
    drilldownTo,
    value: parseInt(sizeMetricValue),
    sizeMetric: {
      label: getMeasureLabel(MQG, sizeMetric),
      value: parseInt(sizeMetricValue),
    },
    colorsMetric: {
      label: getMeasureLabel(MQG, colorsMetric),
      value: parseInt(colorsMetricValue),
    },
  }));
}
