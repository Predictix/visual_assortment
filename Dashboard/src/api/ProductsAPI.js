import { getMQG, getFiltersList } from "../api/utils";

export async function getStyleColors(params, filtersMapping) {
  const MQG = await getMQG();

  const filters = getFiltersList(params, filtersMapping);

  const data = await MQG.QueryView()
    .attribute("id", "Product.StyleColor.id")
    .attribute("label", "Product.StyleColor.label")
    .measure("description", "StyleColorDescription_S_WP_PL_AP")
    .measure("image", "StyleColorImage_S_WP_PL_AP")
    .groupBy("Product.StyleColor")
    .diceBy(filters)
    .get();

  return data;
}
