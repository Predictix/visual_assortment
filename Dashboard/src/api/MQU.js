/* eslint-disable */
function _interopDefault(ex) {
  return ex && typeof ex === "object" && "default" in ex ? ex["default"] : ex;
}

var url = _interopDefault(require("url"));
var https = _interopDefault(require("https"));
var http = _interopDefault(require("http"));

var deepClone = function (obj) {
  if (obj == null || typeof obj !== "object") {
    //jshint ignore:line
    return obj;
  }

  var temp = obj.constructor(); // changed

  for (var key in obj) {
    temp[key] = deepClone(obj[key]);
  }
  return temp;
};
// simple deep Equality (does not handle circular references)
var equals = function (x, y) {
  if (x === y) {
    return true;
  }
  // jshint ignore:start
  // if both x and y are null or undefined and exactly the same
  if (!(x instanceof Object) || !(y instanceof Object)) return false;
  // if they are not strictly equal, they both need to be Objects
  if (x.constructor !== y.constructor) return false;
  // they must have the exact same prototype chain, the closest we can do is
  // test there constructor.
  for (var p in x) {
    if (!x.hasOwnProperty(p)) continue;
    // other properties were tested using x.constructor === y.constructor
    if (!y.hasOwnProperty(p)) return false;
    // allows to compare x[ p ] and y[ p ] when set to undefined
    if (x[p] === y[p]) continue;
    // if they have the same strict value or identity then they are equal
    if (typeof x[p] !== "object") return false;
    // Numbers, Strings, Functions, Booleans must be strictly equal
    if (!equals(x[p], y[p])) return false;
    // Objects and Arrays must be tested recursively
  }
  for (p in y) {
    if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) return false;
    // allows x[ p ] to be set to undefined
  }
  // jshint ignore:end
  return true;
};
// assert that argument is an array
var toArray = function (s) {
  var result = [];
  if (Array.isArray(s)) {
    result = s;
  } else if (s !== undefined && s !== null) {
    result.push(s);
  }
  return result;
};

//capitalize a word
var capitalize = function (s) {
  if (!s) {
    return s;
  }
  if (typeof s !== "string") {
    throw "MQU _capitalize: argument is not of type string:" + s;
  }
  return s.charAt(0).toUpperCase() + s.slice(1);
};

// A modified version of https://gist.github.com/814052/690a6b41dc8445479676b347f1ed49f4fd0b1637
function Promise() {
  this._thens = [];
}

Promise.prototype = {
  /* This is the "front end" API. */

  // then(onResolve, onReject): Code waiting for this promise uses the
  // then() method to be notified when the promise is complete. There
  // are two completion callbacks: onReject and onResolve. A more
  // robust promise implementation will also have an onProgress handler.
  then: function (onResolve, onReject) {
    // capture calls to then()
    this._thens.push({
      resolve: onResolve,
      reject: onReject,
    });
    return this;
  },

  // Some promise implementations also have a cancel() front end API that
  // calls all of the onReject() callbacks (aka a "cancelable promise").
  // cancel: function (reason) {},

  /* This is the "back end" API. */

  // resolve(resolvedValue): The resolve() method is called when a promise
  // is resolved (duh). The resolved value (if any) is passed by the resolver
  // to this method. All waiting onResolve callbacks are called
  // and any future ones are, too, each being passed the resolved value.
  resolve: function (val) {
    this._complete("resolve", val);
  },

  // reject(exception): The reject() method is called when a promise cannot
  // be resolved. Typically, you'd pass an exception as the single parameter,
  // but any other argument, including none at all, is acceptable.
  // All waiting and all future onReject callbacks are called when reject()
  // is called and are passed the exception parameter.
  reject: function (ex) {
    this._complete("reject", ex);
  },

  // Some promises may have a progress handler. The back end API to signal a
  // progress "event" has a single parameter. The contents of this parameter
  // could be just about anything and is specific to your implementation.
  // progress: function (data) {},

  /* "Private" methods. */

  _complete: function (which, arg) {
    // switch over to sync then()
    this.then =
      which === "resolve"
        ? function (resolve /*, reject*/) {
            resolve(arg);
          }
        : function (resolve, reject) {
            reject(arg);
          };
    // disallow multiple calls to resolve or reject
    this.resolve = this.reject = function () {
      // Don't throw an exception anymore if a user attempt to resolve/reject an already completed promise
      // this is to avoid extra conditions from the user side whenever a promise can be resolved/rejected from many places,
      // which is not a good thing to do, but there are some cases where this is needed (like a httprequest promise where the user can abort the request from his end)
      //throw new Error('Promise already completed.');
    };
    // complete all waiting (async) then()s
    var aThen,
      i = 0;
    while ((aThen = this._thens[i++])) {
      //jshint ignore:line
      if (aThen[which]) {
        arg = aThen[which](arg);
      }
    }
    delete this._thens;
  },
};

var _xhr = function (type, cHeaders, data) {
  var request = new XMLHttpRequest();
  request.open(type, cHeaders.url, true);
  request.setRequestHeader("Content-type", "application/json;charset=UTF-8");
  var promise = new Promise();
  request.onreadystatechange = function () {
    if (request.readyState === 4) {
      if (request.status >= 200 && request.status < 300) {
        promise.resolve(request.responseText);
      } else {
        promise.reject({
          status: request.status,
          responseText: request.responseText,
        });
      }
    }
  };
  request.send(data);
  return promise.then(
    function (data) {
      return data;
    },
    function (err) {
      // if the promise is rejected from the caller, abort the request
      request.abort();
      return err;
    }
  );
};
var _postJSONNode = function (type, cHeaders, data) {
  var urlObj = url.parse(cHeaders.url);
  var http$1 = urlObj.protocol === "https:" ? https : http;
  var host = urlObj.hostname;
  var port = urlObj.port || 8080;
  var path = urlObj.path;

  var cookie = null;
  if (cHeaders.session) {
    if (cHeaders.session.indexOf("=") > -1) {
      cookie = cHeaders.session;
    } else {
      cookie = "auth_session_key=" + cHeaders.session;
    }
  }
  var headers = {
    "Content-Type": "application/json",
    "Content-Length": data.length,
    Cookie: cookie,
    Accept: "*/*",
  };

  var promise = new Promise();

  var options = {
    host: host,
    port: port,
    path: path,
    method: "POST",
    headers: headers,
  };
  var req = http$1.request(options, function (res) {
    res.setEncoding("utf-8");

    var responseString = "";
    res.on("data", function (data) {
      responseString += data;
    });
    res.on("end", function () {
      var statusCode = res.statusCode;
      if (statusCode !== 200) {
        promise.reject(
          "Unexpected HTTP response status code: " +
            statusCode +
            ", " +
            responseString
        );
      } else {
        promise.resolve(responseString);
      }
    });
  });
  req.on("error", function (e) {
    promise.reject(e);
  });
  req.write(data);
  req.end();

  return promise.then(
    function (data) {
      return data;
    },
    function (err) {
      // if the promise is rejected from the caller, abort the request
      req.abort();
      return err;
    }
  );
};

var longNumberColumnRegExp = /{"(int128_column|decimal_column)": ?({[^}]+})}/g;

var _hasLongNumberColumns = function (response) {
  return new RegExp(longNumberColumnRegExp).test(response);
};

var _getLongNumberColumns = function (response) {
  var columnRegExp = new RegExp(longNumberColumnRegExp);

  var columns = [];

  do {
    var m = columnRegExp.exec(response);

    if (m) {
      columns.push(m[2]);
    }
  } while (m);

  return columns;
};

var _convertLongNumberColumns = function (response) {
  var columns = _getLongNumberColumns(response);

  if (columns.length === 0) {
    return response;
  }

  for (var i = 0; i < columns.length; i++) {
    var convertedColumn = columns[i].replace(/(\d+)/g, '"$1"');

    response = response.replace(columns[i], convertedColumn);
  }

  return response;
};

// A primitive cross Browser/Node implementation of a HTTP post request
var postJSON = function (type, headers, data) {
  var parse = function (res) {
    var result;
    try {
      if (_hasLongNumberColumns(res)) {
        // int128 or decimal column values are int64 and their value can exceed JavaScript's number precision (up to 15 digits),
        // e.g. 11126578817399144738 becomes 11126578817399144000
        // before response string is parsed to JSON, we're converting them to strings to preserve all digits
        res = _convertLongNumberColumns(res);
      }

      result = JSON.parse(res);
    } catch (e) {
      result = res;
    }
    return result;
  };

  if (isNode()) {
    return _postJSONNode(type, headers, JSON.stringify(data)).then(function (
      data
    ) {
      return parse(data);
    });
    // } else if (isNashorn()) {
    // var response = httpPost(headers.url, JSON.stringify(data));
    // return parse(response.data);
    // return fdfdfd();
  } else {
    return _xhr(type, headers, JSON.stringify(data)).then(function (data) {
      return parse(data);
    });
  }
};

/*
  Compatibility with Java's Nashorn engine for running JavaScript
*/
// function isNashorn() {
//   return typeof Java !== "undefined" && Java && typeof Java.type === "function";
// };

function isNode() {
  if (typeof process !== "undefined" && !process.browser) {
    return true;
  }
  return false;
}

var sortBasedOnArray = function (levels, distribution) {
  var headers = [];
  for (var i = 0; i < distribution.length; i++) {
    var dim = distribution[i].split(".")[0];
    if (dim in levels) {
      headers.push(dim + "." + levels[dim]);
    }
  }
  return headers;
};

Function.prototype.curry = function () {
  var fn = this,
    args = Array.prototype.slice.call(arguments);
  return function () {
    return fn.apply(this, args.concat(Array.prototype.slice.call(arguments)));
  };
};

function toCSV(data, collectDelimiter) {
  if (data && typeof data === "object" && !Array.isArray(data)) {
    data = [data];
  }

  if (typeof data === "undefined") {
    return;
  }

  // check if value is primitive data
  if (Object(data) !== data) {
    return ["SCALAR", data].join("\n");
  }

  var result = [];
  // This should cover all edge cases
  // The idea is that we take the first record and assume that
  // it has all the columns that should be in the header
  // then we iterate through the rest of the records and
  // if there is a new column that we haven't accounted for, we
  // append that to the header
  var headers = data
    .map(function (record) {
      return Object.keys(record);
    })
    .reduce(function (prev, curr) {
      return prev.concat(
        curr.filter(function (col) {
          return prev.indexOf(col) < 0;
        })
      );
    }, []);
  var DEFAULT_COLLECT_DELIMITER = "|";

  result.push(headers.join(","));
  for (var i = 0; i < data.length; i++) {
    var vals = headers.map(function (col) {
      if (data[i][col] == null) {
        return "";
      }
      var value = data[i][col];
      return Array.isArray(value)
        ? value.join(collectDelimiter || DEFAULT_COLLECT_DELIMITER)
        : value;
    });
    result.push(vals.join(","));
  }
  return result.join("\n");
}

var arrayIndexOf = function (arr, fn) {
  if (typeof fn === "function") {
    for (var i = 0; i < arr.length; i++) {
      if (fn(arr[i])) {
        return i;
      }
    }
    return -1;
  }
  return arr.indexOf(fn);
};

var FilterValueKind = {
  Old: "old",
  Simple: "simple",
  Composite: "composite",
};

var getFilterValueKind = function (filterValue) {
  if (typeof filterValue === "object" && filterValue.op && filterValue.value) {
    return FilterValueKind.Old;
  } else if (
    typeof filterValue === "object" &&
    !(filterValue instanceof Array)
  ) {
    return FilterValueKind.Composite;
  } else {
    return FilterValueKind.Simple;
  }
};

var constructUrl = function (
  method,
  view,
  levels,
  filters,
  sourceSpread,
  spreadMethod
) {
  var path = "/" + method + "/" + view + "?";
  for (var key in levels) {
    path += key + "level=" + levels[key] + "&";
  }

  var i, j;
  var filterId, operator, filterValues, filterValue;
  for (filterId in filters) {
    var v = filters[filterId];
    var filterValueKind = getFilterValueKind(v);
    switch (filterValueKind) {
      default:
        break;
      case FilterValueKind.Old: {
        path += filterId + v.op + encodeURIComponent(v.value) + "&";
        break;
      }
      case FilterValueKind.Composite: {
        for (operator in filters[filterId]) {
          filterValues = filters[filterId][operator];
          for (i = 0; i < filterValues.length; i++) {
            filterValue = filterValues[i];
            path += filterId + operator + encodeURIComponent(filterValue) + "&";
          }
        }
        break;
      }
      case FilterValueKind.Simple: {
        if (!Array.isArray(v)) {
          v = [v];
        }
        for (i = 0; i < v.length; i++) {
          path += filterId + "=" + encodeURIComponent(v[i]) + "&";
        }
        break;
      }
    }
  }

  //encode source
  if (sourceSpread) {
    for (j = 1; j < sourceSpread.length; j++) {
      for (i = 0; i < sourceSpread[j].length; i++) {
        if (typeof sourceSpread[j][i] === "string") {
          sourceSpread[j][i] = encodeURIComponent(sourceSpread[j][i]);
        }
      }
    }
  }

  if (spreadMethod) {
    path += "method=" + spreadMethod + "&";
  }

  if (path[path.length - 1] === "&" || path[path.length - 1] === "?") {
    path = path.substring(0, path.length - 1);
  }
  if (sourceSpread) {
    path += ":" + JSON.stringify(sourceSpread);
  }
  return path;
};

var util = {
  isNode: isNode,
  equals: equals,
  toArray: toArray,
  capitalize: capitalize,
  postJSON: postJSON,
  deepClone: deepClone,
  sortBasedOnArray: sortBasedOnArray,
  toCSV: toCSV,
  arrayIndexOf: arrayIndexOf,
  constructUrl: constructUrl,
  Promise: Promise,
};

function createCommonjsModule(fn, module) {
  return (module = { exports: {} }), fn(module, module.exports), module.exports;
}

var MQG = createCommonjsModule(function (module, exports) {
  /**
   * This module defines a collection of builder classes using which a client may construct
   *      complex measure expressions using a convenient and readable object-oriented API.
   *  In addition, the root builder class (called 'Expr') provides methods that allow
   *      for the convenient bottom-up construction of complex measure expressions in
   *      a chaining syntax that obviates the need for repeated calls to 'new' for each
   *      node in the resulting measure expr. For instance, one could define an agg expr
   *      using either the following syntax:
   *
   *          new AggExpr( new MetricExpr('sales'), method, grouping )
   *
   *      or the following, which uses the bottom-up chaining syntax:
   *
   *          new MetricExpr('sales').aggBy(method, grouping)
   *
   * @module MQG
   *
   */

  (function (name, factory) {
    // Install this module using one of three different methods

    {
      // Install using the Node API
      module.exports = factory();
    }
  })("MQG", function () {
    //jshint ignore:line
    var _extendClass = function (Subclass, Superclass) {
      Subclass.prototype = Object.create(Superclass.prototype);
      Subclass.prototype.constructor = Subclass;
    };
    var _sanitizeStr = function (str) {
      return str.toUpperCase().trim();
    };
    /**
     * @param {*} arr
     * @param {(Function|Function[])} constructors
     * @param {(undefined|String)} msg
     */
    var assert = function (cond, message) {
      message = message || "";
      if (!cond) {
        throw new SyntaxError("Assertion failed: " + message);
      }
    };
    /**
     * Assert that 'arr' is an array of elements constructed by constructors
     *
     * @param {*} arr
     * @param {(Function|Function[])} constructors
     * @param {(undefined|String)} msg
     */
    var assertIsArray = function (arr, constructors, msg) {
      var checkElement;
      if (typeof constructors === "function") {
        checkElement = function (element) {
          return element instanceof constructors;
        };
      } else {
        assert(Array.isArray(constructors));
        checkElement = function (element) {
          return constructors.some(function (constructor) {
            return element instanceof constructor;
          });
        };
      }
      assert(
        Array.isArray(arr) && (arr.length === 0 || checkElement(arr[0])),
        msg
      );
    };
    // assert that argument is an array
    var _toArray = function (s) {
      var result = [];
      if (Array.isArray(s)) {
        result = s;
      } else if (s !== undefined && s !== null) {
        result.push(s);
      }
      return result;
    };

    var _padEnd = function (str, targetLength, padString) {
      padString = padString || " ";

      while (str.length < targetLength) {
        str += padString;
      }

      return str;
    };

    var DECIMAL_PART_DIGITS = 18;

    var Type = (function () {
      var _TYPES = [
        "STRING",
        "INT",
        "INT128",
        "FLOAT",
        "DECIMAL",
        "BOOLEAN",
        "NAMED",
        "DATETIME",
      ];
      var _validateType = function (type) {
        type = _sanitizeStr(type);
        if (_TYPES.indexOf(type) === -1) {
          throw new SyntaxError("Invalid type");
        } else {
          return type;
        }
      };
      /**
       * Creates a Type object
       *
       * @class Type
       * @constructor
       * @param {String} kind among _TYPES
       * @param {(String|undefined)} name for NAMED types
       */
      function Type(kind, name) {
        if (typeof kind === "object" && kind.kind) {
          return Type.call(this, kind.kind, kind.named);
        }
        assert(
          typeof kind === "string",
          "kind should be string, but it is currently " + kind
        );
        kind = _validateType(kind);
        assert(
          kind === "NAMED" ? typeof name === "string" : name === undefined
        );
        this.kind = kind;
        if (kind === "NAMED") {
          this.named = name;
        }
      }
      return Type;
    })();
    var ValueType = (function () {
      /**
       * Creates a ValueType object
       *
       * @class ValueType
       * @constructor
       * @param {String} kind 'SINGLETON' or 'SET'
       * @param {Type} type of value
       */
      function ValueType(kind, type) {
        assert(
          typeof kind === "string" && ["SINGLETON", "SET"].indexOf(kind) !== -1
        );
        assert(type instanceof Type);
        this.kind = kind;
        this.type = type;
      }
      return ValueType;
    })();
    var BaseSignature = (function () {
      /**
       * Creates a BaseSignature object
       *
       * @class BaseSignature
       * @constructor
       * @param {Intersection} inter
       * @param {(ValueType|undefined)} type of value (optional)
       */
      function BaseSignature(inter, type) {
        assert(inter instanceof Intersection);
        assert(type instanceof ValueType || typeof type === "undefined");
        this.intersection = inter;
        this.type = type;
      }
      return BaseSignature;
    })();

    var Annotation = (function () {
      /**
       * Creates an Annotation object
       *
       * @class Annotation
       * @constructor
       * @param {String} name
       * @param {Column} [values]
       */
      function Annotation(name, values) {
        assert(typeof name === "string");
        assert(values === undefined || values instanceof Column);

        this.name = name;
        this.values = values;
      }
      return Annotation;
    })();

    var Expr = (function () {
      /**
       * Creates an Expr (Measure Expression) object
       *
       * @class Expr
       * @constructor
       * @param {String} kind Kind of the Measure Exprssion
       * Possible values are:
       * <ul>
       *  <li>ATTRIBUTE</li>
       *  <li>METRIC</li>
       *  <li>FILTER</li>
       *  <li>DICE</li>
       *  <li>AGGREGATION</li>
       *  <li>COMPOSITE</li>
       *  <li>TERM</li>
       *  <li>OP</li>
       *  <li>WIDEN</li>
       *  <li>DROP</li>
       *  <li>DIFFERENCE</li>
       *  <li>COND</li>
       *  <li>DEMOTE</li>
       *  <li>PARAM</li>
       *  <li>PREV</li>
       *  <li>REFINEMENT</li>
       * </ul>
       * @param {Object} val a JSON object that describes the actual Measure Expression
       * @param {String} [str] string-based expr
       */
      function Expr(kind, val, str) {
        /**
         * Reference to the JSON representation of the Expr
         *
         * @private
         * @property _value
         */
        assert(!kind || !str, "kind and str are mutually exclusive");
        if (kind) {
          this.kind = kind.toUpperCase();
          this[kind.toLowerCase()] = val;
        } else if (str) {
          this.str = str;
        }
      }
      /**
       * Convenience post-fix method that uses the FilterExpr builder
       * to filter the current Expr by a conjunction or disjunction of Comparison objects
       *
       * @method filterBy
       * @param {Comparison[]} comparisons an array of Comparison objects
       * @param {Boolean} [isDisjunction=false] set to true to keep values that satisfy any of the comparisons
       * @return {FilterExpr} the Expr resulting from the filtering
       */
      Expr.prototype.filterBy = function (comparisons, type, isDisjunction) {
        return new FilterExpr(this, comparisons, type, isDisjunction);
      };
      /**
       * Convenience post-fix method that uses the DiceExpr builder
       * to filter the current Expr by a conjunction or disjunction of Expr objects
       *
       * @method diceBy
       * @param {Expr[]} dicers an array of Expr objects
       * @param {Boolean} [isDisjunction=false] set to true to keep values that satisfy any of the dicers
       * @return {DiceExpr} the Expr resulting from the dice
       */
      Expr.prototype.diceBy = function (dicers, isDisjunction) {
        if (!_toArray(dicers).length) {
          return this;
        }
        return new DiceExpr(this, dicers, isDisjunction);
      };
      /**
       * Convenience post-fix method that uses the AggExpr builder
       * to aggregate over the current Expr using a given method and an array of Grouping objects
       *
       * @method aggBy
       * @param {String} method aggregation method. It could be one of the following:
       * <p>'COLLECT', 'AMBIG', 'TOTAL', 'MIN', 'MAX', 'COUNT', 'MODE', 'COUNT_DISTINCT', 'HISTOGRAM', 'COVER'</p><
       * @param {(Grouping[]|Intersection)} groupingsOrInter an array of Grouping objects or a target intersection, meaningful(and required) only if the first argument is an aggregation method
       * @return {AggExpr} the Expr resulting from the aggregation
       */
      Expr.prototype.aggBy = function (method, groupingsOrInter) {
        var parseGroupings = function (grps) {
          var output = [];
          var dim;
          for (var i = 0; i < grps.length; i++) {
            dim = grps[i];
            if (dim.indexOf(".") > -1) {
              var el = _parseQualifiedLevel(dim);
              output.push(
                new Grouping(
                  "MAP",
                  el.dim,
                  el.level,
                  undefined,
                  undefined,
                  el.label
                )
              );
            } else {
              output.push(new Grouping("ALL", dim));
            }
          }
          return output;
        };
        if (groupingsOrInter) {
          if (groupingsOrInter instanceof Intersection);
          else if (typeof groupingsOrInter === "string") {
            groupingsOrInter = parseGroupings([groupingsOrInter]);
          } else if (
            Array.isArray(groupingsOrInter) &&
            typeof groupingsOrInter[0] === "string"
          ) {
            //more robust checking
            groupingsOrInter = parseGroupings(groupingsOrInter);
          }
        }
        return new AggExpr(this, method, groupingsOrInter);
      };
      Expr.prototype.widenBy = function (inter) {
        return new WidenExpr(this, inter);
      };
      /**
       * Convenience post-fix method that uses the AggExpr builder to apply a slide.
       * As of LB v4.4.12 only one slide per aggregation is supported.
       *
       * @method slideBy
       * @param {String} method aggregation method. @see {@link aggBy} for details.
       * @param {String} slide the name of a declared Slide to use for grouping.
       * @return {AggExpr} the Expr resulting from the aggregation
       */
      Expr.prototype.slideBy = function (method, slide) {
        if (typeof slide !== "string") {
          throw new SyntaxError(
            "Invalid name of a slide, must be a string: " + slide
          );
        }
        var slideGrouping = new Grouping("SLIDE", undefined, undefined, slide);
        return new AggExpr(this, method, [slideGrouping]);
      };
      /**
       * Convenience post-fix method that creates a <tt>SplitExpr</tt> from the given
       * from the given <tt>labelMaps</tt>.
       *
       * @method splitBy
       * @param  {LabelMap[]} labelMaps a list of label mappings to apply to the expr
       * @return {Expr}                 a SplitExpr
       */
      Expr.prototype.splitBy = function (labelMaps) {
        return new SplitExpr(this, labelMaps);
      };
      Expr.prototype.headerSortBy = function (floatingLevel) {
        return new HeaderSortExpr(this, floatingLevel);
      };
      /**
       * Convenience post-fix method that creates a RelabelExpr from the given
       * labelMaps.
       *
       * @method relabelBy
       * @param  {LabelMap[]} labelMaps a list of label mappings to apply to the expr
       * @return {Expr}                 a RelabelExpr
       */
      Expr.prototype.relabelBy = function (labelMaps) {
        return new RelabelExpr(this, labelMaps);
      };
      /**
       * Convenience post-fix method to demote a dimension.
       *
       * @method demote
       * @param  {String} dimension to demote
       * @return {DemoteExpr}
       */
      Expr.prototype.demote = function (dimension) {
        return new DemoteExpr(this, dimension);
      };
      /**
       * Convenience post-fix method to promote a dimension.
       *
       * @method promote
       * @param  {String} dimension to promote (optional)
       * @return {PromoteExpr}
       */
      Expr.prototype.promote = function (dimension) {
        return new PromoteExpr(this, dimension);
      };
      /**
       * Convenience post-fix method to drop the measure's value.
       *
       * @method drop
       * @return {DropExpr}
       */
      Expr.prototype.drop = function () {
        return new DropExpr(this);
      };

      /**
       * Convenience post-fix method that uses the OpExpr builder with an add operator
       * to add two Exprs
       *
       * @method add
       * @param {Expr} expr the Expr object to be added
       * @return {OpExpr} the OpExpr resulting from the operation
       */
      Expr.prototype.add = function (expr) {
        if (["number", "string"].indexOf(typeof expr) > -1) {
          if (arguments.length === 2) {
            return this.add(new ConstantExpr(expr, arguments[1]));
          } else {
            throw "corresponding measure service type(INT,FLOAT,DECIMAL) is required when the argument is primitive";
          }
        }
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        } else {
          return new OpExpr("+", [this, expr]);
        }
      };
      /**
       * Convenience post-fix method that uses the OpExpr builder with a subtract operator
       * to subtract an Expr from the currrent Expr
       *
       * @method subtract
       * @param {Expr} expr the Expr object to be subtracted
       * @return {OpExpr} the OpExpr resulting from the operation
       */
      Expr.prototype.subtract = function (expr) {
        if (["number", "string"].indexOf(typeof expr) > -1) {
          if (arguments.length === 2) {
            return this.subtract(new ConstantExpr(expr, arguments[1]));
          } else {
            throw "corresponding measure service type(INT,FLOAT,DECIMAL) is required when the argument is primitive";
          }
        }
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        } else {
          return new OpExpr("-", [this, expr]);
        }
      };
      /**
       * Convenience post-fix method that uses the OpExpr builder with a multiply operator
       * to multiply the current Expr with the one in param
       *
       * @method multiply
       * @param {Expr} expr the multiplier
       * @return {OpExpr} the OpExpr resulting from the operation
       */
      Expr.prototype.multiply = function (expr) {
        if (["number", "string"].indexOf(typeof expr) > -1) {
          if (arguments.length === 2) {
            return this.multiply(new ConstantExpr(expr, arguments[1]));
          } else {
            throw "corresponding measure service type(INT,FLOAT,DECIMAL) is required when the argument is primitive";
          }
        }
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        } else {
          return new OpExpr("*", [this, expr]);
        }
      };
      /**
       * Convenience post-fix method that uses the OpExpr builder with a divide operator
       * to divide the current Expr by the one in param
       *
       * @method divide
       * @param {Expr} expr the divisor
       * @return {OpExpr} the OpExpr resulting from the operation
       */
      Expr.prototype.divide = function (expr) {
        if (["number", "string"].indexOf(typeof expr) > -1) {
          if (arguments.length === 2) {
            return this.divide(new ConstantExpr(expr, arguments[1]));
          } else {
            throw "corresponding measure service type(INT,FLOAT,DECIMAL) is required when the argument is primitive";
          }
        }

        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        return new OpExpr("/", [this, expr]);
      };

      /**
       * Convenience post-fix method to widen an expression to other dimension levels
       *
       * @method widen
       * @param {(QualifiedLevel[]|string[]|Intersection)} intersection the intersection to widen to
       * @return {WidenExpr} the WidenExpr resulting from the operation
       */
      Expr.prototype.widen = function (intersection) {
        if (!(intersection instanceof Intersection)) {
          intersection = new Intersection(intersection);
        }
        return new WidenExpr(this, intersection);
      };

      /**
       * Convenience post-fix method to override an expression with another expression
       *
       * @method override
       * @param {Expr} expr the secondary expression
       * @return {OverrideExpr} the OverrideExpr resulting from the operation
       */
      Expr.prototype.overrideBy = function (expr2) {
        return new OverrideExpr(this, expr2);
      };

      /**
       * Convenience post-fix method to difference an expression by another expression
       *
       * @method difference
       * @param {Expr} expr the secondary expression
       * @return {DifferenceExpr} the DifferenceExpr resulting from the operation
       */
      Expr.prototype.difference = function (expr2) {
        return new DifferenceExpr(this, expr2);
      };

      Expr.prototype.toInt = function () {
        return new CastExpr(this, new Type("INT"));
      };
      Expr.prototype.toFloat = function () {
        return new CastExpr(this, new Type("FLOAT"));
      };
      Expr.prototype.toDecimal = function () {
        return new CastExpr(this, new Type("DECIMAL"));
      };
      Expr.prototype.toString = Expr.prototype.toBoolean = function () {
        return this;
      };
      Expr.prototype.prev = function () {
        return new PrevExpr(this);
      };
      Expr.prototype.abs = function () {
        return new OpExpr("ABS", [this]);
      };

      Expr.prototype.negate = function () {
        return new OpExpr("NEGATE", [this]);
      };
      Expr.prototype.cast = function (type) {
        if (typeof type === "string") {
          type = type.toUpperCase();
        }
        return new CastExpr(this, new Type(type));
      };
      Expr.prototype.setAnnotation = function (annotation) {
        assert(annotation instanceof Annotation);

        if (!this.annotation) this.annotation = [annotation];
        else this.annotation.push(annotation);

        return this;
      };

      return Expr;
    })();

    var AttributeExpr = (function (_super) {
      /**
       * Creates an AttributeExpr object
       *
       * @class AttributeExpr
       * @constructor
       * @extends Expr
       * @param {QualifiedLevel} qualifiedLevel a QualifiedLevel object
       * @param {String} name name of the attribute
       * @example
       *
       *      // first we create a QualifiedLevel object
       *      var ql = new MQG.QualifiedLevel('Product', 'sku');
       *
       *      // then we create the AttributeExpr object
       *      var attr = new MQG.AttributeExpr(ql, 'id');
       *
       */
      function AttributeExpr(qualifiedLevel, name) {
        _super.call(this, "ATTRIBUTE", attribute(qualifiedLevel, name));
      }
      _extendClass(AttributeExpr, _super);
      return AttributeExpr;
    })(Expr);

    var MetricExpr = (function (_super) {
      /**
       * Creates a MetricExpr object
       * @param {String} name  name of the metric
       * @param {(MQG.Intersection|undefined)} inter implicit aggregation for the metric
       */
      function MetricExpr(name, inter) {
        assert(typeof name === "string");
        assert(
          typeof inter === "undefined" ||
            inter instanceof Intersection ||
            Array.isArray(inter)
        );
        if (Array.isArray(inter)) {
          inter = new Intersection(inter);
        }
        if (inter instanceof Intersection) {
          // use an AppExpr to instantiate a MetricExpr with its argument
          var metricExpr = new MetricExpr(name);
          return new AppExpr(metricExpr, new InterArgument("inter", inter));
        } else {
          var value = {
            name: name,
          };
          _super.call(this, "METRIC", value);
        }
      }
      _extendClass(MetricExpr, _super);
      return MetricExpr;
    })(Expr);

    var MeasureExpr = (function (_super) {
      /**
       * Creates a MetricExpr object
       * @param {String} name  name of the metric
       * @param {(MQG.Intersection|undefined)} inter implicit aggregation for the metric
       */
      function MeasureExpr(name, intersection) {
        assert(typeof name === "string");
        if (name.indexOf(".") > -1) {
          return new AttributeExpr(name);
        } else {
          return new MetricExpr(name, intersection);
        }
      }
      return MeasureExpr;
    })();

    var OpExpr = (function (_super) {
      var _OPERATORS = {
        NEGATE: "NEGATE",
        "!": "NEGATE", // alternate notation
        SQRT: "SQRT",
        GAMMA: "GAMMA",
        LOG: "LOG",
        TO_UPPER: "TO_UPPER",
        TO_LOWER: "TO_LOWER",
        ADD: "ADD",
        "+": "ADD", // alternate notation
        SUBTRACT: "SUBTRACT",
        "-": "SUBTRACT", // alternate notation
        MULTIPLY: "MULTIPLY",
        "*": "MULTIPLY", // alternate notation
        DIVIDE: "DIVIDE",
        "/": "DIVIDE", // alternate notation
        NAMED: "NAMED",
        INT_TO_FLOAT: "INT_TO_FLOAT",
        INT_TO_DECIMAL: "INT_TO_DECIMAL",
        DECIMAL_TO_INT: "DECIMAL_TO_INT",
        DECIMAL_TO_FLOAT: "DECIMAL_TO_FLOAT",
        STRING_TO_FLOAT: "STRING_TO_FLOAT",
        STRING_TO_INT: "STRING_TO_INT",
        STRING_TO_DECIMAL: "STRING_TO_DECIMAL",
        FLOAT_TO_DECIMAL: "FLOAT_TO_DECIMAL",
        POW: "POW",
        AS_ENTITY: "AS_ENTITY",
        ABS: "ABS",
      };
      var _validateOperator = function (op) {
        op = _OPERATORS[_sanitizeStr(op)];
        if (!op) {
          throw new SyntaxError("Invalide operator");
        } else {
          return op;
        }
      };
      /**
       * Creates a OpExpr object
       *
       * @class OpExpr
       * @constructor
       * @extends Expr
       * @param {String} op the operator to use: '+', '-', '*', '/', '!', 'gamma', 'sqrt', 'log','to_upper', 'to_lower'
       * @param {Expr[]} exprs an array of Expr objects
       * @param {String} name optional name if 'op' is 'NAMED'
       * @example
       *      // FIXME: add an example here
       *
       */
      function OpExpr(op, exprs, name) {
        op = _validateOperator(op);
        assertIsArray(exprs, Expr, "OpExpr takes an array of Expr's.");
        var value = {
          op: {
            kind: op,
          },
          expr: exprs,
        };
        if (op === "NAMED") {
          value.op.name = name;
        }
        _super.call(this, "OP", value);
      }
      _extendClass(OpExpr, _super);
      return OpExpr;
    })(Expr);
    var CastExpr = (function (_super) {
      // WARN to be deprecated
      /**
       * Creates a CastExpr object
       *
       * @class CastExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression whose value you wish to cast
       * @param {Type} type the type to cast the value to
       * @example
       *      // cast an metric expression's value to a string
       *      var castExpr = new MQG.CastExpr(metricExpr, "STRING");
       */
      function CastExpr(expr, type) {
        var value;
        assert(expr instanceof Expr);
        assert(type instanceof Type);
        value = {
          expr: expr,
          type: type,
        };
        _super.call(this, "CAST", value);
      }
      _extendClass(CastExpr, _super);
      return CastExpr;
    })(Expr);
    var LiteralExpr = (function (_super) {
      /**
       * Creates a LiteralExpr object
       *
       * @class LiteralExpr
       * @constructor
       * @extends Expr
       * @param {String} kind 'GENERAL' or 'TOP_SINGLETON'
       * @param {BaseSignature} signature
       * @param {(Column[]|undefined)} column
       * @example
       *      // FIXME: add an example here
       *
       */
      function LiteralExpr(kind, signature, column) {
        var value;
        assert(
          typeof kind === "string" &&
            ["GENERAL", "TOP_SINGLETON"].indexOf(kind) !== -1
        );
        assert(signature instanceof BaseSignature);
        assert(Array.isArray(column));
        value = {
          kind: kind,
          signature: signature,
          column: column,
        };
        _super.call(this, "LITERAL", value);
      }
      _extendClass(LiteralExpr, _super);
      return LiteralExpr;
    })(Expr);

    var ConstantExpr = (function (_super) {
      /**
       * Creates a ConstantExpr object
       *
       * @class ConstantExpr
       * @constructor
       * @extends Expr
       * @param {(Boolean|Number|String)} value
       * @param {Type} type
       */
      function ConstantExpr(value, type) {
        assert(
          typeof value === "boolean" ||
            typeof value === "number" ||
            typeof value === "string" ||
            Array.isArray(value) ||
            _isComplexNumericValue(value)
        );
        assert(type instanceof Type || typeof type === "string");
        if (typeof type === "string") {
          type = new Type(type);
        }
        return new LiteralExpr(
          "GENERAL",
          new BaseSignature(
            new Intersection([]),
            new ValueType("SINGLETON", type)
          ),
          [new Column(type.kind, _toArray(value))]
        );
      }
      _extendClass(ConstantExpr, _super);
      return ConstantExpr;
    })(Expr);

    var VarExpr = (function (_super) {
      /**
       * Creates a VarExpr object
       *
       * @class VarExpr
       * @constructor
       * @extends Expr
       * @param {string} name
       */
      function VarExpr(str) {
        _super.call(this, "VARIABLE", {
          name: str,
        });
      }
      _extendClass(VarExpr, _super);
      return VarExpr;
    })(Expr);

    var AbsExpr = (function (_super) {
      /**
       * Creates a AbsExpr object
       *
       * @class AbsExpr
       * @constructor
       * @extends Expr
       * @param {(undefined|InterArgument|InterArgument[])} interArgs
       * @param {(undefined|ExprArgument|ExprArgument[])} exprArgs
       * @param {Expr} body
       */
      function AbsExpr(interArgs, exprArgs, body) {
        interArgs = _toArray(interArgs);
        exprArgs = _toArray(exprArgs);
        assertIsArray(interArgs, InterArgument);
        assertIsArray(exprArgs, ExprArgument);
        assert(body instanceof Expr);
        _super.call(this, "ABSTRACTION", {
          inter_arg: interArgs,
          expr_arg: exprArgs,
          body: body,
        });
      }
      _extendClass(AbsExpr, _super);
      return AbsExpr;
    })(Expr);

    var AppExpr = (function (_super) {
      /**
       * Creates a AppExpr object
       *
       * @class AppExpr
       * @constructor
       * @extends Expr
       * @param {Expr} abstraction
       * @param {(undefined|InterArgument|InterArgument[])} interArgs
       * @param {(undefined|ExprArgument|ExprArgument[])} exprArgs
       */
      function AppExpr(abstraction, interArgs, exprArgs) {
        assert(abstraction instanceof Expr);
        interArgs = _toArray(interArgs);
        exprArgs = _toArray(exprArgs);
        assertIsArray(interArgs, InterArgument);
        assertIsArray(exprArgs, ExprArgument);
        _super.call(this, "APPLICATION", {
          abstraction: abstraction,
          inter_arg: interArgs,
          expr_arg: exprArgs,
        });
      }
      _extendClass(AppExpr, _super);
      return AppExpr;
    })(Expr);

    var FilterExpr = (function (_super) {
      /**
       * Creates a FilterExpr object
       *
       * @class FilterExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to filter
       * @param {Comparison[]} comparisons an array of Comparison objects
       * @param {Boolean} [isDisjunction=false] set to true to keep values that satisfy any of the comparisons
       * @example
       *      // We create an Attribute Expression to filter later
       *      var attr = new MQG.AttributeExpr(new MQG.QualifiedLevel('Product', 'item'), 'id');
       *
       *      // We filter the Attribute Expression using two different comparisons in disjunction
       *      var expr = new MQG.FilterExpr(attr, [
       *          new MQG.Comparison('=', new MQG.TermExpr(new MQG.ConstantTerm('SKU001', 'STRING'))),
       *          new MQG.Comparison('=', new MQG.TermExpr(new MQG.ParamTerm('sku_id', 'string')))
       *      ]);
       *
       */
      function FilterExpr(expr, comparisons, type, isDisjunction) {
        var value;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        if (
          !(comparisons instanceof MQG.Comparison) &&
          (!comparisons.length || !(comparisons[0] instanceof MQG.Comparison))
        ) {
          if (typeof type !== "string") {
            throw "Third argument must be value type if the second argument is a primitive value(not comparison)";
          }
          var prop;
          var new_comparisons = [];
          for (prop in comparisons) {
            new_comparisons.push(
              new MQG.Comparison(
                prop,
                new MQG.ConstantExpr(_toArray(comparisons[prop]), type)
              )
            );
          }
          comparisons = new_comparisons;
        } else {
          isDisjunction = type;
        }
        isDisjunction = isDisjunction || false;
        value = {
          expr: expr,
          comparison: _toArray(comparisons),
          is_disjunction: isDisjunction,
        };
        _super.call(this, "FILTER", value);
      }
      _extendClass(FilterExpr, _super);
      return FilterExpr;
    })(Expr);
    var DiceExpr = (function (_super) {
      /**
       * Creates a DiceExpr object
       *
       * @class DiceExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to dice
       * @param {Expr[]} dicers an array of Expr objects
       * @param {Boolean} [isDisjunction=false] set to true to keep values that satisfy any of the dicers
       * @example
       *      // FIXME: add an example here
       *
       */
      function DiceExpr(expr, dicers, isDisjunction) {
        var value;
        // default to false if not set
        isDisjunction = isDisjunction || false;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        dicers = _toArray(dicers);
        if (!dicers.length) {
          throw "empty dicers specified for: " + JSON.stringify(expr);
        }
        value = {
          expr: expr,
          dicer: dicers,
          is_disjunction: isDisjunction,
        };
        _super.call(this, "DICE", value);
      }
      _extendClass(DiceExpr, _super);
      return DiceExpr;
    })(Expr);
    var SplitExpr = (function (_super) {
      /**
       * Wraps the given expr in a relabel expression.
       *
       * @class SplitExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to relabel
       * @param {LabelMap} labelMaps the mapping from the old label to the new label
       */
      function SplitExpr(expr, labelMaps) {
        var value;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        labelMaps = _toArray(labelMaps);
        if (!labelMaps.length) {
          throw new SyntaxError(
            "empty labelMaps specified for relabeling expr: " +
              JSON.stringify(expr)
          );
        }
        value = {
          expr: expr,
          map: labelMaps,
        };
        _super.call(this, "SPLIT", value);
      }
      _extendClass(SplitExpr, _super);
      return SplitExpr;
    })(Expr);
    var RelabelExpr = (function (_super) {
      /**
       * Wraps the given expr in a relabel expression.
       *
       * @class RelabelExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to relabel
       * @param {LabelMap} labelMaps the mapping from the old label to the new label
       */
      function RelabelExpr(expr, labelMaps) {
        var value;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        labelMaps = _toArray(labelMaps);
        if (!labelMaps.length) {
          throw new SyntaxError(
            "empty labelMaps specified for relabeling expr: " +
              JSON.stringify(expr)
          );
        }
        value = {
          expr: expr,
          map: labelMaps,
        };
        _super.call(this, "RELABEL", value);
      }
      _extendClass(RelabelExpr, _super);
      return RelabelExpr;
    })(Expr);
    var LabelMap = (function () {
      /**
       * A mapping from a source label to a target label
       * @class LabelMap
       * @constructor
       * @param {string} source the label to replace
       * @param {string} target the new label
       */
      function LabelMap(source, target) {
        this.source = source;
        this.target = target;
      }
      return LabelMap;
    })();
    var WidenExpr = (function (_super) {
      /**
       * Creates a WidenExpr object
       *
       * @class WidenExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to widen
       * @param {Intersection} inter the intersection to widen to
       * @example
       *      // FIXME: add an example here
       *
       */
      function WidenExpr(expr, inter) {
        var value;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        if (!(inter instanceof Intersection)) {
          throw new SyntaxError("Intersection Expected");
        }
        value = {
          expr: expr,
          inter: inter,
        };
        _super.call(this, "WIDEN", value);
      }
      _extendClass(WidenExpr, _super);
      return WidenExpr;
    })(Expr);
    var DropExpr = (function (_super) {
      /**
       * Creates a DropExpr object.
       * Drop expressions drop the values from a measure query to
       * produce a position-only query.  This is useful if you would like
       * to compute over keys without having to worry about values.
       *
       * @class DropExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to drop values from
       * @example
       *      // FIXME: add an example here
       *
       */
      function DropExpr(expr) {
        var value;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        value = {
          expr: expr,
        };
        _super.call(this, "DROP", value);
      }
      _extendClass(DropExpr, _super);
      return DropExpr;
    })(Expr);
    var DifferenceExpr = (function (_super) {
      /**
       * Creates a DifferenceExpr object.
       * Difference expressions remove those positions and values in
       * the left expression that are contained in the right expression.
       * If the right expression is position-only, then positions in the
       * left expression are removed regardless of value.
       *
       * @class DifferenceExpr
       * @constructor
       * @extends Expr
       * @param {Expr} leftExpr the left expression
       * @param {Expr} rightExpr the right expression
       * @example
       *      // FIXME: add an example here
       *
       */
      function DifferenceExpr(leftExpr, rightExpr) {
        var value;
        if (!(leftExpr instanceof Expr)) {
          throw new SyntaxError("First Argument: Expression Expected");
        }
        if (!(rightExpr instanceof Expr)) {
          throw new SyntaxError("Second Argument: Expression Expected");
        }
        value = {
          left: leftExpr,
          right: rightExpr,
        };
        _super.call(this, "DIFFERENCE", value);
      }
      _extendClass(DifferenceExpr, _super);
      return DifferenceExpr;
    })(Expr);
    var DemoteExpr = (function (_super) {
      /**
       * Creates a DemoteExpr object.
       * Demote expressions take a position-only measure expression and
       * convert it to one with a value by making one of the dimensions
       * of its intersection its value.  As such, it may produce a
       * set-valued measure expression
       *
       * @class DemoteExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to Demote. It has to be a position-only expression
       * @param {String} dimension the dimension to convert into a value
       * // FIX: support label besides dimension
       * @example
       *      // FIXME: add an example here
       */
      function DemoteExpr(expr, dimension) {
        var value;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        value = {
          expr: expr,
          dimension: dimension.trim(),
        };
        _super.call(this, "DEMOTE", value);
      }
      _extendClass(DemoteExpr, _super);
      return DemoteExpr;
    })(Expr);
    var PromoteExpr = (function (_super) {
      /**
       * Creates a PromoteExpr object.
       * Promote expressions take a valued measure expression and
       * convert it to a position-only by adding specified dimension
       *
       * @class PromoteExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to Promote. It has to be a valued expression
       * @param {String} dimension the dimension to add to the keys
       * @example
       *      // FIXME: add an example here
       */
      function PromoteExpr(expr, dimension) {
        var value;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        value = {
          expr: expr,
          label: dimension && dimension.trim(),
        };
        _super.call(this, "PROMOTE", value);
      }
      _extendClass(PromoteExpr, _super);
      return PromoteExpr;
    })(Expr);
    var Branch = (function () {
      /**
       * Creates a Branch object
       *
       * @class Branch
       * @constructor
       * @param {Expr} testExpr the expression to be tested for emptiness
       * @param {Expr} resultExpr the result if non-empty
       */
      function Branch(testExpr, resultExpr) {
        if (!(testExpr instanceof Expr)) {
          throw new SyntaxError("First Argument: Expression Expected");
        }
        if (!(resultExpr instanceof Expr)) {
          throw new SyntaxError("Second Argument: Expression Expected");
        }
        this.test = testExpr;
        this.result = resultExpr;
      }
      return Branch;
    })();

    var RefineExpr = (function (_super) {
      /**
       * Creates a RefineExpr object
       *
       * @class RefineExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to refine
       * @param {Intersection} inter optional intersection to refine to instead of a distribution
       * @param {Distribution[]} distribution an array of Distribution objects
       * @example
       *      new RefineExpr(new Expr(undefined, "test"), undefined, new Intersection(new QualifiedLevel("testdim", "testlevel")))
       *
       */
      function RefineExpr(expr, inter, distribution) {
        assert(
          !(inter && distribution),
          "inter and distribution parameters are mutually exclusive."
        );
        assert(
          inter || distribution,
          "inter or distribution parameter must be supplied."
        );

        if (inter) {
          assert(
            inter instanceof Intersection,
            "inter paramater must be of type intersection"
          );
        }

        if (distribution) {
          assertIsArray(
            distribution,
            Distribution,
            "distribution argument should be an array of Distribution but is: " +
              JSON.stringify(distribution)
          );
        }

        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        } else {
          _super.call(this, "REFINEMENT", { expr, inter, distribution });
        }
      }
      _extendClass(RefineExpr, _super);
      return RefineExpr;
    })(Expr);

    var AggExpr = (function (_super) {
      var _METHODS = [
        "COLLECT",
        "AMBIG",
        "TOTAL",
        "MIN",
        "MAX",
        "COUNT",
        "MODE",
        "COUNT_DISTINCT",
        "HISTOGRAM",
        "AVERAGE", // simulated, not built-in
        "SORT",
        "COVER",
      ];
      var _validAggMethod = function (method) {
        if (_METHODS.indexOf(method) === -1) {
          return false;
        } else {
          return true;
        }
      };
      /**
       * Creates an AggExpr object
       *
       * @class AggExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to aggregate
       * @param {String} method the aggregation method. It could be one of the following:
       * <p>'COLLECT', 'AMBIG', 'TOTAL', 'MIN', 'MAX', 'COUNT', 'MODE', 'COUNT_DISTINCT', 'HISTOGRAM', 'COVER'</p>
       * @param {Grouping[]} groupings an array of Grouping objects
       * @param {(Intersection|undefined)} inter target intersection
       * @example
       *      // FIXME: Add an example
       *
       */
      function AggExpr(expr, method, groupingsOrInter) {
        var groupings, inter;
        if (groupingsOrInter instanceof Intersection) {
          inter = groupingsOrInter;
        } else {
          assertIsArray(
            groupingsOrInter,
            Grouping,
            "Third argument of AggExpr should be Grouping[] or Intersection, but is: " +
              JSON.stringify(groupingsOrInter)
          );
          groupings = groupingsOrInter;
        }
        var value;
        var aggMethod;
        if (typeof method === "string") {
          method = _sanitizeStr(method);
          if (!_validAggMethod(method)) {
            throw new SyntaxError("Invalid method for aggregation: " + method);
          }
          aggMethod = {
            primitive: method,
          };
        } else if (typeof method === "object") {
          aggMethod = method;
        }
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        } else {
          value = {
            method: aggMethod,
            expr: expr,
          };
          if (groupings) {
            value.grouping = groupings;
          } else if (inter) {
            value.inter = inter;
          }
          // Marouen: based on Kurt's implementation
          // If the aggMethod is MODE, you can get more than one value for an intersection.
          // Wrap the MODE expr in a MAX aggregation to pick one
          if (aggMethod.primitive === "MODE") {
            _super.call(this, "AGGREGATION", value);
            if (groupings) {
              groupings = groupings.filter(function (elem) {
                if (elem.kind == "ALL") return false;
                else return true;
              });
            } else groupings = groupingsOrInter;
            return new AggExpr(this, "MAX", groupings);
          } else {
            _super.call(this, "AGGREGATION", value);
          }
        }
      }
      _extendClass(AggExpr, _super);
      return AggExpr;
    })(Expr);
    var HeaderSortExpr = (function (_super) {
      /**
       * Creates an HeaderSortExpr object
       *
       * @class HeaderSortExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to aggregate
       * @param {String} floatingLevel the floating level
       */
      function HeaderSortExpr(expr, floatingLevel) {
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        if (!(floatingLevel instanceof QualifiedLevel)) {
          throw new SyntaxError("Qualified Level Expected");
        }
        return _super.call(this, "AGGREGATION", {
          method: {
            header_sort_level: floatingLevel,
          },
          expr: expr,
        });
      }
      _extendClass(HeaderSortExpr, _super);
      return HeaderSortExpr;
    })(Expr);
    var OverrideExpr = (function (_super) {
      /**
       * Creates an OverrideExpr object
       *
       * @class OverrideExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to aggregate
       */
      function OverrideExpr() {
        var exprs = Array.prototype.slice.call(arguments, 0);
        exprs.forEach(function (expr) {
          if (!(expr instanceof Expr)) {
            throw new SyntaxError("Expression Expected");
          }
        });
        return _super.call(this, "OVERRIDE", {
          expr: exprs,
        });
      }
      _extendClass(OverrideExpr, _super);
      return OverrideExpr;
    })(Expr);
    var CompositeExpr = (function (_super) {
      var _KINDS = ["UNION", "INTERSECTION"];
      var _validateKind = function (kind) {
        if (_KINDS.indexOf(kind) === -1) {
          return false;
        } else {
          return true;
        }
      };
      /**
       * Creates an CompositeExpr object.
       * Composite expressions are used to combine together many
       * measure expressions by unioning or intersecting them
       * together.  All the measure expressions must be at the
       * same intersection.
       *
       * @class CompositeExpr
       * @constructor
       * @extends Expr
       * @param {String} kind could be either 'UNION' or 'INTERSECTION'
       * <ul>
       *   <li>INTERSECTION: Only keep those positions and values found in
       *   all measure expressions.</li>
       *   <li>UNION: Collect all positions and values found in all measure expressions.
       *   Currently, trying to combine measure expressions with different values for
       *   the same position will yield a runtime exception. </li>
       * </ul>
       * @param {Expr[]} exprs array of expressions to combine
       * @example
       *      // FIXME: Add an example
       *
       */
      function CompositeExpr(kind, exprs) {
        var value;
        kind = _sanitizeStr(kind);
        if (!_validateKind(kind)) {
          throw new SyntaxError(
            "Invalid kind. Possible values are: INTERSECTION, UNION"
          );
        }
        value = {
          kind: kind,
          expr: exprs,
        };
        _super.call(this, "COMPOSITE", value);
      }
      _extendClass(CompositeExpr, _super);
      return CompositeExpr;
    })(Expr);
    var ParamExpr = (function (_super) {
      /**
       * Creates a ParamExpr object
       *
       * @class ParamExpr
       * @constructor
       * @param {string} name
       * @param {Intersection} inter
       * @param {(Type|string|undefined)} type
       * @param {(string|undefined)} multiplicity
       */
      function ParamExpr(name, inter, type, multiplicity) {
        if (inter instanceof BaseSignature) {
          assert(
            typeof type === "undefined",
            "third argument 'type' shouldn't be present if BaseSignature is supplied"
          );
          _super.call(this, "PARAM", {
            name: name,
            signature: inter,
          });
        } else {
          assert(
            type instanceof Type ||
              typeof type === "string" ||
              typeof type === "undefined",
            [
              "Parameter 'type' should be Type, string or undefined, but is: ",
              type,
            ]
          );
          type =
            type instanceof Type
              ? type
              : typeof type === "string"
              ? new Type(type)
              : undefined; // type === undefined
          multiplicity = multiplicity || "SINGLETON";
          var valueType = type ? new ValueType(multiplicity, type) : undefined;

          _super.call(this, "PARAM", {
            name: name,
            signature: new BaseSignature(inter, valueType),
          });
        }
      }
      _extendClass(ParamExpr, _super);
      return ParamExpr;
    })(Expr);
    var PrevExpr = (function (_super) {
      /**
       * Creates a PrevExpr object.
       * Prev expressions are temporal operator, that return the
       * result of the expression as it was in the previous transaction.
       *
       * Currently only works on expressions that will be materialized.
       *
       * @class PrevExpr
       * @constructor
       * @extends Expr
       * @param {Expr} expr the expression to Demote. It has to be a position-only expression
       * @example
       *      // FIXME: add an example here
       */
      function PrevExpr(expr) {
        var value;
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        }
        value = {
          expr: expr,
        };
        _super.call(this, "PREV", value);
      }
      _extendClass(PrevExpr, _super);
      return PrevExpr;
    })(Expr);
    var Comparison = (function () {
      var _adapter = {
        EQUALS: "EQUALS",
        NOT_EQUALS: "NOT_EQUALS",
        LESS_THAN: "LESS_THAN",
        LESS_OR_EQUALS: "LESS_OR_EQUALS",
        GREATER_THAN: "GREATER_THAN",
        GREATER_OR_EQUALS: "GREATER_OR_EQUALS",
        match: "MATCH",
        MATCH: "MATCH",
        "=~": "MATCH",
        "~": "MATCH",
        "=": "EQUALS",
        "!=": "NOT_EQUALS",
        "<>": "NOT_EQUALS", // alternate notation
        "<": "LESS_THAN",
        "<=": "LESS_OR_EQUALS",
        ">": "GREATER_THAN",
        ">=": "GREATER_OR_EQUALS",
      };
      var _sanitizeOp = function (op) {
        var operator = _sanitizeStr(op);
        if (_adapter[operator]) {
          return _adapter[operator];
        } else {
          throw new SyntaxError("Invalid Operator");
        }
      };
      /**
       * Creates a Comparison object
       *
       * @class Comparison
       * @constructor
       * @param {String} op an operator which could be one of the following:
       * <ul><li>'='</li><li>'!='</li><li>'<'</li><li>'<='</li><li>'>'</li><li>'>='</li><li>'=~'</li><li>'~'</li></ul>
       * @param {Expr} expr an Expr object
       */
      function Comparison(op, expr) {
        var operator = _sanitizeOp(op);
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Expression Expected");
        } else {
          this.op = operator;
          this.expr = expr;
        }
      }
      return Comparison;
    })();
    var QualifiedLevel = (function () {
      /**
       * Creates a QualifiedLevel object
       *
       * @class QualifiedLevel
       * @constructor
       * @param {String} dimension dimension of the qualified level
       * @param {String} level
       * @param {String} [hierarchy] an optional hierarchy
       * @param {String} [label] an optional label
       */
      function QualifiedLevel(dimension, level, hierarchy, label) {
        this.dimension = dimension;
        this.level = level;
        if (hierarchy) {
          this.hierarchy = hierarchy;
        }
        if (label) {
          this.label = label;
        }
      }
      return QualifiedLevel;
    })();
    var _parseQualifiedLevel = function (el) {
      if (el instanceof QualifiedLevel) {
        return el;
      }
      if (typeof el !== "string") {
        throw new SyntaxError(
          "Element should be either String or QualifiedLevel"
        );
      }
      var splitted = el.split("."),
        dim = splitted[0],
        level = splitted[1],
        label;
      if (dim.indexOf(":") !== -1) {
        splitted = dim.split(":");
        label = splitted[0];
        dim = splitted[1];
      }
      return {
        dim: dim,
        level: level,
        label: label,
      };
    };
    var Grouping = (function () {
      var _validGpgKind = function (kind) {
        var KINDS = ["NO_GROUPING", "ALL", "MAP", "SLIDE"];
        if (KINDS.indexOf(kind) === -1) {
          return false;
        } else {
          return true;
        }
      };
      /**
       * Creates a Grouping object.
       * In case if it's a slide grouping, only <tt>kind</tt> and <tt>slide</tt> need to be populated.
       *
       * @class Grouping
       * @constructor
       * @param {String} kind could be one of the following: 'NO_GROUPING', 'ALL', 'MAP', 'SLIDE'
       * @param {String} [dimension] the dimension of the key that we want to aggregate over
       * @param {String} [level] the level of dimension that we want to aggregate up to
       * @param {String} [slide] an optional slide
       * @param {String} [hierarchy] an optional hierarchy
       * @param {String} [label] an optional label
       */
      function Grouping(kind, dimension, level, slide, hierarchy, label) {
        var gpgKind = _sanitizeStr(kind);
        if (_validGpgKind(gpgKind)) {
          this.kind = gpgKind;
          if (dimension) {
            this.dimension = dimension;
          }
          if (level) {
            this.level = level;
          }
          if (hierarchy) {
            this.hierarchy = hierarchy;
          }
          if (slide) {
            this.slide = slide;
          }
          if (label) {
            this.label = label;
          }
        } else {
          throw new SyntaxError("Invalid kind for the grouping");
        }
      }
      return Grouping;
    })();
    var _typeColumnAttr = {
      STRING: "string_column",
      INT: "int_column",
      INT128: "int128_column",
      FLOAT: "float_column",
      DECIMAL: "decimal_column",
      BOOLEAN: "bool_column",
      DATETIME: "datetime_column",
    };

    var _isInt128Value = function (value) {
      return typeof value === "object" && value.high && value.low;
    };

    var _isThreePartDecimalValue = function (value) {
      assert(
        typeof value === "object",
        "Decimal value not represented as an object " + JSON.stringify(value)
      );
      var requiredKeysPresent =
        "integral_digit" in value &&
        "fraction_digit" in value &&
        "negative" in value;
      return requiredKeysPresent;
    };

    var _isComplexNumericValue = function (value) {
      return _isThreePartDecimalValue(value) || _isInt128Value(value);
    };

    var _valuesToInt128Column = function (values) {
      var column = values.reduce(
        function (column, value) {
          column.high.push(value.high);
          column.low.push(value.low);

          return column;
        },
        { high: [], low: [] }
      );

      return column;
    };

    var _valuesToDecimalColumns = function (values) {
      var fraction_digit = [],
        integral_digit = [],
        negative = [];

      values.forEach(function (value) {
        var valueObject = {};
        if (typeof value === "string") {
          if (value.indexOf("-") === 0) {
            valueObject.negative = true;
            value = value.substring(1);
          }
          if (value.indexOf(".") >= 0) {
            valueObject.fraction_digit =
              parseInt(
                _padEnd(
                  value.slice(value.indexOf(".") + 1),
                  DECIMAL_PART_DIGITS,
                  "0"
                )
              ) || 0;
            valueObject.integral_digit =
              parseInt(value.slice(0, value.indexOf("."))) || 0;
          } else {
            valueObject.integral_digit = parseInt(value);
          }
        } else {
          valueObject = value;
        }
        // set defaults if a field is not present
        fraction_digit.push(valueObject.fraction_digit || 0);
        integral_digit.push(valueObject.integral_digit || 0);
        negative.push(valueObject.negative || false);
      });

      return {
        fraction_digit: fraction_digit,
        integral_digit: integral_digit,
        negative: negative,
      };
    };

    var Column = (function () {
      /**
       * Creates a Column object
       *
       * @class Column
       * @constructor
       * @param {String} type type of the parameter: ['STRING', 'INT', 'INT128', 'DECIMAL', 'FLOAT', 'BOOLEAN']
       * @param {String[]|Number[]|Boolean[]|String|Number|Boolean} constants An array of the constants that represent this column (of the type of @type), or a single constant
       *
       */
      function Column(type, constants) {
        var typof = typeof constants;
        assert(type instanceof Type || typeof type === "string");
        type = type instanceof Type ? type.kind.toUpperCase() : type;
        assert(_typeColumnAttr.hasOwnProperty(type), ["Unknown type", type]);
        assert(
          Array.isArray(constants) ||
            typof === "string" ||
            typof === "number" ||
            typof === "boolean" ||
            _isComplexNumericValue(constants),
          "Invalid constants " + JSON.stringify(constants)
        );

        var values = _toArray(constants);

        var column;

        if (type === "INT128") {
          column = _valuesToInt128Column(values);
        } else if (type === "DECIMAL") {
          column = _valuesToDecimalColumns(values);
        } else {
          column = { value: values };
        }

        this[_typeColumnAttr[type]] = column;
      }
      return Column;
    })();
    var RelationBinding = (function () {
      /**
       * Creates a RelationBinding object
       *
       * @class RelationBinding
       * @constructor
       * @param {ParamExpr} paramExpr The name of the parameter and its signature
       * @param {Expr} expr The expr to bind to the parameter expression
       */
      function RelationBinding(paramExpr, expr) {
        if (!(paramExpr instanceof ParamExpr)) {
          throw new SyntaxError("First Argument: Param Expression Expected");
        }
        if (!(expr instanceof Expr)) {
          throw new SyntaxError("Second Argument: Expr Expected");
        }
        this.param = paramExpr.param;
        this.expr = expr;
      }
      return RelationBinding;
    })();
    var Intersection = (function () {
      /**
       * Creates a Intersection object
       *
       * @class Intersection
       * @constructor
       * @param {(string|Expr|QualifiedLevel[]|string[])} qualifiedLevels or strings in the format dimension.level
       */
      function Intersection(varOrExprOrQualifiedLevels) {
        if (typeof varOrExprOrQualifiedLevels === "string") {
          this.variable = varOrExprOrQualifiedLevels;
        } else if (varOrExprOrQualifiedLevels instanceof Expr) {
          this.expr = varOrExprOrQualifiedLevels;
        } else {
          var qualifiedLevels = _toArray(varOrExprOrQualifiedLevels);
          if (
            typeof qualifiedLevels[0] === "string" &&
            qualifiedLevels[0].indexOf(".") > 0
          ) {
            qualifiedLevels = qualifiedLevels.map(function (el) {
              el = _parseQualifiedLevel(el);
              return new QualifiedLevel(el.dim, el.level, undefined, el.label);
            });
          }
          this.qualified_level = qualifiedLevels;
        }
      }
      return Intersection;
    })();
    var MetricInter = (function () {
      function MetricInter(metric, inter) {
        assert(typeof metric === "string");
        assert(inter instanceof Intersection);
        this.metric = metric;
        this.inter = inter;
      }
      return MetricInter;
    })();

    var InterArgument = (function () {
      /**
       * Creates a InterArgument object
       *
       * @class InterArgument
       * @constructor
       * @param {string} name
       * @param {Intersection} inter
       */
      function InterArgument(name, inter) {
        assert(typeof name === "string");
        this.name = name;
        if (inter) {
          assert(inter instanceof Intersection);
          this.inter = inter;
        }
      }
      return InterArgument;
    })();

    var ExprArgument = (function () {
      /**
       * Creates a ExprArgument object
       *
       * @class ExprArgument
       * @constructor
       * @param {string} name
       * @param {Expr} expr
       */
      function ExprArgument(name, expr) {
        assert(typeof name === "string");
        this.name = name;
        if (expr) {
          assert(expr instanceof Expr);
          this.expr = expr;
        }
      }
      return ExprArgument;
    })();

    var Distribution = (function () {
      /**
       * Creates a Distribution object
       *
       * @class Distribution
       * @constructor
       * @param {QualifiedLevel} level_to // spread to lower level
       * @param {QualifiedLevel} level_all // Add an additional level to the hierarchy
       * @param {string} slide // The name of a slide expression
       */
      function Distribution(levelTo, levelAll, slide) {
        if (arguments.length > 1) {
          assert(
            "The levelTo, levelAll, and slide arguments are mutually exclusive but multiple were passed"
          );
        }

        if (levelTo) {
          assert(levelTo instanceof QualifiedLevel);
          this.level_to = levelTo;
        } else if (levelAll) {
          assert(levelAll instanceof QualifiedLevel);
          this.level_all = levelAll;
        } else if (slide) {
          this.slide = slide;
        } else {
          assert("LevelTo, LevelAll, or Slide must be provided");
        }
      }
      return Distribution;
    })();

    var Ancestor = (function (_super) {
      /**
       * Create a measure that relates level members between
       * loLabel:dimension:loLevel and hiLabel:dimension:hiLevel
       *
       * @param {String} dimension
       * @param {String} loLevel
       * @param {String} hiLevel
       * @param {String} loLabel
       * @param {String} hiLabel
       */
      function Ancestor(dimension, loLevel, hiLevel, loLabel, hiLabel) {
        assert(
          Array.prototype.slice.call(arguments, 0).every(function (param) {
            return typeof param === "string";
          }),
          "Expected strings"
        );
        // FIX what if hiLevel === 'ALL'?
        loLabel = loLabel || "Child";
        hiLabel = hiLabel || "Parent";

        // (Parent:loLevel, Child:loLevel, Dimension:loLevel ; )
        return (
          new SplitExpr(
            new DropExpr(
              new AttributeExpr(
                {
                  dimension: dimension,
                  level: loLevel,
                },
                "id"
              )
            ),
            [
              {
                source: dimension,
                target: hiLabel,
              },
              {
                source: dimension,
                target: loLabel,
              },
            ]
          )
            // (Parent:loLevel, Child:loLevel ; Dimension:loLevel)
            .demote(dimension)
            // (Parent:hiLevel, Child:loLevel ; Dimension)
            .aggBy(
              "COLLECT",
              new Intersection([
                {
                  dimension: dimension,
                  level: loLevel,
                  label: loLabel,
                },
                {
                  dimension: dimension,
                  level: hiLevel,
                  label: hiLabel,
                },
              ])
            )
            // (Parent:hiLevel, Child:loLevel ; )
            .drop()
        );
      }
      _extendClass(Ancestor, _super);
      return Ancestor;
    })(Expr);

    var TargetLock = (function () {
      /** Associate 'lockExpr' with 'target'
       *
       * @class TargetLock
       * @constructor
       * @param {Target} target
       * @param {Expr} lockExpr
       * @param {Boolean} convertible
       */
      function TargetLock(target, lockExpr, convertible) {
        assert(lockExpr instanceof Expr);
        this.target = target;
        this.expr = lockExpr;
        this.convertible = convertible;
      }
      return TargetLock;
    })();

    var UpdateRequest = (function () {
      /**
       * Creates an UpdateRequest object
       *
       * @class UpdateRequest
       * @constructor
       * @param {UpdateExpr|UpdateExpr[]} updateExprs
       * @param {string|string[]} namedUpdates
       */
      function UpdateRequest(updateExprs, namedUpdates) {
        if (arguments.length > 2) {
          throw new SyntaxError(
            "argument and relation parameters should be hoisted to Request"
          );
        }
        if (!(updateExprs || namedUpdates)) {
          throw new SyntaxError("Update Expressions or Named Updates Expected");
        }
        if (updateExprs) {
          if (Array.isArray(updateExprs)) {
            assertIsArray(updateExprs, [UpdateExpr]);
            this.expr = updateExprs;
          } else {
            if (!(updateExprs instanceof UpdateExpr)) {
              throw new SyntaxError(
                "First Argument: UpdateExpr Expression Expected"
              );
            }
            this.expr = [updateExprs];
          }
        }
        if (namedUpdates) {
          if (Array.isArray(namedUpdates)) {
            this.named = namedUpdates;
          } else {
            if (!(typeof namedUpdates === "string")) {
              throw new SyntaxError(
                "Second Argument: String Expected or Array of Strings"
              );
            }
            this.named = [namedUpdates];
          }
        }
      }
      return UpdateRequest;
    })();

    var UpdateTransform = (function () {
      function UpdateTransform(spread_kind, distribution) {
        this.spread_kind = spread_kind;
        this.distribution = distribution;
      }
      return UpdateTransform;
    })();

    var UpdateQueryTransform = (function (_super) {
      /**
       * Creates an UpdateTransform object for a spread-by-query
       *
       * @class UpdateTransform
       * @constructor
       * @param {Expr} expr Measure expression to compute the result of the query update
       * @param {QualifiedLevel[]} distribution intersection of the previous expression
       * @param {string|object} type The expected type, it can be either the type kind, or an object than contains the kind as well as other attributes(useful for named types).
       */
      // type is optional, it's not used for remove requests
      function UpdateQueryTransform(expr, distribution, type) {
        assert(
          expr instanceof Expr &&
            (!type || typeof type === "string" || typeof type === "object")
        );
        var spread_kind = {
          query: {
            expr: expr,
          },
        };
        if (type) {
          spread_kind.query.type = (typeof type === "object" && type) || {
            kind: type,
          };
        }
        return _super.call(this, spread_kind, distribution);
      }
      _extendClass(UpdateQueryTransform, _super);
      return UpdateQueryTransform;
    })(UpdateTransform);

    var UpdateExpr = (function () {
      /**
       * Creates an UpdateExpr object
       *
       * @class UpdateExpr
       * @constructor
       * @param {string} kind Update kind
       * @param {string} metric The named metric to update
       * @param {Expr} inputExpr
       * @param {UpdateTransform[]} transform
       * @param {(string|Type|undefined)} type The type of the update Expression, should be the same as the type of destination metric. if no type is specified, the type of source is used.
       */
      function UpdateExpr(kind, metric, inputExpr, transform, transformDepr) {
        assert(
          typeof kind === "string" && ["SPREAD", "REMOVE"].indexOf(kind) !== -1
        );
        assert(typeof metric === "string" || metric instanceof Target);
        //just to avoid breaking changes
        if (!(transform instanceof UpdateTransform) && transformDepr) {
          transform = transformDepr;
        }
        assertIsArray(transform, [UpdateTransform, UpdateQueryTransform]);
        this.kind = kind;
        if (metric instanceof Target) {
          this.target = metric;
        } else {
          this.target = new Target(metric);
        }
        this.input = inputExpr;
        this.transform = transform;
      }
      return UpdateExpr;
    })();

    var QueryRequest = (function () {
      /**
       * Creates an QueryRequest object
       *
       * @class QueryRequest
       * @constructor
       * @param {string} report_name
       * @param {string[]} keys
       * @param {string[]} measures
       * @param {(undefined|Boolean)} return_row_numbers
       * @param {(undefined|Boolean)} limited_rewrites
       * @param {string[]} constraints
       */
      function QueryRequest(
        report_name,
        keys,
        measures,
        return_row_numbers,
        limited_rewrites,
        row_limit,
        constraints
      ) {
        assert(typeof report_name === "string");
        assert(keys instanceof Array);
        assert(measures instanceof Array);
        assert(
          typeof return_row_numbers === "undefined" ||
            typeof return_row_numbers === "boolean"
        );
        assert(
          measures || constraints,
          "QueryRequest requires at least one measure or constraint"
        );
        this.report_name = report_name;
        this.key = keys;
        if (measures) {
          this.measure = measures;
        }
        if (constraints) {
          this.constraint = constraints;
        }
        this.return_row_numbers = return_row_numbers;
        if (typeof limited_rewrites !== "undefined") {
          this.limited_rewrites = limited_rewrites;
        }
        if (typeof row_limit !== "undefined") {
          this.row_limit = row_limit;
        }
      }
      return QueryRequest;
    })();
    var InstallRequest = (function () {
      /**
       * Creates an InstallRequest object
       *
       * @class InstallRequest
       * @constructor
       * @param {(Expr|Expr[])} measures
       */
      function InstallRequest(measures) {
        measures = _toArray(measures);
        this.measure_expr = measures;
      }
      return InstallRequest;
    })();
    var Request = (function () {
      /**
       * Creates an Request object
       *
       * @class Request
       * @constructor
       * @param {(undefined|QueryRequest|[QueryRequest])} queries
       * @param {(undefined|UpdateRequest|[UpdateRequest])} updates
       * @param {(undefined|[RelationBinding])} relation  [description]
       * @param {(undefined|Boolean)} model_request whether to request the model
       * @param {(undefined|TargetLock|[TargetLock])} locks
       * @param {(undefined|InstallRequest)} install_request
       */
      function Request(
        queries,
        updates,
        relation,
        model_request,
        locks,
        install_request
      ) {
        assert(
          typeof queries === "undefined" ||
            queries instanceof QueryRequest ||
            queries instanceof Array
        );
        assert(
          typeof updates === "undefined" ||
            updates instanceof UpdateRequest ||
            updates instanceof Array
        );
        assert(typeof relation === "undefined" || relation instanceof Array);
        assert(
          typeof model_request === "undefined" ||
            typeof model_request === "boolean"
        );
        assert(
          typeof locks === "undefined" ||
            locks instanceof TargetLock ||
            locks instanceof Array
        );
        if (queries) {
          this.query_request = _toArray(queries);
        }
        if (updates) {
          this.update_request = _toArray(updates);
        }
        if (model_request) {
          this.model_request = {};
        }
        this.relation = relation;
        if (locks) {
          this.target_lock = _toArray(locks);
        }
        if (install_request) {
          this.install_request = install_request;
        }
      }
      return Request;
    })();
    var EditabilityRequest = (function () {
      function EditabilityRequest(edited, visible) {
        assertIsArray(edited, MetricInter);
        assertIsArray(visible, MetricInter);
        this.edited = edited;
        this.visible = visible;
      }
      return EditabilityRequest;
    })();
    var EditabilityRequestRequest = (function () {
      /**
       * Specialized Request for editability.
       */
      function EditabilityRequestRequest(editability_request) {
        assert(editability_request instanceof EditabilityRequest);
        this.editability_request = editability_request;
      }
      return EditabilityRequestRequest;
    })();
    var ModelRequestRequest = (function () {
      /**
       * Specialized Request for model.
       */
      function ModelRequestRequest() {
        this.model_request = {};
      }
      return ModelRequestRequest;
    })();

    var _isAttribute = function (str) {
      if (str.indexOf(".") > 0) {
        return true;
      }
      return false;
    };
    var Target = function (target) {
      if (typeof target === "string") {
        if (_isAttribute(target)) {
          this.attribute = attribute(target);
        } else {
          this.metric = target;
        }
      } else if (target instanceof QualifiedLevel) {
        this.level = target;
      } else {
        throw "argument not supported";
      }
    };
    var attribute = function (qualifiedLevel, name) {
      var _splitAttr = function (str) {
        var splitted = str.split(".");
        return {
          attr: splitted[2],
          dim: splitted[0],
          level: splitted[1],
        };
      };
      if (typeof qualifiedLevel === "string") {
        var splitted = _splitAttr(qualifiedLevel);
        qualifiedLevel = new MQG.QualifiedLevel(splitted.dim, splitted.level);
        name = splitted.attr;
      }
      return {
        qualified_level: qualifiedLevel,
        name: name,
      };
    };
    var KeyRequest = function (qualifiedLevel, name) {
      var attr = attribute(qualifiedLevel, name);

      return {
        qualified_level: attr.qualified_level,
        attribute: attr.name,
      };
    };

    // Public API
    var MQG = {};
    MQG.Type = Type;
    MQG.ValueType = ValueType;
    MQG.BaseSignature = BaseSignature;
    MQG.Annotation = Annotation;
    MQG.Expr = Expr;
    MQG.AttributeExpr = AttributeExpr;
    MQG.MetricExpr = MetricExpr;
    MQG.FilterExpr = FilterExpr;
    MQG.DiceExpr = DiceExpr;
    MQG.AggExpr = AggExpr;
    MQG.HeaderSortExpr = HeaderSortExpr;
    MQG.OverrideExpr = OverrideExpr;
    MQG.WidenExpr = WidenExpr;
    MQG.VarExpr = VarExpr;
    MQG.AbsExpr = AbsExpr;
    MQG.AppExpr = AppExpr;
    MQG.InterArgument = InterArgument;
    MQG.ExprArgument = ExprArgument;
    MQG.OpExpr = OpExpr;
    MQG.DropExpr = DropExpr;
    MQG.DifferenceExpr = DifferenceExpr;
    MQG.DemoteExpr = DemoteExpr;
    MQG.PromoteExpr = PromoteExpr;
    MQG.CompositeExpr = CompositeExpr;
    MQG.ParamExpr = ParamExpr;
    MQG.ConstantExpr = ConstantExpr;
    MQG.LiteralExpr = LiteralExpr;
    MQG.CastExpr = CastExpr;
    MQG.Comparison = Comparison;
    MQG.QualifiedLevel = QualifiedLevel;
    MQG.Grouping = Grouping;
    MQG.Intersection = Intersection;
    MQG.MetricInter = MetricInter;
    MQG.Branch = Branch;
    MQG.RelationBinding = RelationBinding;
    MQG.Column = Column;
    MQG.Branch = Branch;
    MQG.UpdateExpr = UpdateExpr;
    MQG.SplitExpr = SplitExpr;
    MQG.RelabelExpr = RelabelExpr;
    MQG.LabelMap = LabelMap;
    MQG.Request = Request;
    MQG.UpdateTransform = UpdateTransform;
    MQG.UpdateQueryTransform = UpdateQueryTransform;
    MQG.UpdateRequest = UpdateRequest;
    MQG.QueryRequest = QueryRequest;
    MQG.InstallRequest = InstallRequest;
    MQG.EditabilityRequest = EditabilityRequest;
    MQG.EditabilityRequestRequest = EditabilityRequestRequest;
    MQG.ModelRequestRequest = ModelRequestRequest;
    MQG.Ancestor = Ancestor;
    MQG.TargetLock = TargetLock;
    MQG.Measure = MeasureExpr;
    MQG.Target = Target;
    MQG.PrevExpr = PrevExpr;
    MQG.Distribution = Distribution;
    MQG.RefineExpr = RefineExpr;
    MQG.KeyRequest = KeyRequest;

    return MQG;
  });
});

var MetaModel = function (metaModel) {
  this.metaModel = metaModel;
};

MetaModel.prototype.getMetrics = function () {
  return this.metaModel.model.metric.map(function (el) {
    return el.name;
  });
};
MetaModel.prototype.getAttributes = function () {
  var attributes = [];
  this.metaModel.model.dimension.forEach(function (dim) {
    dim.level.forEach(function (level) {
      level.attribute.forEach(function (att) {
        var label = att.label && att.label !== dim.name ? att.label + ":" : "";
        attributes.push(label + dim.name + "." + level.name + "." + att.name);
      });
    });
  });
  return attributes;
};
MetaModel.prototype.getAttributeType = function (attribute) {
  var dim = attribute.split(".")[0];
  dim = dim.indexOf(":") !== -1 ? dim.split(":")[1] : dim;
  var level = attribute.split(".")[1];
  var name = attribute.split(".")[2];

  var dimProperties = this.metaModel.model.dimension.filter(function (el) {
    return el.name === dim;
  })[0];
  if (!dimProperties) {
    throw "dimension " + dim + " does not exist";
  }
  var levelProperties = dimProperties.level.filter(function (el) {
    return el.name === level;
  })[0];
  if (!levelProperties) {
    throw "Level " + level + " does not exist";
  }
  var attProperties = levelProperties.attribute.filter(function (el) {
    return el.name === name;
  })[0];
  if (!attProperties) {
    throw "Attribute " + name + " does not exist";
  }
  return attProperties.type;
};

MetaModel.prototype._getMetricProperties = function (name) {
  var properties = this.metaModel.model.metric.filter(function (el) {
    return el.name === name;
  })[0];
  if (!properties) {
    throw "Metric " + name + " does not exist";
  }
  return properties;
};

MetaModel.prototype.getMetricIntersection = function (name) {
  var inter = this._getMetricProperties(name).signature.base_signature
    .intersection;
  if (Object.keys(inter).length === 0) {
    return [];
  }
  return inter.qualified_level.map(function (el) {
    var label = el.label && el.label !== el.dimension ? el.label + ":" : "";
    return label + el.dimension + "." + el.level;
  });
};

MetaModel.prototype.getIntersection = function (name) {
  if (name.indexOf(".") >= 0) {
    return [name.split(".").slice(0, -1).join(".")];
  } else {
    return this.getMetricIntersection(name);
  }
};

MetaModel.prototype.getMetricType = function (name) {
  return this._getMetricProperties(name).signature.base_signature.type
    ? this._getMetricProperties(name).signature.base_signature.type.type
    : undefined;
};

MetaModel.prototype.getMetricDimensions = function (name) {
  return this.getMetricIntersection(name).map(function (el) {
    return el.split(".")[0];
  });
};

MetaModel.prototype.getType = function (att) {
  if (att.indexOf(".") >= 0) {
    return this.getAttributeType(att);
  } else {
    return this.getMetricType(att);
  }
};
/* For entity typed metrics(metrics that have a custom type), the type of the entity refmode should
  be used when creating the relation source (Column property, like string_column)
*/
MetaModel.prototype.getPrimitiveType = function (name) {
  var kind = this.getType(name) ? this.getType(name).kind : undefined;
  if (kind === "NAMED") {
    return this.getBackingType(this.getMetricType(name).named);
  }
  return kind;
};
MetaModel.prototype.getBackingType = function (predicate) {
  var entries = this.metaModel.model.type.filter(function (el) {
    return el.type === predicate;
  });
  if (!entries.length) {
    throw "no backing type for predicate " + predicate;
  }
  return entries[0].backing_type.kind;
};

MetaModel.prototype.getMeasureDimensions = function (name) {
  if (name.indexOf(".") > -1) {
    return [name.split(".")[0]];
  } else {
    return this.getMetricDimensions(name);
  }
};

MetaModel.prototype.getSlideTargets = function (metric) {
  var target;
  this.metaModel.model.slide.forEach(function (el) {
    if (el.name === metric) {
      target = el.target;
    }
  });
  if (!target) {
    throw "Slide does not exit " + metric;
  }
  return target;
};

MetaModel.prototype.getSlideSources = function (metric) {
  var source;
  this.metaModel.model.slide.forEach(function (el) {
    if (el.name === metric) {
      source = el.source_level;
    }
  });
  if (!source) {
    throw "Slide does not exit " + metric;
  }
  return source;
};

MetaModel.prototype.getLevelDimension = function (level) {
  var levels = this.getLevels();
  var dim;
  for (var i = 0; i < levels.length; i++) {
    if (levels[i].split(".")[1] === level) {
      if (dim) {
        throw "conflicting records " + dim + " " + levels[i].split(".")[0];
      }
      dim = levels[i].split(".")[0];
    }
  }
  return dim;
};

MetaModel.prototype.getLevels = function () {
  var levels = [];
  for (var i = 0; i < this.metaModel.model.dimension.length; i++) {
    var dimension = this.metaModel.model.dimension[i];
    for (var j = 0; j < dimension.level.length; j++) {
      levels.push(dimension.name + "." + dimension.level[j].name);
    }
  }
  return levels;
};

var metamodel = MetaModel;

function Optimizer(rewrites, config) {
  this._rewrites = rewrites || [];
  this._tearDown = function (request, env) {
    return request;
  };
  this._filter = function () {
    return true;
  };
  this.config = config || {};
}
Optimizer.prototype.applyFilter = function (fn) {
  this._filter = fn;
  return this;
};
Optimizer.prototype.filter = function (expr, id) {
  return this._filter(expr, id);
};
Optimizer.prototype.addRewrite = function (fn) {
  if (Array.isArray(fn)) {
    this._rewrites = this._rewrites.concat(fn);
  } else {
    this._rewrites.push(fn);
  }
  return this;
};
Optimizer.prototype.tearDown = function (fn) {
  if (fn) {
    this._tearDown = fn;
  }
  return this;
};
Optimizer.prototype.rewrite = function (expr, env) {
  for (var i = 0; i < this._rewrites.length; i++) {
    var r = this._rewrites[i].curry(env);
    expr = r(expr);
  }
  return expr;
};
Optimizer.prototype.optimize = function (request, env) {
  if (this.config.optimize === false) {
    return request;
  }
  var i, j, k;
  var query_requests = request.query_request || [];
  for (j = 0; j < query_requests.length; j++) {
    var query_request = query_requests[j];
    var measure = query_request.measure || [];
    for (i = 0; i < measure.length; i++) {
      measure[i] = this.rewrite(measure[i], env);
    }
  }

  var update_requests = request.update_request || [];
  for (j = 0; j < update_requests.length; j++) {
    for (k = 0; k < update_requests[j].expr.length; k++) {
      var transform = update_requests[j].expr[k].transform;
      for (i = 0; i < transform.length; i++) {
        var spread = transform[i].spread_kind;
        if (spread.query) {
          spread.query.expr = this.rewrite(spread.query.expr, env);
        }
      }
    }
  }

  this._tearDown(request, env);
  return request;
};

var optimizer = Optimizer;

/* exteremly simple and naive implemetation of a dependecy injection container
 A particular important feature of this DI container is that the registred objects are scoped per container (not globally) and containers can be cloned.
 This opens the door for doing many useful tricks. An example is choosing whether you want an object to be a singleton or not.
 here's an example
 container = new Container();
 container.register('view', ['metamodel'], function(){}) // name of object, dependencies, definition
 container.register('introspect', ['metamodel'], function(){})
 container.register('util',[], function(){}) // Util will be instanciated at the container scope
 subContainer1 = container.clone();
 subContainer1.register('metaModel', [], function(){ return metaModel1}) // view, introspect will be intanciated at subcontainer1, using metaModel1
 subContainer2 = container.clone();
 subContainer2.register('metaModel', [], function(){ return metaModel2}) // view, introspect will be instanciated at subcontainer2, using metaModel2
*/
function Container(pending, instances) {
  this.pending = pending || {};
  this.instances = instances || {};
}
Container.prototype.get = function (name) {
  if (!(name in this.instances)) {
    throw "DI: " + name + " is not ready or does not exist";
  }
  return this.instances[name];
};

Container.prototype.register = function (name, deps, onReady) {
  deps = deps || [];
  if (typeof onReady !== "function") {
    onReady = function () {
      return onReady;
    };
  }
  for (var i = 0; i < deps.length; i++) {
    if (!(deps[i] in this.instances)) {
      this.pending[name] = {
        deps: deps,
        onReady: onReady,
      };
      return;
    }
  }
  var that = this;
  this.instances[name] = onReady.apply(
    null,
    deps.map(function (el) {
      return that.instances[el];
    })
  );
  this.checkoutReadyDeps();
};

Container.prototype.checkoutReadyDeps = function () {
  var reload = false;
  var that = this;
  for (var service in this.pending) {
    var deps = this.pending[service].deps;
    var ready = true;
    for (var i = 0; i < deps.length; i++) {
      if (!(deps[i] in this.instances)) {
        ready = false;
        break;
      }
    }
    if (ready) {
      this.instances[service] = this.pending[service].onReady.apply(
        null,
        deps.map(function (el) {
          return that.instances[el];
        })
      );
      delete this.pending[service];
      reload = true;
    }
  }
  if (reload) {
    this.checkoutReadyDeps();
  }
};

Container.prototype.clone = function () {
  return new Container(
    util.deepClone(this.pending),
    util.deepClone(this.instances)
  );
};

/* Create a singleton that objects can register themselves to it by doing di.register()
 * This allows the objects to register themselfs to a global instance, rather than having a main function or a caller to register them.
 */

var di = new Container();
var di_1 = {
  register: di.register.bind(di),
  get: di.get.bind(di),
  container: di,
  Container: Container,
};

var LOGLEVELS = {
  DEBUG: {
    name: "DEBUG",
    level: 0,
  },
  INFO: {
    name: "INFO",
    level: 1,
  },
  NONE: {
    name: "NONE",
    level: 1000,
  },
};

var loglvl = LOGLEVELS.NONE.name;

var setLogLevel = function (lvl) {
  if (LOGLEVELS.hasOwnProperty(lvl)) {
    loglvl = lvl;
  } else {
    throw lvl + " is not a valid log level. Use INFO or DEBUG...";
  }
};

var assertLevel = function (level) {
  try {
    return (
      level.hasOwnProperty("name") &&
      Object.keys(LOGLEVELS)
        .map(function (el) {
          return LOGLEVELS[el].name;
        })
        .indexOf(level.name) > -1
    );
  } catch (e) {
    console.log(e);
    throw "Level not recognized. Use INFO or DEBUG...";
  }
};

var log = function (lvl, msg) {
  assertLevel(lvl);
  if (lvl.level >= LOGLEVELS[loglvl].level) {
    console.log(lvl.name + ": " + msg);
  }
};

var logger = {
  log: log,
  setLogLevel: setLogLevel,
  LOGLEVELS: LOGLEVELS,
};

di_1.register("Filter", ["MQG"], function (MQG) {
  var Filter = function (id, expr) {
    if (!(this instanceof Filter)) {
      return new Filter(id, expr);
    }
    if (!id && !expr) {
      throw "invalid filter call: no filter expression passed";
    }
    if (id && !expr) {
      expr = id;
      id = null;
    }
    if (!id) {
      id = "untitled";
    }

    this._id = id;
    if (typeof expr !== "function") {
      this._expr = function () {
        return expr;
      };
    } else {
      this._expr = expr;
    }
    return this;
  };
  Filter.prototype.id = function () {
    return this._id;
  };
  Filter.prototype.expr = function (parameterRepo) {
    var id = this._id;
    var original_filterBy = MQG.Expr.prototype.filterBy;
    MQG.Expr.prototype.filterBy = function (value, options) {
      options = options || {};
      var disjunction = options.disjunction;
      id = options.id || id;
      var constant = options.constant || false;
      if (constant) {
        return original_filterBy.call(this, value, disjunction);
      }
      return original_filterBy.call(
        this,
        value,
        disjunction,
        id,
        parameterRepo
      );
    };
    var expr = this._expr.call(this, parameterRepo);
    MQG.Expr.prototype.filterBy = original_filterBy;
    return expr;
  };

  return Filter;
});

di_1.register("FilterSet", ["Filter", "optimizer"], function (
  Filter,
  optimizer
) {
  /**
   * An immutable container (set) of Filter objects.
   * MeasureGenertors use instances of this class to construct
   * <i>dicers</i> when measuring the base measure or attribute
   * of a given MeasureConf.
   *
   * @class FiltersSet
   * @constructor
   * @param {Filter[]} filters an array of filters
   */

  function FiltersSet(filters) {
    this._filters = util.toArray(filters);
  }

  FiltersSet.prototype.add = function (id, expr) {
    var filter = id;
    if (!(id instanceof Filter)) {
      filter = new Filter(id, expr);
    }
    var clone = this.clone();
    clone._filters.push(filter);
    return clone;
  };

  FiltersSet.prototype.clone = function () {
    return new FiltersSet(this._filters.slice());
  };

  FiltersSet.prototype.get = function (id) {
    var fs = this._filters.filter(function (el) {
      return el.id() === id;
    });
    if (id && fs.length) {
      return fs[0];
    }
  };

  FiltersSet.prototype.extend = function (filterSet) {
    var clone = this.clone();
    // using internal methods for performance reasons
    for (var i = 0; i < filterSet._filters.length; i++) {
      clone._filters.push(filterSet._filters[i]);
    }
    return clone;
  };

  FiltersSet.prototype._filterByComp = function (comparisonFn) {
    var i = 0;
    var clone = this.clone();
    while (i < clone._filters.length) {
      if (!comparisonFn(clone._filters[i])) {
        // filter not in metric intersection
        clone._filters.splice(i, 1);
      } else {
        i++;
      }
    }
    return clone;
  };
  /**
   * exclude specified filters ids from the filters set
   * @method excludeFilters
   * @param {string[]} filters ids to exclude
   * @return {FiltersSet} new filters set
   */
  FiltersSet.prototype.excludeFilters = function (filtersIds) {
    filtersIds = util.toArray(filtersIds);
    return this._filterByComp(function (filter) {
      return filtersIds.indexOf(filter.id()) === -1;
    });
  };
  /**
   * only include filters that match the specified filters ids
   * @method includeFilters
   * @param {string[]} filters ids to include
   * @return {FiltersSet} new filters set
   */
  FiltersSet.prototype.includeFilters = function (filtersIds) {
    return this._filterByComp(function (filter) {
      return filtersIds.indexOf(filter.id()) > -1;
    });
  };
  /**
   * generates appropriate dicers array
   *
   * @method generate
   * @param {ParameterRepo} Parameter repository to be used for parameter terms
   * @return {Expr[]} an array of dicers
   */
  FiltersSet.prototype.generate = function (expr, parameterRepo) {
    var i, j;
    var filters = this._filters.sort(function (a, b) {
      return a.id() > b.id();
    });

    var fdicers = [];
    filters.forEach(function (el) {
      var exprs = el.expr(parameterRepo);
      if (!exprs) {
        return;
      }
      if (!Array.isArray(exprs)) {
        exprs = [exprs];
      }
      exprs.forEach(function (e) {
        fdicers.push({ id: el.id(), expr: e });
      });
    });

    //remove redundant exprs
    fdicers = fdicers.filter(function (el, index) {
      return (
        util.arrayIndexOf(fdicers, function (e) {
          return JSON.stringify(el.expr) === JSON.stringify(e.expr);
        }) === index
      );
    });

    if (optimizer.excludeFilter) {
      fdicers = fdicers.filter(function (el) {
        return !optimizer.excludeFilter(expr, el.id, el.expr);
      });
      var fdicersClone = fdicers.slice();
      // remove filters that are superset of others
      for (i = 0; i < fdicers.length; i++) {
        for (j = 0; j < fdicers.length; j++) {
          if (j === i) {
            continue;
          }
          if (
            optimizer.excludeFilter(
              fdicers[i].expr,
              fdicers[j].id,
              fdicers[j].expr
            )
          ) {
            //A is subset of B doesn't mean that it's safe to remove the B dicer. We need to check that they are at the same itnersection
            var dims = expr.getDimensions();
            var a = fdicers[i].expr.getIntersection().filter(function (el) {
              return dims.indexOf(el.split(".")[0]) > -1;
            });
            var b = fdicers[j].expr.getIntersection().filter(function (el) {
              return dims.indexOf(el.split(".")[0]) > -1;
            });
            if (!util.equals(a, b)) {
              continue;
            }
            var index = util.arrayIndexOf(fdicersClone, function (el) {
              return (
                JSON.stringify(el.expr) === JSON.stringify(fdicers[j].expr)
              );
            });
          }
        }
      }
      fdicers = fdicersClone;
    }

    return fdicers.map(function (el) {
      return el.expr;
    });
  };
  return FiltersSet;
});

function is_sorted(arr) {
  var len = arr.length - 1;
  for (var i = 0; i < len; ++i) {
    if (arr[i] > arr[i + 1]) {
      return false;
    }
  }
  return true;
}

function isInt128Column(valueKey, valueObj) {
  return valueKey === "int128_column" && valueObj.high && valueObj.low;
}

var parseKeyedReport = function (origin_map, fields) {
  /* Parse the Measure service response using a sort-merge algotihm
Add a comment to this line
       * First, all columns are sorted according to the id column which preced every values column.
       * Second, apply a join across all columns at once(not pair-wise), by holding an array of indexes of columns and taking the minimal id each time, and create an entry in the final array based on it.
       * ideally,  such processing should be done in the backend not the front-end...
       * also, the measure service can relieve MQG from doing the sorting process by returning sorted index columns...
       */
  if (fields.length * 2 !== origin_map.length) {
    console.warn("fields do not match res length");
  }

  /*
   * resulted map after the first step(sorting), the format of sorted_map is:
   * [
   *  [{id:253,value:'amine'},{id:257,value:'johnny'}],
   *  [{id:253,value:'super-user'}, {id:257,value:'normal-user'}],
   *  [{id:257,value:'not-empty}]
   * ]
   */
  var sorted_map = [];
  var n = 18;
  var padString = "";
  while (n-- > 0) padString += "0";

  for (var i = 0; i < origin_map.length / 2; i++) {
    var col = i * 2;
    var index_col = origin_map[col].int_column.value || [];
    var value_key = Object.keys(origin_map[col + 1])[0];
    var value_obj = origin_map[col + 1][value_key];
    var value_col;
    var fraction;
    if (value_key === "decimal_column" && value_obj.negative) {
      value_col = [];
      for (var col_i = 0; col_i < value_obj.negative.length; col_i++) {
        /*
         *  Here we are getting the exact value of the decimal using a join on string parts, while ensuring that the fractional part
         *    is always adjusted to be 18 digit long.  We remove the insignificant 0s after constructing the full decimal number.
         *
         *  Another approach to calculating the decimal is to use Javascript math on the parts (sign, integer and fraction), like so
         *    Value = ((value_obj.negative[col_i] ? -1 : 1) * (value_obj.integral_digit[col_i] + (value_obj.fraction_digit[col_i] * Math.pow(10,-18))))
         *
         *  However, the above float-operations approach leads to imprecision in low-order decimal digits.  The reasons are
         *    1. Float operations are inherently imprecise on low-order decimal digits
         *    2. Whereas LB returns fractional parts to the 18th digit, javascript float is only upto the 16th digit
         */

        fraction = value_obj.fraction_digit[col_i].toString();
        fraction = (padString.slice(fraction.length) + fraction).replace(
          /0+$/,
          ""
        );
        value_col.push(
          (value_obj.negative[col_i] ? "-" : "") +
            value_obj.integral_digit[col_i] +
            (fraction ? "." + fraction : "")
        );
      }
    } else if (isInt128Column(value_key, value_obj)) {
      value_col = Object.keys(value_obj.high).map(function (index) {
        return { high: value_obj.high[index], low: value_obj.low[index] };
      });
    } else {
      value_col = value_obj.value;
    }
    // frequently the indexes are already sorted.
    if (is_sorted(index_col)) {
      sorted_map.push(index_col || []);
      sorted_map.push(value_col || []);
    } else {
      // can be further optimized
      var obj_arr = index_col
        .map(function (el, i) {
          return {
            id: el,
            value: value_col[i],
          };
        })
        .sort(function (a, b) {
          if (a.id < b.id) return -1; //jshint ignore:line
          if (a.id > b.id) return 1; //jshint ignore:line
          return 0;
        });
      sorted_map.push(
        obj_arr.map(function (el) {
          return el.id;
        })
      );
      sorted_map.push(
        obj_arr.map(function (el) {
          return el.value;
        })
      );
    }
  }
  // The join is done across all columns at once (not pairwise)
  // The indexes maintains the index we're currently in for every column
  var indexes = [];
  for (var i = 0; i < sorted_map.length / 2; i++) {
    // start from 0, a check has to be made for empty columns
    indexes.push(0);
  }
  // get the minimal id
  var get_min_id = function () {
    var min = null;
    for (var i = 0; i < indexes.length; i++) {
      var el = sorted_map[i * 2][indexes[i]];
      if (el < min || min == null) {
        min = el;
      }
    }
    return min;
  };

  var keyed_entries = [];

  var prevEntryId = null;
  var prevEntry = null;

  while (true) {
    var entry = {};

    //used to sort response in a determenestic way efficiently
    var sort_str = "";
    var id = get_min_id();
    if (id == null) {
      //jshint ignore:line
      // get_min_id returns null when there is no element that corresponds to any index in indexes array, which means we're done
      break;
    }
    for (var j = 0; j < indexes.length; j++) {
      var index_col = sorted_map[j * 2];
      var value_col = sorted_map[j * 2 + 1];
      if (index_col[indexes[j]] != null && index_col[indexes[j]] === id) {
        entry[fields[j]] = value_col[indexes[j]];
        sort_str += value_col[indexes[j]];
        indexes[j]++;
      } else {
        entry[fields[j]] = null;
      }
    }

    // COLLECT case, so need to update the previous entry, adding values
    if (prevEntryId === id) {
      for (var key in entry) {
        if (Object.prototype.hasOwnProperty.call(entry, key)) {
          if (entry[key] !== null) {
            var values = Array.isArray(prevEntry[key])
              ? prevEntry[key]
              : [prevEntry[key]];
            values.push(entry[key]);
            prevEntry[key] = values;
          }
        }
      }
    } else {
      keyed_entries.push({
        index: keyed_entries.length,
        key: sort_str,
        entry: entry,
      });

      prevEntryId = id;
      prevEntry = entry;
    }
  }

  var sorted_entries = keyed_entries.sort(function (a, b) {
    if (a.key < b.key) {
      return -1;
    } else if (a.key > b.key) {
      return 1;
    }

    return a.index - b.index;
  });

  return sorted_entries.map(function (x) {
    return x.entry;
  });
};
// Adapter of non keyed queries(numberOfRows = false), which convert response to a keyed response by inserting fake keys
var parseNonKeyedResponse = function (report_column, fields) {
  fields = util.toArray(fields);
  if (fields.length !== report_column.length) {
    console.warn("fields do not match res length");
  }
  var result = [];
  var currentColumn;
  var key;
  var val;
  for (var i = 0; i < report_column.length; i++) {
    currentColumn = report_column[i];
    key = null;
    for (key in currentColumn) {
    }
    val = currentColumn[key].value;
    if (!val) {
      val = [];
    } else if (val.length > 1 && report_column.length > 1);
    var keysArr = [];
    for (var j = 0; j < val.length; j++) {
      keysArr.push(j);
    }
    result.push({
      int_column: {
        value: keysArr,
      },
    });
    result.push(currentColumn);
  }

  return parseKeyedReport(result, fields);
};

var responseparser = {
  parseKeyedReport: parseKeyedReport,
  parseNonKeyedResponse: parseNonKeyedResponse,
};

di_1.register("ViewCommon", [], function () {
  function _compileMeasureName(name, levels) {
    if (name.indexOf(".*.") >= 0) {
      var dim = name.split(".")[0];
      if (levels && levels[dim]) {
        return name.replace("*", levels[dim]);
      } else {
        // no matching level
        throw "no level specified, cannot replace wildcard of " + name;
      }
    }
    return name;
  }

  function AttributeConfig(id, name) {
    this.name = name || id;
    this.id = id;
  }
  AttributeConfig.prototype.generateExpr = function (levels) {
    if (this.name instanceof MQG.AttributeExpr) {
      return this.name;
    }
    return new MQG.AttributeExpr(_compileMeasureName(this.name, levels));
  };
  AttributeConfig.prototype.generateKey = function (levels) {
    return new MQG.KeyRequest(_compileMeasureName(this.name, levels));
  };

  function MeasureConfig(id, name, agg) {
    this.id = id;
    if (!name) {
      name = id;
    }
    if (id && name && !agg && typeof name === "string") {
      if (
        [
          "COLLECT",
          "AMBIG",
          "TOTAL",
          "MIN",
          "MAX",
          "COUNT",
          "MODE",
          "COUNT_DISTINCT",
          "HISTOGRAM",
          "AVERAGE",
          "COVER",
        ].indexOf(name) > -1
      ) {
        agg = name;
        name = id;
      }
    }
    if (typeof name === "function") {
      this.expr = name;
    } else if (name instanceof MQG.Expr) {
      this.expr = function () {
        return name;
      };
    } else {
      this.expr = function (dynamics) {
        if (!agg) {
          var primitiveType = MQG.Measure(name).getPrimitiveType();
          if (
            primitiveType === "STRING" ||
            primitiveType === "BOOLEAN" ||
            primitiveType === "NAMED"
          ) {
            agg = "AMBIG";
          } else {
            agg = "TOTAL";
          }
        }
        return new MQG.Measure(name)
          .diceBy(dynamics.filters)
          .groupBy(agg, dynamics.levels);
      };
    }
  }

  MeasureConfig.prototype.generate = function (
    filters,
    levels,
    params,
    relationsRepo
  ) {
    var original_measure = MQG.Measure;
    var original_diceBy = MQG.Expr.prototype.diceBy;

    MQG.Expr.prototype.diceBy = function (dicers, disjunction) {
      return original_diceBy.call(this, dicers, disjunction, relationsRepo);
    };

    MQG.Measure = function (name, intersection) {
      if (name.indexOf(".") >= 0) {
        return new AttributeConfig(name).generateExpr(levels);
      } else {
        return new MQG.MetricExpr(name, intersection);
      }
    };
    MQG.Relation = function (source, type, singleton) {
      var levels = source[0];
      if (!levels) {
        throw "invalid source, " + source;
      }
      var values = source.slice(1);
      return relationsRepo.addRelation(
        _generate_spread_relation_name("relation", levels),
        levels,
        values,
        type,
        singleton
      );
    };
    var result = this.expr({
      filters: filters,
      levels: levels,
      params: params,
    });
    MQG.Relation = null;
    MQG.Expr.prototype.diceBy = original_diceBy;
    MQG.Measure = original_measure;
    return result;
  };

  // This function is useful to extract the portion of the source responsible for spreading to a given measure (source can include multiple measures to spread to)
  function filterSource(source, measure) {
    var headers = source[0];
    var measureIndex = headers.indexOf(measure);
    if (measureIndex === -1) {
      throw "Measure " + measure + " is not in the source";
    }
    var levels = headers.filter(function (key) {
      return key.indexOf(".") > -1;
    });
    var filteredValues = source.slice(1).map(function (entry) {
      return entry.filter(function (val, pos) {
        return pos === measureIndex || pos < levels.length;
      });
    });
    return [levels.concat([measure])].concat(filteredValues);
  }

  return {
    AttributeConfig: AttributeConfig,
    MeasureConfig: MeasureConfig,
    filterSource: filterSource,
  };
});

di_1.register(
  "QueryView",
  ["generators", "FilterSet", "Filter", "ViewCommon", "Request"],
  function (generators, FilterSet, Filter, ViewCommon, Request) {
    var MeasureConfig = ViewCommon.MeasureConfig;
    var AttributeConfig = ViewCommon.AttributeConfig;

    function QueryView(reportName) {
      if (!(this instanceof QueryView)) {
        return new QueryView(reportName);
      }
      if (typeof reportName === "object") {
        this.reportName = reportName.reportName;
        this.attributes = reportName.attributes;
        this.measures = reportName.measures;
        this.return_row_numbers = reportName.return_row_numbers;
        this.filters = reportName.filters;
        this.levels = reportName.levels;
        this.preProcessor = reportName.preProcessor;
        this.postProcessor = reportName.postProcessor;
        this._params = reportName.params;
        this.type = reportName.type;
        this.row_limit = reportName.row_limit;
        this.install_logic = reportName.install_logic;
        return this;
      }
      this.type = "query"; // adding an annotation to avoid using instanceof to differentiate between different view classes. The problem with instanceof in this scenario is that it needs the class type for comparison. therefore the query class needs to be injected. a case of this problem is with the Request class which needs to know the type of the view class in order to construct the json request. which will cause a cyclic reference, since Request is injected in the view class to allow getting the response in a convenient way.
      this.reportName = reportName || "untitled";
      this.attributes = [];
      this.measures = [];
      this.return_row_numbers = true;
      this.postProcessor = function (data) {
        return data;
      };
      this.preProcessor = function () {};
      this.filters = new FilterSet();
      this.levels = {};
      this._params = [];
    }

    QueryView.prototype.clone = function () {
      return new QueryView({
        reportName: this.reportName,
        attributes: this.attributes.slice(),
        measures: this.measures.slice(),
        return_row_numbers: this.return_row_numbers,
        postProcessor: this.postProcessor,
        preProcessor: this.preProcessor,
        filters: this.filters.clone(),
        levels: util.deepClone(this.levels),
        params: this._params,
        type: this.type,
        row_limit: this.row_limit,
        install_logic: this.install_logic,
      });
    };

    QueryView.prototype.attribute = function (id, attribute) {
      var attributeConfig = new AttributeConfig(id, attribute);
      this._assertUniqueId(attributeConfig.id);
      var clone = this.clone();
      clone.attributes.push(attributeConfig);
      return clone;
    };

    QueryView.prototype.measure = function (id, measure, agg) {
      var measureConfig = new MeasureConfig(id, measure, agg);
      this._assertUniqueId(measureConfig.id);
      var clone = this.clone();
      clone.measures.push(measureConfig);
      return clone;
    };

    QueryView.prototype.postProcess = function (fn) {
      var clone = this.clone();
      clone.postProcessor = fn;
      return clone;
    };

    QueryView.prototype.preProcess = function (fn) {
      var clone = this.clone();
      clone.preProcessor = fn;
      return clone;
    };

    QueryView.prototype.setLimit = function (n) {
      var clone = this.clone();
      clone.row_limit = n;
      return clone;
    };

    QueryView.prototype.diceBy = function (id, expr) {
      var clone = this.clone();
      if (id instanceof FilterSet) {
        clone.filters = id.clone();
      } else {
        clone.filters = clone.filters.add(new Filter(id, expr));
      }
      return clone;
    };

    QueryView.prototype.setParams = function (params) {
      var clone = this.clone();
      clone._params = params;
      return clone;
    };

    QueryView.prototype.getParams = function () {
      return this._params;
    };

    QueryView.prototype.groupBy = function (levels) {
      var clone = this.clone();
      if (typeof levels === "string") {
        levels = [levels];
      }
      if (Array.isArray(levels)) {
        var obj = {};
        levels.forEach(function (el) {
          obj[el.split(".")[0]] = el.split(".").slice(1).join(".");
        });
        levels = obj;
      }
      levels = levels || {};

      clone.levels = levels;
      return clone;
    };

    QueryView.prototype.process = function (relationsRepo) {
      var view = this.preProcessor(relationsRepo) || this;
      view.attributes = view.attributes.filter(function (el) {
        if (typeof el !== "string") {
          return true;
        }
        return (
          el.name.indexOf(".*.") === -1 || view.levels[el.name.split(".")[0]]
        );
      });
      return view;
    };

    QueryView.prototype.install = function () {
      this.type = "install";
      return this;
    };

    QueryView.prototype.generate = function (relationsRepo) {
      if (!relationsRepo) {
        var request = new Request();
        request.view(this);
        return request.generate();
      }
      var view = this.process(relationsRepo);
      var measures = view.measures.map(function (el) {
        return el.generate(
          view.filters,
          view.levels,
          view._params,
          relationsRepo
        );
      });
      if (view.type === "install") {
        return new MQG.InstallRequest(measures);
      }
      return new MQG.QueryRequest(
        view.reportName,
        view.attributes.map(function (el) {
          return el.generateKey(view.levels);
        }),
        measures,
        view.return_row_numbers,
        undefined,
        view.row_limit,
        false
      );
    };

    QueryView.prototype.get = function (headers) {
      var request = new Request();
      request.view(this);
      return request.getResponse(headers).then(function (data) {
        return data[0];
      });
    };

    QueryView.prototype.parseResponse = function (data) {
      var view = this.process();
      var result;
      if (this.return_row_numbers) {
        result = responseparser.parseKeyedReport(data, view._getFields());
      } else {
        result = responseparser.parseNonKeyedResponse(data, view._getFields());
      }
      return this.postProcessor(result);
    };

    QueryView.prototype.installLogic = function (value) {
      var clone = this.clone();

      clone.install_logic = value;

      return clone;
    };

    QueryView.prototype._assertUniqueId = function (id) {
      if (
        this._getFields().filter(function (el) {
          return el === id;
        }).length > 0
      ) {
        throw "duplicate id " + id;
      }
    };
    QueryView.prototype._getFields = function () {
      return this.attributes
        .map(function (el) {
          return el.id;
        })
        .concat(
          this.measures.map(function (el) {
            return el.id;
          })
        );
    };
    return QueryView;
  }
);

di_1.register(
  "SpreadView",
  ["metaModel", "generators", "Filter", "FilterSet", "Request", "ViewCommon"],
  function (metaModel, generators, Filter, FilterSet, Request, ViewCommon) {
    var MeasureConfig = function (measure, expr) {
      this._measure = measure;
      if (typeof expr !== "function") {
        var base = expr || measure;
        this._expr = function (d) {
          return d.source.diceBy(d.filters)[d.method + "Spread"]({
            destination: measure,
            base: base,
            filters: d.filters,
          });
        };
      } else {
        this._expr = expr;
      }
    };
    MeasureConfig.prototype.measure = function () {
      return this._measure;
    };
    MeasureConfig.prototype.expr = function (
      source,
      method,
      filters,
      relationsRepo
    ) {
      var original_diceBy = MQG.Expr.prototype.diceBy;

      MQG.Expr.prototype.diceBy = function (dicers, disjunction) {
        return original_diceBy.call(this, dicers, disjunction, relationsRepo);
      };

      var result = this._expr({
        source: source,
        method: method,
        filters: filters,
      });
      MQG.Expr.prototype.diceBy = original_diceBy;
      return result;
    };
    var SpreadView = function (reportName) {
      if (!(this instanceof SpreadView)) {
        return new SpreadView(reportName);
      }
      this.type = "spread";
      if (typeof reportName === "object") {
        this.reportName = reportName.reportName;
        this._filters = reportName.filters.clone();
        this._preProcessor = reportName.preProcessor;
        this._measures = reportName.measures.slice();
        this._source = util.deepClone(reportName.source);
        this._method = reportName.method;
        return this;
      }
      this.reportName = reportName;
      this._preProcessor = function () {};
      this._filters = new FilterSet();
      this._measures = [];
      this._source = [];
      this._method = "replicate";
      return this;
    };
    SpreadView.prototype.clone = function () {
      return new SpreadView({
        reportName: this.reportName,
        preProcessor: this._preProcessor,
        filters: this._filters,
        source: this._source,
        measures: this._measures,
        method: this._method,
      });
    };
    SpreadView.prototype.measure = function (metric, expr) {
      var clone = this.clone();
      clone._measures.push(new MeasureConfig(metric, expr));
      return clone;
    };
    SpreadView.prototype.method = function (method) {
      var clone = this.clone();
      clone._method = method;
      return clone;
    };
    SpreadView.prototype.preProcess = function (fn) {
      var clone = this.clone();
      clone._preProcessor = fn;
      return clone;
    };
    SpreadView.prototype.parseResponse = function (data) {
      return data;
    };

    SpreadView.prototype.diceBy = function (id, expr) {
      var clone = this.clone();
      if (id instanceof FilterSet) {
        clone._filters = id.clone();
      } else {
        clone._filters = clone._filters.add(new Filter(id, expr));
      }
      return clone;
    };

    SpreadView.prototype.source = function (source) {
      var clone = this.clone();
      clone._source = source;
      return clone;
    };
    SpreadView.prototype.get = function (headers) {
      var request = new Request();
      request.view(this);
      return request.getResponse(headers);
    };
    SpreadView.prototype._relation = function (relationsRepo, sortMeasure) {
      var source = util.deepClone(this._source);
      if (source[0].indexOf(sortMeasure) === -1) {
        source[0] = source[0].concat([sortMeasure]);
      }
      var filtered_source = ViewCommon.filterSource(source, sortMeasure);
      var levels = filtered_source[0].slice(0, -1);
      var values = filtered_source.slice(1);
      //remove null values, to allow a more flexible syntax
      values = values.filter(function (el) {
        if (el[el.length - 1] == null) {
          return false;
        }
        return true;
      });
      return relationsRepo.addRelation(
        _generate_spread_relation_name("spread", levels),
        levels,
        values,
        this._method === "percentage"
          ? new MQG.Type("FLOAT")
          : metaModel.getType(sortMeasure)
      );
    };
    SpreadView.prototype._getMeasures = function () {
      return this._measures.length
        ? this._measures
        : this._source[0]
            .filter(function (el) {
              return el.indexOf(".") === -1;
            })
            .map(function (el) {
              return new MeasureConfig(el, el);
            });
    };

    SpreadView.prototype.generate = function (relationsRepo) {
      var view = this._preProcessor(relationsRepo) || this;
      return view._getMeasures().map(function (measureConf) {
        var sourceExpr = view._relation(relationsRepo, measureConf.measure());
        var expr = measureConf.expr(
          sourceExpr,
          view._method,
          view._filters,
          relationsRepo
        );
        return new MQG.UpdateRequest(
          generators.spreadByQuery(measureConf.measure(), expr, sourceExpr)
        );
      });
    };
    return SpreadView;
  }
);

di_1.register(
  "RemoveView",
  ["SpreadView", "ViewCommon", "generators", "metaModel"],
  function (SpreadView, ViewCommon, generators, metaModel) {
    var MeasureConfig = function (measure, expr) {
      this._measure = measure;
      if (typeof expr !== "function") {
        this._expr = function (dynamics) {
          return dynamics.source
            .diceBy(dynamics.filters)
            .widen(metaModel.getIntersection(measure))
            .diceBy(dynamics.filters);
        };
      }
    };
    MeasureConfig.prototype.measure = function () {
      return this._measure;
    };
    MeasureConfig.prototype.expr = function (source, filters, relationsRepo) {
      var original_diceBy = MQG.Expr.prototype.diceBy;

      MQG.Expr.prototype.diceBy = function (dicers, disjunction) {
        return original_diceBy.call(this, dicers, disjunction, relationsRepo);
      };

      var result = this._expr({
        source: source,
        filters: filters,
      });
      MQG.Expr.prototype.diceBy = original_diceBy;
      return result;
    };

    var RemoveView = function (reportName) {
      if (!(this instanceof RemoveView)) {
        return new RemoveView(reportName);
      }
      SpreadView.call(this, reportName);
      this.type = "remove";
    };
    RemoveView.prototype = Object.create(SpreadView.prototype);
    RemoveView.prototype.clone = function () {
      return new RemoveView({
        reportName: this.reportName,
        preProcessor: this._preProcessor,
        filters: this._filters,
        source: this._source,
        measures: this._measures,
        method: this._method,
      });
    };
    RemoveView.prototype._relation = function (relationsRepo, sortMeasure) {
      var source = util.deepClone(this._source);
      if (source[0].indexOf(sortMeasure) === -1) {
        source[0] = source[0].concat([sortMeasure]);
      }
      var filtered_source = ViewCommon.filterSource(source, sortMeasure);
      var levels = filtered_source[0].slice(0, -1);
      var values = filtered_source.slice(1);

      return relationsRepo.addRelation(
        _generate_spread_relation_name("remove", levels),
        levels,
        values
      );
    };

    RemoveView.prototype.measure = function (metric, expr) {
      var clone = this.clone();
      clone._measures.push(new MeasureConfig(metric, expr));
      return clone;
    };

    RemoveView.prototype._getMeasures = function () {
      return this._measures.length
        ? this._measures
        : this._source[0]
            .filter(function (el) {
              return el.indexOf(".") === -1;
            })
            .map(function (el) {
              return new MeasureConfig(el, el);
            });
    };
    RemoveView.prototype.generate = function (relationsRepo) {
      var view = this._preProcessor(relationsRepo) || this;
      return view._getMeasures().map(function (measureConf) {
        var sourceExpr = view._relation(relationsRepo, measureConf.measure());
        var expr = new MQG.DropExpr(
          measureConf.expr(sourceExpr, view._filters, relationsRepo)
        );
        return new MQG.UpdateRequest(
          generators.removeByQuery(measureConf.measure(), expr, sourceExpr)
        );
      });
    };
    return RemoveView;
  }
);

di_1.register("Request", ["optimizer", "generators"], function (
  optimizer,
  generators
) {
  var Request = function () {
    if (!(this instanceof Request)) {
      return new Request();
    }
    this.views = [];
  };
  Request.prototype.view = function (view) {
    this.views.push(view);
    return this;
  };
  Request.prototype.generate = function () {
    if (this.views.length === 0) {
      throw "no Relax views added";
    }
    var queries = [];
    var updates = [];
    var installs = [];

    var relationsRepo = new generators.RelationsRepo();

    for (var i = 0; i < this.views.length; i++) {
      var view = this.views[i];
      if (view.type === "query" || view.type === "transaction") {
        queries.push(view.generate(relationsRepo));
      } else if (view.type === "install") {
        installs.push(view.generate(relationsRepo));
      } else {
        updates = updates.concat(util.toArray(view.generate(relationsRepo)));
      }
    }

    var req = new MQG.Request(
      queries,
      updates,
      relationsRepo.genRelations(),
      undefined,
      undefined,
      installs
    );

    req = optimizer.optimize(req, {
      relationRepo: relationsRepo,
    });

    req.install_logic = this._resolveInstallLogic(this.views);

    logger.log(logger.LOGLEVELS.DEBUG, JSON.stringify(req));

    return req;
  };

  Request.prototype._resolveInstallLogic = function (views) {
    var filteredViews = views.filter(function (v) {
      return v.install_logic !== undefined;
    });

    return filteredViews.length
      ? filteredViews.every(function (v) {
          return v.install_logic;
        })
      : undefined;
  };

  Request.prototype.parseResponse = function (data) {
    var queryViews = this.views.filter(function (el) {
      return el.type === "query" || el.type === "transaction";
    });
    if (!queryViews.length) {
      return [];
    }

    var res = [];
    if (data && data.report) {
      // throw if the limit is exceeded
      if (data.row_limit_exceeded) {
        throw {
          error: "row_limit_exceeded",
          message: "The response was larger than the requested limit",
        };
      }
      var report = data.report;
      for (var i = 0; i < report.length; i++) {
        if (report[i].report_column) {
          res.push(queryViews[i].parseResponse(report[i].report_column));
        } else {
          throw "Exception when parsing response :" + JSON.stringify(report[i]);
        }
      }
    } else {
      var errorMessage = data;
      if (!data) {
        errorMessage = "empty response";
      } else if (data.problem && data.problem.length && data.problem[0].text) {
        errorMessage = data.problem[0].text;
      }
      throw "Exception when parsing response :" + JSON.stringify(errorMessage);
    }
    return res;
  };
  Request.prototype.getResponse = function (headers) {
    if (headers && !headers.url) {
      headers = {
        url: headers,
      };
    }
    if (!headers || !headers.url) {
      throw "invalid url";
    }
    var query = this.generate();
    var that = this;
    // storing the request promise, needed for abort to work
    this.requestPromise = new util.Promise();
    util.postJSON("POST", headers, query).then(
      function (res) {
        try {
          that.requestPromise.resolve(that.parseResponse(res));
        } catch (err) {
          that.requestPromise.reject(err);
        }
      },
      function (err) {
        that.requestPromise.reject(err);
      }
    );

    return this.requestPromise;
  };
  Request.prototype.abort = function () {
    this.requestPromise.reject("Request aborted");
  };
  return Request;
});

function _generate_spread_relation_name(prefix, levels) {
  var relationName = prefix;
  if (levels.length === 0) {
    return relationName + "_empty";
  }
  levels.forEach(function (el) {
    relationName += "_" + el.split(".").join("_");
  });
  return relationName;
}

var NumericTypes = ["INT", "DECIMAL", "FLOAT"];

function getDimension(inter) {
  return inter.split(".")[0];
}

function joinIntersections(source, dest) {
  var destDims = dest.map(function (el) {
    return getDimension(el);
  });
  return source
    .filter(function (el) {
      return destDims.indexOf(getDimension(el)) === -1;
    })
    .concat(dest);
}

function removeDimension(source, dim) {
  return source.filter(function (el) {
    return dim !== getDimension(el);
  });
}

function addDimension(source, dim) {
  source.push(dim);
  return source;
}

function parseQualifiedLevels(ql) {
  return ql.map(function (el) {
    var label = el.label && el.label !== el.dimension ? el.label + ":" : "";
    return label + el.dimension + "." + el.level;
  });
}

function parseIntersction(inter) {
  return parseQualifiedLevels(inter.qualified_level);
}

function parseAttributeIntersection(attribute) {
  var label =
    attribute.label && attribute.label !== attribute.dimension
      ? attribute.label + ":"
      : "";
  return [
    label +
      attribute.qualified_level.dimension +
      "." +
      attribute.qualified_level.level,
  ];
}

function _parseAttribute(attribute) {
  var label =
    attribute.label && attribute.label !== attribute.dimension
      ? attribute.label + ":"
      : "";
  return (
    label +
    attribute.qualified_level.dimension +
    "." +
    attribute.qualified_level.level +
    "." +
    attribute.name
  );
}

di_1.register("introspect", ["metaModel"], function (metamodel) {
  function getPrimitiveType(expr) {
    var type = getType(expr);
    if (type.type) {
      return type.type.kind;
    }
    return type.kind;
  }

  function existInIntersection(label, intersection) {
    var exists = false;
    intersection.forEach(function (el) {
      if (el.label === label) {
        exists = true;
      }
    });
    return exists;
  }

  function removeFromIntersection(label, intersection) {
    if (!existInIntersection(label, intersection)) {
      throw (
        "Label " +
        label +
        " does not exist in intersection " +
        JSON.stringify(intersection)
      );
    }
    return intersection.filter(function (el) {
      if (el.label === label) {
        return false;
      }
      return true;
    });
  }

  function getAggIntersection(source, groupings) {
    source = source.map(function (el) {
      return {
        dimension: el.split(".")[0],
        level: el.split(".")[1],
        label: el.split(".")[0],
      };
    });
    var intersection = source.slice();
    groupings.forEach(function (el) {
      if (el.slide) {
        var sources = metamodel.getSlideSources(el.slide);
        sources.forEach(function (s) {
          intersection = removeFromIntersection(s.label, intersection);
        });
        intersection = intersection.concat(metamodel.getSlideTargets(el.slide));
      } else {
        intersection = removeFromIntersection(el.dimension, intersection);
        if (el.kind === "MAP") {
          intersection.push({
            dimension: el.dimension,
            label: el.dimension,
            level: el.level,
          });
        }
      }
    });
    return intersection.map(function (el) {
      return el.dimension + "." + el.level;
    });
  }

  function getType(expr) {
    switch (expr.kind) {
      case "ATTRIBUTE":
        return metamodel.getType(_parseAttribute(expr.attribute));
      case "METRIC":
        return metamodel.getType(expr.metric.name);
      case "AGGREGATION":
        var method = expr.aggregation.method.primitive;

        if (method === "COVER") {
          return null;
        }

        if (["COUNT", "COUNT_DISTINCT", "HISTOGRAM"].indexOf(method) !== -1) {
          return new MQG.Type("INT");
        }

        var type = getType(expr.aggregation.expr);

        if (
          ["TOTAL", "AVERAGE"].indexOf(method) !== -1 &&
          NumericTypes.indexOf(type.kind) === -1
        ) {
          throw "Method " + method + " only works for numeric values";
        }

        return type;
      case "DICE":
        return getType(expr.dice.expr);
      case "FILTER":
        return getType(expr.filter.expr);
      case "COMPOSITE":
        //all expression should have the same type
        return getType(expr.composite.expr[0]);
      case "OP":
        return getType(expr.op.expr[0]);
      case "WIDEN":
        return getType(expr.widen.expr);
      case "DROP":
        return null;
      case "DIFFERENCE":
        // All expressions should have the same intersection, so it suffice to just retreive the intersection of the first one
        return getType(expr.difference.right);
      case "DEMOTE":
        var inter = getIntersection(expr.demote.expr);
        var dimLevel = inter.filter(function (el) {
          return el.split(".")[0] === expr.demote.dimension;
        })[0];
        // not sure whether hard coding is the only way to go here
        return metamodel.getType(dimLevel + ".id");
      case "PARAM":
        return expr.param.signature.type && expr.param.signature.type.type;
      case "LITERAL":
        return expr.literal.signature.type && expr.literal.signature.type.type;
      case "VARIABLE":
        throw "unsupported";
      case "CAST":
        return expr.cast.type;
      case "PREV":
        return getType(expr.prev.expr);
      case "OVERRIDE":
        return getType(expr.override.expr[0]);
      case "SPLIT":
        return getType(expr.split.expr);
    }
  }

  function isSuperset(expr, metric) {
    var i, l;
    switch (expr.kind) {
      case "ATTRIBUTE":
        return (
          (Array.isArray(metric) &&
            metric.indexOf(_parseAttribute(expr.attribute)) > -1) ||
          _parseAttribute(expr.attribute) === metric
        );
      case "METRIC":
        return (
          (Array.isArray(metric) && metric.indexOf(expr.metric.name) > -1) ||
          expr.metric.name === metric
        );
      case "COMPOSITE":
        l = 0;
        for (i = 0; i < expr.composite.expr.length; i++) {
          if (isSuperset(expr.composite.expr[i], metric)) {
            l++;
          }
        }
        if (expr.composite.kind === "INTERSECTION") {
          return l === expr.composite.expr.length;
        }
        return l > 0;
      case "OP":
        l = 0;
        for (i = 0; i < expr.op.expr.length; i++) {
          if (isSuperset(expr.op.expr[i], metric)) {
            l++;
          }
        }
        return l === expr.op.expr.length;
      case "WIDEN":
        return isSuperset(expr.widen.expr, metric);
      case "DROP":
        return isSuperset(expr.drop.expr, metric);
      case "DIFFERENCE":
        // can be further optimized
        return (
          isSuperset(expr.difference.left, metric) &&
          isSuperset(expr.difference.right, metric)
        );
      case "CAST":
        return isSuperset(expr.cast.expr, metric);
      case "PREV":
        return isSuperset(expr.prev.expr, metric);
      case "OVERRIDE":
        l = 0;
        for (i = 0; i < expr.override.expr.length; i++) {
          if (isSuperset(expr.override.expr[i], metric)) {
            l++;
          }
        }
        return l > 0;
      default:
        return false;
    }
  }

  function isSubset(expr, metric) {
    var i, l;
    if (!metric) {
      throw "invalid parameters";
    }
    switch (expr.kind) {
      case "ATTRIBUTE":
        if (typeof metric === "string") {
          return _parseAttribute(expr.attribute) === metric;
        } else if (Array.isArray(metric)) {
          return metric.indexOf(_parseAttribute(expr.attribute)) > -1;
        } else {
          return isSuperset(metric, _parseAttribute(expr.attribute));
        }
      case "METRIC":
        if (typeof metric === "string") {
          return expr.metric.name === metric;
        } else if (Array.isArray(metric)) {
          return metric.indexOf(expr.metric.name) > -1;
        } else {
          return isSuperset(metric, expr.metric.name);
        }
      case "AGGREGATION":
        return isSubset(expr.aggregation.expr, metric);
      case "DICE":
        return isSubset(expr.dice.expr, metric);
      case "FILTER":
        return isSubset(expr.filter.expr, metric);
      case "COMPOSITE":
        l = 0;
        for (i = 0; i < expr.composite.expr.length; i++) {
          if (isSubset(expr.composite.expr[i], metric)) {
            l++;
          }
        }
        if (expr.composite.kind === "UNION") {
          return l === expr.composite.expr.length;
        }
        return l > 0;
      case "OP":
        l = 0;
        for (i = 0; i < expr.op.expr.length; i++) {
          if (isSubset(expr.op.expr[i], metric)) {
            l++;
          }
        }
        return l > 0;
      case "DROP":
        return isSubset(expr.drop.expr, metric);
      case "DIFFERENCE":
        // can be further optimized
        return isSubset(expr.difference.left, metric);
      case "DEMOTE":
        return isSubset(expr.demote.expr, metric);
      case "CAST":
        return isSubset(expr.cast.expr, metric);
      case "PREV":
        return isSubset(expr.prev.expr, metric);
      case "OVERRIDE":
        l = 0;
        for (i = 0; i < expr.override.expr.length; i++) {
          if (isSubset(expr.override.expr[i], metric)) {
            l++;
          }
        }
        return l === expr.override.expr.length;
      default:
        return false;
    }
  }

  // the argument should be a valid measure expression
  function getIntersection(expr) {
    switch (expr.kind) {
      case "ATTRIBUTE":
        return parseAttributeIntersection(expr.attribute);
      case "METRIC":
        return metamodel.getMetricIntersection(expr.metric.name);
      case "AGGREGATION":
        if (expr.aggregation.inter) {
          return parseIntersction(expr.aggregation.inter);
        } else {
          var aggIntersection = getAggIntersection(
            getIntersection(expr.aggregation.expr),
            expr.aggregation.grouping
          );
          var method = expr.aggregation.method.primitive;
          if (["SORT"].indexOf(method) !== -1) {
            // This is not exactly right as sorting intersection adds an Int.Int and an Index label. in the statement below, the label is missing because
            // MQG does not support Labels yet. This can cause problems when the original expression has an Int.Int key already. that's why the check is necessary
            if (aggIntersection.indexOf("Int.Int") >= 0) {
              throw "Sorting an expression keyed by Int.Int is not supported yet";
            }
            return aggIntersection.concat(["Int.Int"]);
          }
          return aggIntersection;
        }
      case "DICE":
        return getIntersection(expr.dice.expr);
      case "FILTER":
        return getIntersection(expr.filter.expr);
      case "COMPOSITE":
        //all expression at the same intersection
        return getIntersection(expr.composite.expr[0]);
      case "OP":
        // All expressions should have the same intersection, so it suffice to just retreive the intersection of the first one
        return getIntersection(expr.op.expr[0]);
      case "WIDEN":
        return joinIntersections(
          getIntersection(expr.widen.expr),
          parseIntersction(expr.widen.inter)
        );
      case "DROP":
        return getIntersection(expr.drop.expr);
      case "DIFFERENCE":
        // All expressions should have the same intersection, so it suffice to just retreive the intersection of the first one
        return getIntersection(expr.difference.left);
      case "DEMOTE":
        return removeDimension(
          getIntersection(expr.demote.expr),
          expr.demote.dimension
        );
      case "PROMOTE":
        return addDimension(
          getIntersection(expr.promote.expr),
          expr.promote.label
        );
      case "PARAM":
        return parseIntersction(expr.param.signature.intersection);
      case "LITERAL":
        return parseIntersction(expr.literal.signature.intersection);
      case "VARIABLE":
        throw "unsupported";
      case "CAST":
        return getIntersection(expr.cast.expr);
      case "PREV":
        return getIntersection(expr.prev.expr);
      case "OVERRIDE":
        return getIntersection(expr.override.expr[0]);
      case "RELABEL":
        return getIntersection(expr.relabel.expr);
      case "SPLIT":
        return getIntersection(expr.split.expr);
      default:
        throw "cannot parse expression, " + JSON.stringify(expr);
    }
  }

  return {
    getIntersection: getIntersection,
    getType: getType,
    getPrimitiveType: getPrimitiveType,
    isSubset: isSubset,
    isSuperset: isSuperset,
  };
});

di_1.register("generators", ["metaModel", "FilterSet", "MQG"], function (
  metaModel,
  FiltersSet,
  MQG
) {
  function RelationsRepo() {
    this.relations = {};
  }

  RelationsRepo.prototype._sanitize_name = function (name) {
    name = name.replace(/[^\w]+/g, "_");
    if (!/^[a-zA-Z]/.test(name)) {
      return "p_" + name;
    }
    return name;
  };

  RelationsRepo.prototype._genNextName = function (str) {
    var match = /_([0-9]+)$/.exec(str);
    if (match != null) {
      //jshint ignore:line
      var num = parseInt(match[1]) + 1;
      return str.replace(match[0], "_" + num);
    } else {
      return str + "_1";
    }
  };

  RelationsRepo.prototype.addParam = function (id, type, values) {
    if (!Array.isArray(values)) {
      values = [values];
    }
    return this.addRelation(
      id,
      [],
      values.map(function (v) {
        return [v];
      }),
      type
    );
  };
  RelationsRepo.prototype._createRelationbinding = function (
    id,
    intersection,
    values,
    type,
    singleton
  ) {
    var primitiveType = type;
    if (type && type.kind === "NAMED") {
      primitiveType = metaModel.getBackingType(type.named);
    } else if (type && type.kind) {
      primitiveType = type.kind;
    }
    return new MQG.RelationBinding(
      createParamExpr(id, intersection, type, singleton),
      new MQG.LiteralExpr(intersection, values, primitiveType)
    );
  };
  RelationsRepo.prototype.getValue = function (id) {
    return (this.relations[id] && this.relations[id].values) || null;
  };
  RelationsRepo.prototype.removeRelation = function (id) {
    if (this.relations[id]) {
      delete this.relations[id];
    }
  };
  RelationsRepo.prototype.get = function (id) {
    return this.relations[id];
  };
  RelationsRepo.prototype.getIds = function () {
    return Object.keys(this.relations);
  };
  RelationsRepo.prototype.addRelation = function (
    id,
    intersection,
    values,
    type,
    singleton
  ) {
    if (!id || !id.length) {
      id = "param_untitled";
    }
    id = this._sanitize_name(id);
    values = _normalize_value(values);
    singleton = singleton ? true : false;
    var obj = {
      intersection: intersection,
      values: values,
      type: type,
      singleton: singleton,
    };
    if (!this.relations[id]) {
      this.relations[id] = obj;
      return this.getParamExpr(id);
    } else {
      if (util.equals(this.relations[id], obj)) {
        // if there is already a relation with same type, intersection, and values, return it
        return this.getParamExpr(id);
      } else {
        // check for existing paramterm with another name
        return this.addRelation(
          this._genNextName(id),
          intersection,
          values,
          type,
          singleton
        );
      }
    }
  };

  RelationsRepo.prototype.getParamExpr = function (id) {
    if (!(id in this.relations)) {
      throw "invalid id, " + id;
    }
    var r = this.relations[id];
    return createParamExpr(id, r.intersection, r.type, r.singleton);
  };

  RelationsRepo.prototype.genRelations = function () {
    var rels = [];
    for (var id in this.relations) {
      var r = this.relations[id];
      rels.push(
        this._createRelationbinding(
          id,
          r.intersection,
          r.values,
          r.type,
          r.singleton
        )
      );
    }
    return rels;
  };

  var _normalize_value = function (values, type) {
    if (values.length) {
      switch (type) {
        case "INT":
        case "FLOAT":
          values[values.length - 1] = Number(values[values.length - 1]);
          break;
        case "BOOLEAN":
          values[values.length - 1] = values[values.length - 1] === true;
          break;
      }
    }
    return values;
  };

  var createParamExpr = function (relationName, intersection, type, singleton) {
    var _get_signature_type = function () {
      var kind;
      if (type) {
        // This may be an incorrect assumption. I don't know when SINGLETON sould be used, and how to determine it based on user input and metamodel.
        // It seems to me that SINGLETON is just a particular case of SET, so I'm using SET
        kind = singleton ? "SINGLETON" : "SET";
      }
      if (!kind) {
        return;
      }
      return new MQG.ValueType(kind, new MQG.Type(type));
    };
    return new MQG.ParamExpr(
      relationName,
      new MQG.BaseSignature(
        new MQG.Intersection(intersection),
        _get_signature_type()
      )
    );
  };

  var _parseQualifiedLevel = function (el) {
    if (el instanceof MQG.QualifiedLevel) {
      return el;
    }
    if (typeof el !== "string") {
      throw new SyntaxError(
        "Element should be either String or QualifiedLevel"
      );
    }
    var splitted = el.split("."),
      dim = splitted[0],
      level = splitted[1],
      label;
    if (dim.indexOf(":") !== -1) {
      splitted = dim.split(":");
      label = splitted[0];
      dim = splitted[1];
    }
    return new MQG.QualifiedLevel(dim, level, undefined, label);
  };

  var removeByQuery = function (update, expr, inputExpr) {
    var distribution = metaModel.getIntersection(update);
    return new MQG.UpdateExpr(
      "REMOVE",
      update,
      inputExpr,
      util.toArray(
        new MQG.UpdateQueryTransform(
          expr,
          distribution.map(function (el) {
            return _parseQualifiedLevel(el);
          }),
          null
        )
      )
    );
  };

  var spreadByQuery = function (update, expr, inputExpr) {
    var distribution = metaModel.getIntersection(update);
    return new MQG.UpdateExpr(
      "SPREAD",
      update,
      inputExpr,
      util.toArray(
        new MQG.UpdateQueryTransform(
          expr,
          distribution.map(function (el) {
            return _parseQualifiedLevel(el);
          }),
          metaModel.getType(update)
        )
      )
    );
  };

  return {
    RelationsRepo: RelationsRepo,
    spreadByQuery: spreadByQuery,
    removeByQuery: removeByQuery,
  };
});

var _view = function (str) {
  return /^\w+/.exec(str)[0];
};

var _kind = function (str) {
  var kind = str.slice(0, str.indexOf("/"));
  if (["query", "spread", "remove", "install"].indexOf(kind) > -1) {
    return kind;
  }
};

var _normalize = function (str) {
  if (str.indexOf("/") === 0) {
    str = str.slice(1);
  }
  if (str.lastIndexOf("/") === str.length - 1) {
    str = str.slice(0, str.length - 1);
  }
  var kind = str.slice(0, str.indexOf("/"));
  if (["query", "spread", "remove", "install"].indexOf(kind) > -1) {
    str = str.slice(str.indexOf("/") + 1);
  }

  str = str.replace("/?", "?");
  return str;
};

var getLevels = function (queryParams) {
  var levels = {};
  var regex = /^(\w+)level$/;
  queryParams.forEach(function (param) {
    if (!regex.test(param.name)) {
      return;
    }
    var parsedLevel = regex.exec(param.name)[1];
    levels[parsedLevel] = param.value;
  });
  return levels;
};

var getFilters = function (queryParams, filtersList) {
  var filters = {};
  queryParams.forEach(function (param) {
    if (filtersList.indexOf(param.name) === -1) {
      return;
    }
    if (!filters[param.name]) {
      filters[param.name] = {};
    }
    if (!filters[param.name][param.op]) {
      filters[param.name][param.op] = [];
    }
    filters[param.name][param.op].push(param.value);
  });
  return filters;
};

var getSource = function (str) {
  // check if it's valid spread format
  var regex = /:(\[.*\])$/;
  var parsedSource = [];
  if (regex.exec(str)) {
    var sourceStr = regex.exec(str)[1];
    try {
      parsedSource = JSON.parse(sourceStr);
      var i, j;
      for (j = 1; j < parsedSource.length; j++) {
        for (i = 0; i < parsedSource[j].length; i++) {
          if (typeof parsedSource[j][i] === "string") {
            parsedSource[j][i] = decodeURIComponent(parsedSource[j][i]);
          }
        }
      }
    } catch (ex) {
      throw "Uri error: Invalid source: " + sourceStr;
    }
  }
  return parsedSource;
};

var getMethod = function (queryParams) {
  var m = queryParams.filter(function (el) {
    return el.name === "method";
  });
  if (m.length) {
    return m[0].value;
  } else {
    return "replicate";
  }
};

var getQueryParams = function (str) {
  if (str.indexOf("?") === -1) {
    return [];
  }
  var path = str.slice(str.indexOf("?") + 1);
  if (path.indexOf(":[") > -1) {
    path = path.slice(0, path.lastIndexOf(":["));
  }
  var queryParams = path.split("&");
  var params = [];
  var regex = /^([\.\w]+)(=|<|>|!=|<=|>=|=~|~)(([^<>=!~].*)?)$/;
  queryParams.forEach(function (param) {
    if (!regex.test(param)) {
      return;
    }
    var parsed = regex.exec(param);
    var name = parsed[1];
    var op = parsed[2];
    var value = parsed[3];
    params.push({ name: name, op: op, value: decodeURIComponent(value) });
  });
  return params;
};

var parse = function (str, filtersList) {
  str = _normalize(str);

  var view = _view(str);
  var queryParams = getQueryParams(str);
  var filters = getFilters(queryParams, filtersList);
  var levels = getLevels(queryParams);
  var method = getMethod(queryParams);
  var source = getSource(str);

  return {
    view: view,
    params: queryParams,
    filters: filters,
    levels: levels,
    method: method,
    source: source,
  };
};

di_1.register("RelaxRepository", [], function () {
  var Repository = function () {
    this.views = {};
    this.filters = {};
    this.rewrites = [];
  };

  Repository.prototype.view = function (id, view) {
    if (!id) {
      throw "Invalid parameters";
    }
    if (!view) {
      return this.views[id].clone();
    }
    this.views[id] = view.clone();
  };
  Repository.prototype.filter = function (filter) {
    if (typeof filter === "string") {
      if (!this.filters[filter]) {
        return;
      }
      return this.filters[filter].clone();
    }
    this.filters[filter.id()] = filter.clone();
  };
  Repository.prototype.rewrite = function (rewrite) {
    this.rewrites.push(rewrite);
  };
  return Repository;
});

di_1.register("RelaxFilter", ["Filter", "MQG"], function (Filter, MQG) {
  var RelaxFilter = function (id, params, generate) {
    if (!(this instanceof RelaxFilter)) {
      return new RelaxFilter(id, params, generate);
    }
    var args = [].slice.call(arguments).filter(function (el) {
      return el;
    });
    if (!id) {
      throw "A filter id must be set";
    }
    if (args.length === 1) {
      params = [id];
      generate = id;
    } else if (args.length === 2) {
      generate = params;
      params = [id];
    }
    if (typeof generate === "string") {
      var name = generate;
      generate = function (value) {
        return new MQG.Measure(name).filterBy(value, false);
      };
    } else if (typeof generate !== "function") {
      var expr = generate;
      generate = function () {
        return expr;
      };
    }

    this._id = id;
    this._params = params;
    this._generate = generate;
    return this;
  };
  RelaxFilter.prototype.id = function () {
    return this._id;
  };
  RelaxFilter.prototype.params = function () {
    return this._params;
  };
  RelaxFilter.prototype.clone = function () {
    return new RelaxFilter(this._id, this._params.slice(), this._generate);
  };
  RelaxFilter.prototype.generateFilter = function (value) {
    return new Filter(this._id, this._generate.curry(value));
  };
  return RelaxFilter;
});

di_1.register("RelaxFilterSet", ["relaxRepository", "FilterSet"], function (
  repo,
  FilterSet
) {
  function RelaxFilterSet(filters) {
    FilterSet.call(this, filters);
  }
  RelaxFilterSet.prototype = Object.create(FilterSet.prototype);
  RelaxFilterSet.prototype.add = function (id, val) {
    if (id && typeof id === "string" && repo.filter(id)) {
      var f = repo.filter(id).generateFilter(val);
      return FilterSet.prototype.add.call(this, f);
    } else {
      return FilterSet.prototype.add.call(this, id, val);
    }
  };
  RelaxFilterSet.prototype.clone = function () {
    return new RelaxFilterSet(this._filters.slice());
  };

  return RelaxFilterSet;
});

di_1.register(
  "relax",
  [
    "relaxRepository",
    "Request",
    "RelaxFilterSet",
    "RelaxFilter",
    "RemoveView",
    "metaModel",
  ],
  function (
    repository,
    Request,
    FiltersSet,
    RelaxFilter,
    RemoveView,
    metaModel
  ) {
    metaModel
      .getMetrics()
      .concat(metaModel.getAttributes())
      .forEach(function (el) {
        repository.filter(new RelaxFilter(el));
      });

    function buildFiltersSet(urlParams) {
      var filters = [];
      var urlParamIds = Object.keys(urlParams);
      for (var filterId in repository.filters) {
        var filter = repository.filters[filterId];
        var params = filter.params();
        var isSuperset = params.every(function (val) {
          return urlParamIds.indexOf(val) >= 0;
        });
        if (isSuperset) {
          var val = {};
          for (var i = 0; i < params.length; i++) {
            val[params[i]] = urlParams[params[i]];
          }
          if (params.length === 1) {
            val = val[params[0]];
          }
          filters.push(filter.generateFilter(val));
        }
      }
      return new FiltersSet(filters);
    }

    function getFilterParams() {
      var params = [];
      for (var key in repository.filters) {
        params = params.concat(repository.filters[key].params());
      }
      return params;
    }

    function generateView(str) {
      var parsed = parse(str, getFilterParams());
      var view = repository.views[parsed.view];
      if (!view) {
        throw "View " + parsed.view + " does not exist";
      }
      var filters = parsed.filters;
      if (view.type === "query" || view.type === "install") {
        view = view
          .diceBy(buildFiltersSet(filters))
          .groupBy(parsed.levels)
          .setParams(parsed.params);
        if (_kind(str) === "install") {
          view = view.install();
        }
        return view;
      } else {
        return view
          .diceBy(buildFiltersSet(filters))
          .method(parsed.method)
          .source(parsed.source);
      }
    }

    var RelaxRequest = function () {
      Request.call(this);
    };
    RelaxRequest.prototype = Object.create(Request.prototype);
    RelaxRequest.prototype.push = function (url) {
      logger.log(logger.LOGLEVELS.INFO, url);
      this.views.push(generateView(url));
      return this;
    };
    RelaxRequest.prototype.pushQuery = function (view, levels, filters) {
      return this.push(util.constructUrl("query", view, levels, filters));
    };
    RelaxRequest.prototype.pushSpread = function (
      view,
      levels,
      filters,
      sourceSpread,
      spreadMethod
    ) {
      return this.push(
        util.constructUrl(
          "spread",
          view,
          {},
          filters,
          sourceSpread,
          spreadMethod
        )
      );
    };
    RelaxRequest.prototype.pushRemove = function (
      view,
      levels,
      filters,
      sourceSpread,
      spreadMethod
    ) {
      return this.push(
        util.constructUrl(
          "remove",
          view,
          {},
          filters,
          sourceSpread,
          spreadMethod
        )
      );
    };
    return RelaxRequest;
  }
);

function getDimensions(intersection) {
  return intersection.map(function (el) {
    return el.split(".")[0];
  });
}

di_1.register("MQG", ["metaModel", "introspect"], function (
  metaModel,
  introspect
) {
  MQG.Expr.prototype.getIntersection = function () {
    return introspect.getIntersection(this);
  };
  MQG.Expr.prototype.getType = function () {
    return introspect.getType(this);
  };
  MQG.Expr.prototype.isSubset = function (expr) {
    return introspect.isSubset(this, expr);
  };
  MQG.Expr.prototype.isSuperset = function (expr) {
    return introspect.isSuperset(this, expr);
  };
  MQG.Expr.prototype.getPrimitiveType = function () {
    return introspect.getPrimitiveType(this);
  };
  MQG.Expr.prototype.getDimensions = function () {
    return this.getIntersection().map(function (el) {
      return el.split(".")[0];
    });
  };

  var original_diceBy = MQG.Expr.prototype.diceBy;

  function _test_filter_string(filterStr) {
    var regex = /^([\w\.]+)(=|<|>|!=|<=|>=|=~|~)([^<>=!~].*)$/;
    if (!regex.test(filterStr)) {
      return false;
    }
    var parsed = regex.exec(filterStr);
    var name = parsed[1];
    var op = parsed[2];
    var value = {};
    value[op] = parsed[3];
    return {
      name: name,
      value: value,
    };
  }

  MQG.Expr.prototype.diceBy = function (dicers, disjunction, parameterRepo) {
    if (dicers.generate) {
      dicers = dicers.generate(this, parameterRepo);
    } else if (!Array.isArray(dicers)) {
      dicers = util.toArray(dicers);
    }
    dicers = dicers.map(function (el) {
      if (typeof el === "string") {
        var parsed = _test_filter_string(el);
        return new MQG.Measure(parsed.name).filterBy(parsed.value, false);
      } else {
        return el;
      }
    });
    return original_diceBy.call(this, dicers, disjunction);
  };

  MQG.Expr.prototype.groupBy = function (method, levels) {
    if (typeof levels === "string") {
      levels = [levels];
    }
    if (Array.isArray(levels)) {
      var obj = {};
      levels.forEach(function (el) {
        obj[el.split(".")[0]] = el.split(".").slice(1).join(".");
      });
      levels = obj;
    }
    levels = levels || {};
    var inter = introspect.getIntersection(this);
    var ldims = Object.keys(levels);
    var projectAway = getDimensions(inter).filter(function (el) {
      return ldims.indexOf(el) === -1;
    });
    projectAway = projectAway.filter(function (dim, index) {
      return projectAway.indexOf(dim) === index;
    });
    var aggArr = Object.keys(levels).map(function (el) {
      return el + "." + levels[el];
    });
    var agg = projectAway.concat(aggArr);
    return this.aggBy(method, agg);
  };

  var _generate_comparison = function (expr, op, value, id, parameterRepo) {
    var type = introspect.getType(expr);
    if (id && parameterRepo) {
      return new MQG.Comparison(
        op,
        parameterRepo.addParam(id, type.kind, value)
      );
    } else {
      return new MQG.Comparison(
        op,
        new MQG.LiteralExpr(
          "GENERAL",
          new MQG.BaseSignature(
            new MQG.Intersection([]),
            new MQG.ValueType("SET", new MQG.Type(type))
          ),
          [new MQG.Column(new MQG.Type(type), value)]
        )
      );
    }
  };
  var original_filterBy = MQG.Expr.prototype.filterBy;
  MQG.Expr.prototype.filterBy = function (
    value,
    disjunction,
    id,
    parameterRepo
  ) {
    disjunction = disjunction || false;
    if (
      value instanceof MQG.Comparison ||
      (value && value.length && value[0] instanceof MQG.Comparison)
    ) {
      return original_filterBy.call(this, value, disjunction);
    }
    if (typeof disjunction === "object") {
      disjunction = disjunction.disjunction;
    }
    var mqfilters = [];
    if (
      typeof value === "string" ||
      typeof value === "number" ||
      Array.isArray(value)
    ) {
      value = {
        "=": value,
      };
    }
    for (var op in value) {
      if (!value.hasOwnProperty(op)) {
        continue;
      }
      var values = util.toArray(value[op]);
      if (this.getPrimitiveType() === "INT") {
        try {
          values = values.map(function (n) {
            return parseInt(n);
          });
        } catch (ex) {
          console.error(
            "FilterBy error: filtering an INT metric by an incompatible value ",
            values
          );
        }
      }
      if (op !== "=") {
        for (var j = 0; j < values.length; j++) {
          mqfilters.push(
            _generate_comparison(this, op, values[j], id, parameterRepo)
          );
        }
      } else {
        mqfilters.push(
          _generate_comparison(this, op, values, id, parameterRepo)
        );
      }
    }
    return original_filterBy.call(this, mqfilters, disjunction);
  };
  var original_LiteralExpr = MQG.LiteralExpr;
  MQG.LiteralExpr = function (intersection, values, type) {
    if (typeof intersection === "string") {
      // direct call
      return new original_LiteralExpr(intersection, values, type);
    }
    type = type || undefined;
    var sample_value = values[0];
    var typeInt = type ? 1 : 0;

    if (sample_value) {
      if (
        intersection.length &&
        sample_value.length !== intersection.length + typeInt
      ) {
        throw (
          "values mismatch with metric intersection, values:" +
          sample_value +
          ",intersection:" +
          intersection +
          " ,type" +
          type
        );
      }
    }

    var columns = [];
    var i;
    for (i = 0; i < intersection.length; i++) {
      var levelType = metaModel.getPrimitiveType(intersection[i] + ".id");
      columns.push(
        new MQG.Column(
          levelType,
          values.map(function (el) {
            return el[i];
          })
        )
      );
    }
    if (typeInt) {
      columns.push(
        new MQG.Column(
          type,
          values.map(function (el) {
            return el[i];
          })
        )
      );
    }
    return new original_LiteralExpr(
      "GENERAL",
      new MQG.BaseSignature(
        new MQG.Intersection(intersection),
        type ? new MQG.ValueType("SET", new MQG.Type(type)) : undefined
      ),
      columns
    );
  };

  MQG.Expr.prototype.ratioSpread = function (obj) {
    var destination = obj.destination;
    var base = obj.base || obj.destination;
    var multiplierExpr = MQG.Measure(base).diceBy(obj.filters);
    if (base == destination) {
      multiplierExpr = multiplierExpr.prev();
    }
    var divideExpr =
      (obj.divideMetric && MQG.Measure(obj.divideMetric).diceBy(obj.filters)) ||
      multiplierExpr;
    return multiplierExpr
      .toFloat()
      .multiply(
        this.toFloat()
          .divide(
            divideExpr
              .groupBy(
                "TOTAL",
                this.getIntersection().filter(function (el) {
                  return (
                    divideExpr.getDimensions().indexOf(el.split(".")[0]) !== -1
                  );
                })
              )
              .toFloat()
          )
          .widen(multiplierExpr.getIntersection())
      )
      .cast(metaModel.getType(destination));
  };

  MQG.Expr.prototype.replicateSpread = function (obj) {
    var destination = obj.destination;
    var dicers = obj.filters;
    if (obj.base !== obj.destination) {
      dicers = dicers.add(MQG.Measure(obj.base));
    }
    var resultExpr = this.widen(metaModel.getIntersection(destination)).diceBy(
      dicers
    );
    if (metaModel.getType(destination)) {
      resultExpr = resultExpr.cast(metaModel.getType(destination));
    }
    return resultExpr;
  };

  MQG.Expr.prototype.percentageSpread = function (obj) {
    var destination = obj.destination;
    var baseExpr = MQG.Measure(obj.base).diceBy(obj.filters);
    return this.toDecimal()
      .widen(metaModel.getIntersection(destination))
      .multiply(baseExpr.toDecimal());
  };

  MQG.Expr.prototype.evenSpread = function (obj) {
    var destination = obj.destination;
    var baseExpr = MQG.Measure(obj.base).diceBy(obj.filters);
    return this.divide(
      baseExpr.groupBy("COUNT", this.getIntersection()).cast(this.getType())
    )
      .widen(metaModel.getIntersection(destination))
      .cast(metaModel.getType(destination));
  };

  return MQG;
});

var arrIntersection = function (arr1, arr2) {
  return arr1.filter(function (el) {
    return arr2.indexOf(el) !== -1;
  });
};

di_1.register("rewrites", ["introspect", "MQG"], function (introspect, MQG) {
  function rewrite(expr, strategy) {
    switch (expr.kind) {
      case "ATTRIBUTE":
        break;
      case "METRIC":
        break;
      case "AGGREGATION":
        expr.aggregation.expr = strategy(expr.aggregation.expr);
        break;
      case "DICE":
        expr.dice.expr = strategy(expr.dice.expr);
        expr.dice.dicer = expr.dice.dicer.map(strategy);
        break;
      case "FILTER":
        expr.filter.expr = strategy(expr.filter.expr);
        expr.filter.comparison = expr.filter.comparison.map(function (el) {
          el.expr = strategy(el.expr);
          return el;
        });
        break;
      case "COMPOSITE":
        expr.composite.expr = expr.composite.expr.map(strategy);
        break;
      case "OP":
        expr.op.expr = expr.op.expr.map(strategy);
        break;
      case "WIDEN":
        expr.widen.expr = strategy(expr.widen.expr);
        break;
      case "DROP":
        expr.drop.expr = strategy(expr.drop.expr);
        break;
      case "DIFFERENCE":
        expr.difference.left = strategy(expr.difference.left);
        expr.difference.right = strategy(expr.difference.right);
        break;
      case "DEMOTE":
        expr.demote.expr = strategy(expr.demote.expr);
        break;
      case "PROMOTE":
        expr.promote.expr = strategy(expr.promote.expr);
        break;
      case "PARAM":
        break;
      case "LITERAL":
        break;
      case "VARIABLE":
        throw "unsupported";
      case "CAST":
        expr.cast.expr = strategy(expr.cast.expr);
        break;
      case "PREV":
        expr.prev.expr = strategy(expr.prev.expr);
        break;
      case "OVERRIDE":
        expr.override.expr = expr.override.expr.map(strategy);
        break;
    }
    return expr;
  }

  var bottomUpStrategy = function (env, expr) {
    expr = rewrite(expr, bottomUpStrategy.curry(env));
    //expr = removeLiterals(expr, env);
    expr = removeNotApplicableDicers(expr);
    expr = filterToDice(expr, env);
    return expr;
  };

  var removeNotApplicableDicers = function (expr) {
    if (expr.kind === "DICE") {
      var dicers = expr.dice.dicer;
      var diceInter = introspect
        .getIntersection(expr.dice.expr)
        .map(function (el) {
          return el.split(".")[0];
        });
      var i = 0;
      while (i < dicers.length) {
        var inter = arrIntersection(
          diceInter,
          introspect.getIntersection(dicers[i]).map(function (el) {
            return el.split(".")[0];
          })
        );
        if (!inter.length) {
          dicers.splice(i, 1);
        } else {
          i++;
        }
      }
      if (dicers.length === 0) {
        return expr.dice.expr;
      }
    }
    return expr;
  };

  function _parseAttribute(attribute) {
    var label =
      attribute.label && attribute.label !== attribute.dimension
        ? attribute.label + ":"
        : "";
    return (
      label +
      attribute.qualified_level.dimension +
      "." +
      attribute.qualified_level.level +
      "." +
      attribute.name
    );
  }

  var filterToDice = function (expr, env) {
    if (
      expr.kind !== "FILTER" ||
      expr.filter.expr.kind !== "ATTRIBUTE" ||
      expr.filter.expr.attribute.name !== "id" ||
      expr.filter.comparison.length !== 1 ||
      expr.filter.comparison[0].op !== "EQUALS" ||
      expr.filter.comparison[0].expr.kind !== "PARAM" ||
      expr.filter.comparison[0].expr.param.signature.intersection
        .qualified_level.length
    ) {
      return expr;
    }

    //TODO:labels can be supported as well. I just need to make a quick workaround for now
    if (expr.filter.expr.attribute.qualified_level.label) {
      return expr;
    }

    var param = expr.filter.comparison[0].expr.param;
    return env.relationRepo.addRelation(
      filtertodice_id(param.name),
      [_parseAttribute(expr.filter.expr.attribute)],
      env.relationRepo.getValue(param.name)
    );
  };

  var filtertodice_id = function (id) {
    return "filtertodice_" + id + "_filtertodice";
  };
  var teardown = function (request, env) {
    //remove filtertodice relations
    var ids = env.relationRepo.getIds();
    var filterToDiceIds = ids.filter(function (el) {
      return ids.indexOf(filtertodice_id(el)) > -1;
    });
    filterToDiceIds.forEach(function (el) {
      //quick, fragile and dirty. Need to rewrite this when I have more time
      var regexp = new RegExp('"param":{"name":"' + el + '"', "g");
      //1 for the param in relationsRepo
      if (JSON.stringify(request).match(regexp).length <= 1) {
        env.relationRepo.removeRelation(el);
      }
    });
    request.relation = env.relationRepo.genRelations();
  };

  return { rewrites: bottomUpStrategy, teardown: teardown };
});

function MQG$1(config) {
  this.config = config;
  this.metaModel = new metamodel(this.config.metaModel);
  this.measureService = config.measureService;

  this.container = di_1.container.clone();
  var that = this;
  //register singletons of this instance
  this.container.register("metaModel", [], function () {
    return that.metaModel;
  });
  this.optimizer = new optimizer([], this.config);
  this.container.register("optimizer", [], function () {
    return that.optimizer;
  });
  var rewrite = this.container.get("rewrites");
  this.optimizer.addRewrite(rewrite.rewrites).tearDown(rewrite.teardown);
  var Repository = this.container.get("RelaxRepository");
  this.repository = new Repository();
  this.container.register("relaxRepository", [], function () {
    return that.repository;
  });
  this.Filter = this.container.get("Filter");
  this.RelaxFilter = this.container.get("RelaxFilter");
  this.FiltersSet = this.container.get("FilterSet");
  this.Request = this.container.get("Request");
  this.QueryView = this.container.get("QueryView");
  this.SpreadView = this.container.get("SpreadView");
  this.RemoveView = this.container.get("RemoveView");
  this.RelaxRequest = this.container.get("relax");
  this.builder = this.container.get("MQG");
  this.introspect = this.container.get("introspect");
  this.util = util;

  var origin_getResponse = this.Request.prototype.getResponse;
  this.Request.prototype.getResponse = function (headers) {
    headers = headers || that.measureService;
    return origin_getResponse.call(this, headers);
  };
  this.QueryView.prototype.save = function (id) {
    that.repository.view(id || this.reportName, this);
  };
  this.SpreadView.prototype.save = function (id) {
    that.repository.view(id || this.reportName, this);
  };
  this.RemoveView.prototype.save = function (id) {
    that.repository.view(id || this.reportName, this);
  };
  //TODO, use arguments instead of a,b,c,d
  this.addRelaxFilter = function (a, b, c, d) {
    that.repository.filter(new that.RelaxFilter(a, b, c, d));
    return this;
  };
  this.setLogLevel = function (info) {
    logger.setLogLevel(info);
    return this;
  };

  this.relaxQuery = function (view, levels, filters) {
    return new that.RelaxRequest()
      .pushQuery(view, levels, filters)
      .getResponse()
      .then(function (data) {
        return data[0];
      });
  };

  this.relaxSpread = function (
    view,
    levels,
    filters,
    sourceSpread,
    spreadMethod
  ) {
    return new that.RelaxRequest()
      .pushSpread(view, levels, filters, sourceSpread, spreadMethod)
      .getResponse();
  };

  this.relaxRemove = function (view, levels, filters, sourceRemove) {
    return new that.RelaxRequest()
      .pushRemove(view, levels, filters, sourceRemove)
      .getResponse();
  };
}

// static methods
MQG$1.fetchMetaModel = function (headers) {
  if (typeof headers === "string") {
    headers = {
      url: headers,
    };
  }
  return util.postJSON(
    "POST",
    headers,
    new MQG.Request(undefined, undefined, undefined, true)
  );
};

MQG$1.init = function (measureService) {
  return MQG$1.fetchMetaModel(measureService).then(function (data) {
    return new MQG$1({
      measureService: measureService,
      metaModel: data,
    });
  });
};
MQG$1.version = 4;

var MQU = MQG$1;

module.exports = MQU;
