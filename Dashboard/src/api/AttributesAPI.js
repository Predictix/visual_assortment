import { getMQG } from "../api/utils";

export async function getAttributes() {
  const MQG = await getMQG();

  // TODO: make this configurable
  const data = [
    { label: "Fabric Weight", value: "ProductFabricWeight_S_WP_PL_AP" },
    { label: "Product Silhouette", value: "ProductSilhouette_S_WP_PL_AP" },
    { label: "Sleeve Length", value: "ProductSleeveLength_S_WP_PL_AP" },
    { label: "Material Category", value: "ProductMaterialCategory_S_WP_PL_AP" },
    { label: "Fashion intent", value: "ProductFashionIntent_S_WP_PL_AP" },
  ];

  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(data);
    }, 1000);
  });

  // Get the list of the levels in the Concept dimension
  return MQG.config.metaModel.model.dimension
    .find((dimension) => dimension.caption === "Concept")
    .level.map((level) => ({ value: level.name, label: level.caption }));
}
