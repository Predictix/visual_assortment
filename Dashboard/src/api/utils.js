import MQU from "./MQU";

let MQG = null;

// Instantiate MQG only a single time
export async function getMQG() {
  if (!MQG) {
    MQG = await MQU.init("http://localhost:8080/OLAP");
  }

  return MQG;
}

// Get the list of the current filters
// in the format which MQG supports
export function getFiltersList(params, filtersMapping) {
  const filters = [];

  for (const [key, value] of params) {
    if (key === "splitBy") {
      continue;
    }
    const { dimension, level } = filtersMapping[key];
    filters.push(`${dimension}.${level}.id=${value}`);
  }

  return filters;
}

// Get the list of the measures that we have in the workspace
export function getMeasuresList(MQG) {
  return MQG.config.metaModel.model.metric.map((metric) => ({
    value: metric.name,
    label: metric.caption,
  }));
}

// Get the label of a specific measure
export function getMeasureLabel(MQG, measure) {
  return MQG.config.metaModel.model.metric.find(
    (metric) => metric.name === measure
  ).caption;
}
