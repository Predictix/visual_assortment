import { getMQG, getFiltersList } from "../api/utils";

export async function getPlans(component, filtersMapping, params) {
  const MQG = await getMQG();

  const filters = getFiltersList(params, filtersMapping);

  let data = MQG.QueryView();

  const { name, ...rest } = component;

  for (let key in rest) {
    data = data.measure(key, rest[key]);
  }

  data = await data.diceBy(filters).get();

  return { ...data[0], name };
}
