import { getMQG } from "../api/utils";

export async function getFilterData(filter, filtersMapping) {
  const MQG = await getMQG();
  const { level, dimension, drilldownTo } = filtersMapping[filter];

  const options = await MQG.QueryView()
    .attribute("label", `${dimension}.${level}.label`)
    .measure("value", `${dimension}.${level}.id`)
    .groupBy(`${dimension}.${level}`)
    .get();

  return {
    id: filter,
    options,
    level,
    dimension,
    drilldownTo,
  };
}
