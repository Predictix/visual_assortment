module.exports = {
  theme: {
    inset: {
      "0": 0,
      auto: "auto",
      "1/2": "50%",
    },
  },
  plugins: [require("@tailwindcss/custom-forms")],
};
