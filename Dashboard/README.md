# Assortment Application Dashboard

![Dashboard Screenshot](screenshots/dashboard_screenshot.png)

## Getting Started

#### Clone the repo

```bash
git clone git@bitbucket.org:Predictix/visual_assortment.git
```

#### Commands

| Command       | Description              |
| ------------- | ------------------------ |
| `npm install` | Install the dependencies |
| `npm start`   | Run the project          |
| `npm test`    | Run tests                |
| `npm build`   | Build the project        |

## Documentation

### Dashboard Configuration

```json
{
    // {name: pxVal}, e.g. {lg: 1200, md: 996, sm: 768, xs: 480}
    // Breakpoint names are arbitrary but must match in the cols and layouts objects.
    "breakpoints": { "lg": number, "md": number, "sm": number, "xs": number, "xxs": number },
    // # of cols. This is a breakpoint -> cols map, e.g. {lg: 12, md: 10, ...}
    "cols": { "lg": number, "md": number, "sm": number, "xs": number, "xxs": number },
    // Default Breakpoint
    "currentBreakpoint": string,
    // List of the boxes showed by default
    "layouts": {
        // breakpoint
        [key]: {
            // A string corresponding to the box id
            "i": string,
            // These are all in grid units, not pixels
            "w": number,
            "h": number,
            "x": number,
            "y": number,
            "minW": ?number = 0,
            "maxW": ?number = Infinity,
            "minH": ?number = 0,
            "maxH": ?number = Infinity

        }
    },
    // List of the hidden boxes (in Sidebar)
    "toolbox": {
        // Box Id
        [key]: {
            "i": string,
            "w": number,
            "h": number,
            "x": number,
            "y": number,
            "minW": number,
            "minH": number
        }
    },
    // Custom properties of each box
    "boxes": {
        // Contribution Analysis Box Configuration
        "contribution-analysis": {
            // Box Id
            "id": "contribution-analysis",
            // Box Label
            "label": string,
            // Array of the chart colors code
            "chartColors": string[],
            // Default Widgets
            "widgets": {
                // Widget Id
                "id": string,
                // Default Attribute
                "attribute": string,
                // Default Measure
                "measure": string
            }[],
        },
        // Buy Plan Review Box Configuration
        "buy-plan-review": {
            // Box Id
            "id": "buy-plan-review",
            // Box Label
            "label": string,
            "components": {
                // Name of the component
                "name": string,
                // List of the measures to be displayed in each component keyed by a key
                [key]: string
            }[],
            "barColors": {
                // Color of the key
                [key]: string
            }
        },
        // Assortment Mix Box Configuration
        "assortment-mix": {
            // Box Id
            "id": "assortment-mix",
            // Box Label
            "label": string,
            // Default number of items displayed in the box
            "itemsPerPage": number
        },
        // Statistics Box Configuration
        "stats": {
            // Box Id
            "id": "stats",
            // Box Label
            "label": string
        },
        // Filtering Treemap Configuration
        "filtering-treemap": {
            // Box Id
            "id": "filtering-treemap",
            // Box Label
            "label": string,
            // Measure used in calculating the rectangle size
            "sizeMetric": string,
            // Measure used in calculating the rectangle color
            "colorsMetric": string,
            // Colors Configuration
            "colorsOptions": {
                // List of the colors
                "values": {
                    // Code of the color (Hexadecimal, plain text...)
                    "code": string,
                    // Use the color which satisfy the below interval
                    "min": number,
                    "max": number
                }[],
                "defaultCode": string
            },
            // List of the filters keys used in the treemap
            "filtersList": string[],
            // Default filter used in the first level of the treemap
            "defaultFilter": string
        }
    },
    // Configuration of the filters keys
    "filtersMapping": {
        // The filter key
        [key]: {
            // Dimension of the filter
            "dimension": string,
            // Level of the filter
            "level": string,
            // Filter Key of where the current filter should drill down to
            "drilldownTo": string | null
        }
    },
    // List of the global filters keys
    "globalFilters": string[]
}
```

[Example](https://bitbucket.org/Predictix/visual_assortment/src/mqg-integration/Dashboard/src/config/layout.json)

for more reference, check: https://github.com/STRML/react-grid-layout#grid-item-props

TODO:

- Improve the performance.
- Integrate the statistics visual component with MQG.
- Get rid of react router and replace it with a custom hook that manage
  the URLSearchParams object (check https://github.com/ReactTraining/react-router/blob/dev/packages/react-router-dom/index.tsx#L391 for inspiration).
- Import the SVG icons directly from https://www.npmjs.com/package/heroicons
